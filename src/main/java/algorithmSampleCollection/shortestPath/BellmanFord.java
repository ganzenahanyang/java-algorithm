package algorithmSampleCollection.shortestPath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * 음수 간선의 순환을 감지할 수 있다
 * 기본 시간 복잡도는 O(VE)로  다익스트라보다 느리다
 * <p>
 * 1. 출발 노드를 설정한다
 * 2. 최단 거리 테이블을 초기화한다 (거리를 무한)
 * 3. 다음의 과정을 N-1 번 반복한다
 *      1) 전체 간선 E개를 하나씩 확인한다.
 *      2) 각 간선을 거쳐 다른 노드로 가는 비용을 계산하여 최단 거리 테이블을 갱신한다.
 * 4. 음수 간선 순환이 발생하는지 체크하고 싶으면 3번의 과정을 한 번 더 수행한다.
 *      1) 이 때 최단 거리 테이블이 갱신된다면 음수 간선 순환이 존재하는 것이다.
 */
public class BellmanFord {
    static class Edge {
        int cost;
        int nodeA;
        int nodeB;

        public Edge(int cost, int nodeA, int nodeB) {
            this.cost = cost;
            this.nodeA = nodeA;
            this.nodeB = nodeB;
        }
    }

    // 노드의 갯수 N 간선의 갯수 M
    // 노드의 갯수는 최대 500개라고 가정
    static int N, M;
    // 모든 간선의 정보를 담는 리스트
    static ArrayList<Edge> edges = new ArrayList<>();
    // 최단거리 테이블
    static int[] d = new int[501];

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();
        M = sc.nextInt();

        // 모든 간선 정보를 입력 받기
        for (int i = 0; i < M; i++) {
            int cost = sc.nextInt();
            int nodeA = sc.nextInt();
            int nodeB = sc.nextInt();
            edges.add(new Edge(cost, nodeA, nodeB));
        }

        // 최단거리 테이블을 모두 무한으로 초기화
        Arrays.fill(d, Integer.MAX_VALUE);

        // 벨만 포드 알고리즘 수행
        boolean negativeCycle = bellmanFord(1);

        // 음수 순환 발생의 경우 -1 출력
        if (negativeCycle) {
            System.out.println(-1);
        } else {
            // 1번 노드를 제외한 다른 모든 노드로 가기 위한 최단거리를 출력
            for (int i = 2; i <= N; i++) {
                System.out.print("1 to " + i + " : ");
                // 도달할 수 없는 경우 -1을 출력
                if (d[i] == Integer.MAX_VALUE) {
                    System.out.println(-1);
                } else {
                    System.out.println(d[i]);
                }
            }
        }
    }

    static boolean bellmanFord(int start) {
        // 시작 노드에 대해서 초기화
        d[start] = 0;
        // 전체 n - 1 번의 라운드를 반복
        for (int i = 0; i < N; i++) {
            // 매 반복마다 "모든 간선"을 확인하며
            for (int j = 0; j < M; j++) {
                Edge e = edges.get(j);
                int now = e.nodeA;
                int next = e.nodeB;
                int cost = e.cost;
                // 현재 간선을 거쳐서 다른 노드로 이동하는 거리가 더 짧은 경우
                if (d[now] != Integer.MAX_VALUE && d[next] > d[now] + cost) {
                    d[next] = d[now] + cost;
                    // n번째 라운드에서도 값이 갱신된다면 음수 순환이 존재
                    if (i == N - 1)
                        return true;
                }
            }
        }
        return false;
    }
}
