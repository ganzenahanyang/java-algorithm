package algorithmSampleCollection.shortestPath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;

/**
 * 1. 출발 노드를 설정한다
 * 2. 최단 거리 테이블을 초기화한다.
 * 3. 방문하지 않은 노드 중에서 최단 거리가 가장 짧은 노드를 선택한다.
 * 4. 해당 노드를 거쳐 다른 노드로 가는 비용을 계산하여 최단 거리 테이블을 갱신한다.
 * 5. 3번과 4번을 반복한다.
 *
 * 힙 자료구조를 이용하는 다익스트라 알고리즘의 시간 복잡도는 O(ElogV)이다.
 * 노드를 하나씩 꺼내 검사하는 반복문(while)은 노드의 갯수 이상으로 처리되지 않는다.
 *      결론적으로 우선순위큐에서 꺼낸 노드와 연결된 다른 노드들을 확인하는 총 횟수는 최대 간선의 개수(E)만큼 연산이 수행될 수 있다.
 * 전체과정은 E개의 원소를 우선순위 큐에 넣었다가 모두 빼내는 연산과 매우 유사하다.
 * 따라서 시간복잡도를 O(ElogE)로 판단할 수 있다.
 * 중복 간선을 포함하지 않는 경우에는 이를 O(ElogV)로 정리할수 있다
 */

public class Dijkstra {

    static class Node implements Comparable<Node> {

        private int index; // 정점의 번호
        private int cost; // 정점까지의 거리리

        public Node(int index, int distance) {
            this.index = index;
            this.cost = distance;
        }

        // 거리(비용)가 짧은 것이 높은 우선 순위를 가지도록 설정
        @Override
        public int compareTo(Node o) {
            return this.index - o.index;
        }
    }

    // 노드의 갯수(N), 간선의 갯수(M), 시작 노드 번호(Start)
    // 노드의 갯수는 최대 100,000 개라고 가정
    static int N, M, Start;

    // 각 노드에 연결되어 있는 노드에 대한 정보를 담는 배열
    // 인접 리스트의 형태로 생성한다.
    static ArrayList<ArrayList<Node>> adjList = new ArrayList<ArrayList<Node>>();

    // 최단거리 테이블
    static int[] d = new int[100001];

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();
        M = sc.nextInt();
        Start = sc.nextInt();

        // 그래프 초기화
        for (int i = 0; i <= N; i++) {
            // 인접 리스트 구현을 위해 리스트 안의 각각의 값에 List 선언
            adjList.add(new ArrayList<Node>());
        }

        // 모든 간선 정보를 인접 리스트에 삽입
        for (int i = 0; i < M; i++) {
            int from = sc.nextInt();
            int to = sc.nextInt();
            int dist = sc.nextInt();

            adjList.get(from).add(new Node(to, dist));
        }

        // 최단거리 테이블을 모두 무한으로 초기화
        Arrays.fill(d, Integer.MAX_VALUE);

        // 다익스트라 알고리즘을 수행
        dijkstra(Start);

        for (int i = 1; i <= N; i++) {
            System.out.printf("From %d To %d distance : ", Start, i, d[i]);

        }
    }

    static void dijkstra(int start) {
        PriorityQueue<Node> pq = new PriorityQueue<>();
        // 시작 노드로 가기 위한 최단 경로는 0으로 설정(자기 자신이기 때문)하여 큐에 삽입
        pq.add(new Node(start, 0));
        d[start] = 0;
        while (!pq.isEmpty()) { // 큐가 비어있지 않다면
            // 가장 최단 거리가 짧은 노드에 대한 정보 꺼내기
            /**
             * 내 생각 정리
             * 다익스트라 알고리즘은 양의 가중치를 가진 상황에 한해서 유효하게 작동한다
             * 따라서 현재 출발하는 정점에서 가장 짧은 길을 택하는 것 (현재의 정점에 도달하기까지도 계속 짧은 길을 택했다.)이 최단 경로를 얻는 것이다
             */
            Node now = pq.poll();
            int cost = now.cost; // 현재 노드까지의 비용
            int index = now.index; // 현재 노드

            // 현재 노드가 이미 처리된 적이 있는 노드라면 무시
            if (d[index] < cost)
                continue;

            // 현재 노드와 연결된 다른 인접한 노드들을 확인
            for (int i = 0; i < adjList.get(index).size(); i++) {
                // 현재 정점까지의 이동 비용 + 다음 노드로 이동하는데 드는 비용
                int nextCost = cost + adjList.get(index).get(i).cost;
                // 현재 노드를 거쳐서, 다른 노드로 이동하는 거리가 짧은 경우
                if (nextCost < d[adjList.get(index).get(i).index]) {
                    d[adjList.get(index).get(i).index] = nextCost;
                    pq.add(new Node(adjList.get(index).get(i).index, nextCost));
                }
            }
        }

    }
}
