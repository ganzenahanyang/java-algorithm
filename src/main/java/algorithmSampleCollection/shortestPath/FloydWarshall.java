package algorithmSampleCollection.shortestPath;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 모든 노드에서 다른 모든 노드까지의 최단 경로를 모두 계산한다.
 * 다익스트라 알고리즘과 마찬가지로 단계별로 "거쳐 가는 노드를 기준으로 알고리즘을 수행한다."
 *      다만 매 단계마다 방문하지 않은 노드 중에 최단 거리 노드를 찾는 과정이 필요하지 않다.
 * 2차원 테이블에 최단 거리 정보를 저장한다.
 *
 * 플로이드 워셜은 아래의 점화식을 사용한다
 * a 에서 b로 가는 최단 거리보다 a에서 k를 거쳐 b로 가는 거리가 더 짧은지 검사한다.
 * D(ab) = min(D(ab), D(ak) + D(kb))
 *
 * 노드의 갯수가 N개일 때 알고리즘 상으로 N번의 단계를 수행한다
 * 각 단계마다 O(N^2)의 연산을 통해 현재 노드를 거쳐가는 모든 경로를 고려한다.
 * 따라서 Time Complexity 는 O(N^3)
 */
public class FloydWarshall {

    // 노드의 갯수 N 간선의 갯수 M
    // 노드의 갯수는 최대 500개 이 이상은 시간복잡도 때문에 안나옴
    static int N, M;
    // 2차원 배열(그래프 표현)을 만들기
    static int[][] graph = new int[501][501]; // 행을 출발 열을 도착으로 가정

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        N = sc.nextInt();
        M = sc.nextInt();

        // 최단 거리 테이블을 모두 무한으로 초기화
        for (int i = 0; i < 501; i++) {
            Arrays.fill(graph[i], Integer.MAX_VALUE);
        }

        // 자기 자신에서 자기 자신으로 가는 비용은 0으로 초기화
        for (int a = 1; a < N; a++) {
            for (int b = 1; b < N; b++)
                if (a == b)
                    graph[a][b] = 0;
        }

        // 각 간선에 대한 정보를 입력받아 그 값으로 초기화
        for(int i = 0 ; i < M ; i++){
            // A에서 B로 가는 비용은 C라고 설정
            int a = sc.nextInt();
            int b = sc.nextInt();
            int c = sc.nextInt();
            graph[a][b] = c;
        }

        // 점화식에 따라 플로이드 워셜 알고리즘을 수행
        /**
         * 1번 정점을 거치는 케이스에 대한 업데이트
         * D23 = min(D23, D21 + D13)
         * D24 = min(D24, D21 + D14)
         * D32 = min(D32, D31 + D14)
         * D34 = min(D34, D31 + D14)
         * D42 = min(D42, D41 + D12)
         * D43 = min(D43, D41 + D13)
         */
        for(int k = 1; k <= N ; k++){ // 특정한 노드 k를 거침
            for(int a = 1 ; a <= N ; a++){
                for(int b = 1 ; b <= N ; b++){
                    graph[a][b] = Math.min(graph[a][b], graph[a][k] + graph[k][b]);
                }
            }
        }

        // 수행된 결과를 출력
        for(int a = 1 ; a <= N; a++){
            for(int b = 1 ; b <= N ; b++){
                // 도달할 수 없는 경우, 무한 출력
                if(graph[a][b] == Integer.MAX_VALUE){
                    System.out.println("INFINITY");
                }else{
                    System.out.println(graph[a][b] + " ");
                }
            }
        }
    }
}

