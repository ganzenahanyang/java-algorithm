package algorithmSampleCollection.shortestPath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * 1. 출발 노드를 설정한다
 * 2. 최단 거리 테이블을 초기화한다.
 * 3. 방문하지 않은 노드 중에서 최단 거리가 가장 짧은 노드를 선택한다.
 * 4. 해당 노드를 거쳐 다른 노드로 가는 비용을 계산하여 최단 거리 테이블을 갱신한다.
 * 5. 3번과 4번을 반복한다.
 */
public class DijkstraSimple {

    static class Node {

        private int index; // 정점의 번호
        private int cost; // 정점까지의 거리리

        public Node(int index, int distance) {
            this.index = index;
            this.cost = distance;
        }

    }

    // 노드의 갯수(N), 간선의 갯수(M), 시작 노드 번호(Start)
    // 노드의 갯수는 최대 100,000 개라고 가정
    static int N, M, Start;

    // 각 노드에 연결되어 있는 노드에 대한 정보를 담는 배열
    // 인접 리스트이 형태로 생성한다.
    static ArrayList<ArrayList<Node>> adjList = new ArrayList<ArrayList<Node>>();

    // 방문(해당 정점까지의 탐색이 완료 / 현재 기준 정점) 배열
    static boolean[] visited = new boolean[100001];

    // 최단거리 테이블
    static int[] d = new int[100001];

    // 방문하지 않은 노드 중에서, 가장 최단 거리가 짧은 노드의 번호를 리턴

    /**
     * 내 생각 정리
     * 다익스트라 알고리즘은 양의 가중치를 가진 상황에 한해서 유효하게 작동한다
     * 따라서 현재 출발하는 정점에서 가장 짧은 길을 택하는 것 (현재의 정점에 도달하기까지도 계속 짧은 길을 택했다.)이 최단 경로를 얻는 것이다
     */
    static int getSmallestNode() {
        int minDist = Integer.MAX_VALUE;
        int index = 0; // 가장 최단 거리가 짧은 노드의 인덱스
        for (int i = 1; i <= N; i++) {
            if (d[i] < minDist && !visited[i]) {
                minDist = d[i];
                index = i;
            }
        }
        return index;
    }

    static void dijkstra(int start) {
        // 시작 노드에 대한 초기화
        d[start] = 0;
        visited[start] = true;

        for (int j = 0; j < adjList.get(start).size(); j++) {
            d[adjList.get(start).get(j).index] = adjList.get(start).get(j).cost;
        }

        // 시작 노드를 제외한 전체 n-1개의 노드에 대한 반복
        for (int i = 0; i < N - 1; i++) {
            // 현재 최단 거리가 가장 짧은 노드를 꺼내서, 방문 처리
            int now = getSmallestNode();
            visited[now] = true;
            // 현재 노드와 연결된 다른 노드를 확인
            for (int j = 0; j < adjList.get(now).size(); j++) {
                int cost = d[now] + adjList.get(now).get(j).cost;
                // 현재 노드를 거쳐서 다른 노드로 이동하는거리가 짧은 경우
                if (cost < d[adjList.get(now).get(j).index]) {
                    d[adjList.get(now).get(j).index] = cost;
                }
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();
        M = sc.nextInt();
        Start = sc.nextInt();

        // 그래프 초기화
        for (int i = 0; i <= N; i++) {
            // 인접 리스트 구현을 위해 리스트 안의 각각의 값에 List 선언
            adjList.add(new ArrayList<Node>());
        }

        // 모든 간선 정보를 인접 리스트에 삽입
        for (int i = 0; i < M; i++) {
            int from = sc.nextInt();
            int to = sc.nextInt();
            int dist = sc.nextInt();

            adjList.get(from).add(new Node(to, dist));
        }

        // 최단거리 테이블을 모두 무한으로 초기화
        Arrays.fill(d, Integer.MAX_VALUE);

        // 다익스트라 알고리즘을 수행
        dijkstra(Start);

        for (int i = 1; i <= N; i++) {
            System.out.printf("From %d To %d distance : ", Start, i, d[i]);

        }
    }
}
