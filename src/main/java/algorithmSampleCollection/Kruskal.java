package algorithmSampleCollection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * 1. 간선 데이터를 비용에 따라 오름차순으로 정렬한다.
 * 2. 간선을 하나씩 확인하며 현재의 간선이 사이클을 발생시키는지 확인한다.
 * 1) 사이클이 발생하지 않는 경우 최소 신장 트리에 포함시킨다.
 * 2) 사이클이 발생하는 경우 최소 신장 트리에 포함시키지 않는다.
 * 3. 모든 간선에 대해 2번을 반복한다
 */
public class Kruskal {
    static class Edge implements Comparable<Edge> {

        int distance;
        int nodeA;
        int nodeB;

        public Edge(int distance, int nodeA, int nodeB) {
            this.distance = distance;
            this.nodeA = nodeA;
            this.nodeB = nodeB;
        }

        @Override
        public int compareTo(Edge o) {
            return this.distance - o.distance;
        }
    }

    // 노드의 갯수 V와 간선의 갯수 E
    // 노드의 갯수는 최대 100,000개라고 가정
    static int V, E;
    // 부모 테이블 초기화 하기
    static int[] parent = new int[100001];
    // 모든 간선을 담을 리스트와 최종 비용을 담을 변수
    static ArrayList<Edge> edges = new ArrayList<>();
    static int result = 0;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        V = sc.nextInt();
        E = sc.nextInt();

        // 부모 테이블에서 부모를 자신으로 초기화
        for (int i = 1; i <= V; i++) {
            parent[i] = i;
        }

        // 모든 간선에 대한 정보를 입력 받기
        for (int i = 0; i < E; i++) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            int cost = sc.nextInt();
            edges.add(new Edge(cost, a, b));
        }

        // 간선을 비용순으로 정렬
        Collections.sort(edges);

        // 간선을 하나씩 확인하며
        for (int i = 0; i < edges.size(); i++) {
            // 사이클이 발생하지 않는 경우에만 집합에 포함
            if (findParent(edges.get(i).nodeA) != findParent(edges.get(i).nodeB)) {
                unionParent(edges.get(i).nodeA, edges.get(i).nodeB);
                result += edges.get(i).distance;
            }
        }

        System.out.println(result);
    }

    // 특정 원소가 속한 집합을 찾기
    static int findParent(int x) {
        // 루트 노드가 아니라면, 루트 노드를 찾을 때까지 재귀적으로 호출
        if (x == parent[x])
            return x;
        return parent[x] = findParent(parent[x]);
    }

    // 두 원속가 속한 집합을 표시
    static void unionParent(int a, int b) {
        a = findParent(a);
        b = findParent(b);
        if (a < b)
            parent[b] = a;
        else
            parent[a] = b;
    }


}
