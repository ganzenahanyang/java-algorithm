package algorithmSampleCollection.sorting;

/**
 * 특정한 조건이 부합할때만 사용할 수 있다.
 * 데이터의 크기 범위가 제한되어 정수 형태로 표현할 수 있을때 사용 가능하다.
 * 데이터의 갯수가 N, 데이터(양수) 중 최댓값이 K일때 최악의 경우에도 O(N + K)를 보장한다.
 * <p>
 * 데이터를 하나씩 확인하며 각 데이터가 몇번씩 등장했는지 횟수를 기록한다.
 */
public class CountingSort {
    // 모든 원소의 값이 0 ~ 9 사이라고 가정
    static int[] arr = {7, 5, 9, 0, 3, 1, 6, 2, 9, 1, 4, 8, 0, 5, 2};

    public static void main(String[] args) {
        countingSort();
    }

    static void countingSort() {
        // 등장 횟수를 기록할 배열
        int[] counts = new int[10];

        // 각 데이터의 등장 횟수 세기
        for (int i = 0; i < arr.length; i++) {
            counts[arr[i]]++;
        }
        // 각 데이터별 등장 횟수 만큼 출력
        for (int i = 0; i < counts.length; i++) {
            for (int j = 0; j < counts[i]; j++) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }
}
