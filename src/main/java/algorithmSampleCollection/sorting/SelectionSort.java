package algorithmSampleCollection.sorting;

import java.util.Arrays;

/**
 * 처리되지 않은 데이터 중에서 가장 작은 데이터를 "선택"해 맨 앞에 있는 데이터와 바꾸는 것을 반복한다.
 * TimeComplexity = O(n^2)
 */

public class SelectionSort {

    static int[] arr = {7, 5, 9, 0, 3, 1, 6, 2, 4, 8};

    public static void main(String[] args) {
        SelectionSort();
        Arrays.stream(arr).forEach(v -> System.out.print(v + " "));
        //System.out.println();
    }

    static void SelectionSort() {
        for (int i = 0; i < arr.length; i++) {
            // 시작점이 min 이라고 가정
            int minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                // min 값의 인덱스 갱신
                if (arr[minIndex] > arr[j]) {
                    minIndex = j;
                }
            }
            // SWAP
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
    }
}
