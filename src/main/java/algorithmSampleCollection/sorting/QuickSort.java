package algorithmSampleCollection.sorting;

import java.util.Arrays;

/**
 * 기준 데이터를 설정하고 그 기준보다 큰 데이터와 작은 데이터의 위치를 바꾸는 방법
 * 기준 데이터(pivot)은 보통 첫 번째 데이터를 기준으로 한다.
 * <p>
 * Time Complexity = O(nlogn) / worst = O(n^2)
 */
public class QuickSort {

    static int[] arr = {7, 5, 9, 0, 3, 1, 6, 2, 4, 8};


    public static void main(String[] args) {
        quickSort(0, arr.length - 1);
        Arrays.stream(arr).forEach(v -> System.out.print(v + " "));
    }


    static void quickSort(int start, int end) {
        if (start >= end)
            return;
        int pivot = start;
        int left = start + 1;
        int right = end;

        // left와 right가 서로 교차하기 전까지
        while (left <= right) {
            // 피벗보다 큰 데이터를 찾을 때 까지 반복
            while (left <= end && arr[left] <= arr[pivot]) {
                left++;
            }
            // 피벗보다 작은 데이터를 찾을 때까지 반복
            while (right > start && arr[right] >= arr[pivot]) {
                right--;
            }

            // left와 right이 엇갈렸다면 작은 데이터와 피벗을 교체 (해당 턴의 정렬 종료)
            // 피벗과 left의 값을 교체하므로써, 바뀐 pivot의 위치를 기준으로 왼쪽에는 pivot보다 작은 값 오른쪽에는 pivot보다 큰 값이 위치하게 된다.
            if (left > right) {
                int temp = arr[pivot];
                arr[pivot] = arr[right];
                arr[right] = temp;
            }
            // 엇갈리지 않았다면 작은 데이터와 큰 데이터를 교체
            else {
                int temp = arr[left];
                arr[left] = arr[right];
                arr[right] = temp;
            }
        }
        // 분할 이후 왼쪽 부분과 오른쪽 부분에서 각각 정렬 수행
        quickSort(right + 1, end);
        quickSort(start, right - 1);
    }
}
