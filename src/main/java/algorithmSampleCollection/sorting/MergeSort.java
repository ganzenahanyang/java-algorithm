package algorithmSampleCollection.sorting;

import java.util.Arrays;

/**
 * Time Complexity : O(nlogn)
 * 쪼개기 위해서 임시 공간이 필요하고 이는 메모리를 먹게됨
 */
public class MergeSort {

    public static void main(String[] args) {
        int[] arr = {3, 9, 4, 7, 5, 0, 1, 6, 8, 2};
        mergeSort(arr);
        Arrays.stream(arr).forEach(v -> System.out.print(v + " "));
    }

    private static void mergeSort(int[] arr) {
        // 배열의 크기만큼 임시 저장소를 만든다
        int[] temp = new int[arr.length];
        divide(arr, temp, 0, arr.length - 1);
    }

    private static void divide(int[] arr, int[] temp, int start, int end) {
        // 시작이 끝보다 작을 때까지만 호출
        if(start<end){
            // 나눌 기준을 정한다.
            int mid = (start + end) / 2;
            // 중간을 기준으로 앞과 뒤로 나누어 재귀를 돈다.
            divide(arr, temp, start, mid);
            divide(arr, temp, mid + 1, end);
            // 모든 재귀가 끝나면 가운데를 기준으로 왼쪽가 오른쪽이 각각 정렬되어 있다.
            merge(arr, temp, start, mid, end);
        }
    }

    private static void merge(int[] arr, int[] temp, int start, int mid, int end) {
        // 임시 저장 배열에 원래 배열 복사
        for(int i = start ; i <= end ; i++){
            temp[i] = arr[i];
        }
        // 중간 지점을 기준으로 왼쪽 오른쪽에서 각각 index를 움직여야하므로 part1, part2 선언
        int part1 = start;
        int part2 = mid + 1;
        // 원본 배열에 저장할 위치의 초깃값 = start(왼쪽 파트의 가장 첫번째 인덱스
        int index = start;
        // 왼쪽 배열을 다 돌거나 오른쪽 배열을 다 돌 때까지 반복
        while(part1 <= mid && part2 <= end){
            // 왼쪽, 오른쪽 부분 배열을 비교하면서 작은 값을 원본 배열에 저장
            if(temp[part1] <= temp[part2]){
                arr[index] = temp[part1++];
            }else{
                arr[index] = temp[part2++];
            }
            index++;
        }
        // 행여, part1(왼쪽 부분 배열에 남은 값이 있을 수 있으므로 추가로 반복문 진행
        for(int i = 0 ; i <= mid - part1; i++){
            arr[index + i] = temp[part1 + i];
        }
        // 오른쪽 파트는 진행할 필요가 없다.
        // 최종 배열의 뒤에 이미 자리해있다.
    }
}
