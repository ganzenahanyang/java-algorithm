package algorithmSampleCollection.sorting;

import java.util.Arrays;

/**
 * 앞에서부터 다음 데이터와 비교하며 다음 값이 더 작을 경우 현재의 값과 위치를 바꾼다.
 * 이 과정을 수행하면 배열의 끝부터 큰 값이 위치하면서 정렬된다.
 * <p>
 * Time Complexity = O(N^2)
 */
public class BubbleSort {

    static int arr[] = {0, 2, 1, 5, 3, 4, 6, 9, 8, 7};

    public static void main(String[] args) {
        bubbleSort();
        Arrays.stream(arr).forEach(v -> System.out.print(v + " "));
    }

    static void bubbleSort() {
        for (int i = 0; i < arr.length; i++) {
            // 항상 다음 값(j+1)과 비교해야한다.
            // 또한 매 단계마다 가장 오른쪽부터 정렬된 값이 위치할 것이고, 해당 영역은 정렬에서 제외해야한다.
            // 따라서 j의 범위는 j < arr.length - 1 - i
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }
}
