package algorithmSampleCollection.sorting;

import java.util.Arrays;

/**
 * 처리되지 않은 데이터를 하나씩 골라 적절한 위치에 "삽입" 한다.
 * 선택정렬보다 구현 난이도는 어렵지만, 일반적으로 더 효율적으로 동작한다.
 *
 * 삽입 정렬의 시간 복잡도는 O(N^2)이며, 선택정렬과 마찬가지로 반복무닝 두 번 중첩된다.
 * 삽입 정렬은 현재 리스트의 데이터가 거의 정렬되어 있는 상태라면 매우 빠르게 동작한다.
 *
 * 최선의 경우 O(N)의 시간 복잡도를 가진다.(이미 정렬되어있는 경우)
 */
public class InsertionSort {

    static int[] arr = {7, 5, 9, 0, 3, 1, 6, 2, 4, 8};

    public static void main(String[] args) {
        InsertionSort();
        Arrays.stream(arr).forEach(v -> System.out.print(v + " "));
    }

    static void InsertionSort() {
        // i를 기준으로 앞에 있는 것들은 정렬이 완료된 것이라고 "가정"한다.
        // 따라서 index는 1부터 시작한다. (0번째 index는 정렬이 되어있다고 가정)
        for (int i = 1; i < arr.length; i++) {
            for (int j = i; j > 0; j--) {
                // 내 앞의 값이 나보다 크면 자리를 바꾼다.
                if (arr[j] < arr[j - 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = temp;
                }else{
                    // 내 앞의 값이 나보다 작으면 멈춘다.
                    // 내 앞은 모두 정렬이 되어있다고 가정한다.
                    // 땨라서 작은게 나왔다면 그 앞은 다 정렬되어있을거라는 확신이 있다.
                    break;
                }
            }
        }
    }
}
