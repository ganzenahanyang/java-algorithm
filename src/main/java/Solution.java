

import java.util.ArrayList;
import java.util.HashMap;

public class Solution {

	static HashMap<String, String> hm = new HashMap<>();

	public ArrayList<String> solution(String[] record) {
		ArrayList<String> answer = new ArrayList<String>();
		for (int i = 0; i < record.length; i++) {
			String[] line = record[i].split(" ");
			String action = line[0];
			String uid = line[1];
			String name = "";
			if (line.length == 3) {
				name = line[2];
			}
			if (!name.isEmpty())
				hm.put(uid, name);
			StringBuilder sb = new StringBuilder();
			sb.append(uid);
			if (action.equals("Enter")) {
				sb.append(" 들어왔습니다.");
			} else if (action.equals("Leave")) {
				sb.append(" 나갔습니다.");
			}
			if (!action.equals("Change"))
				answer.add(sb.toString());
		}

		for (int i = 0; i < answer.size(); i++) {
			String uid = answer.get(i).split(" ")[0];
			String change = answer.get(i).replace(uid, hm.get(uid) + "님이");
			answer.set(i, change);
		}

		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		String[] record = { "Enter uid1234 Muzi", "Enter uid4567 Prodo", "Leave uid1234", "Enter uid1234 Prodo",
				"Change uid4567 Ryan" };
		s.solution(record);
	}

}
