import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComparatorTest {

	public static void main(String[] args) {
		List<People> list = new ArrayList<>();
		list.add(new People("방영우", 36));
		list.add(new People("김정민", 29));
		list.add(new People("유태욱", 30));
				
		Collections.sort(list);
		
		for(People p:list)
			System.out.printf("%s, %d  ", p.name, p.age);
		System.out.println();
	}

}

class People implements Comparable<People> {
	String name;
	int age;

	public People(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}

	@Override
	public int compareTo(People o) {
		if(this.age < o.age)
			return -1; // 음수일 경우 오름차순 양수일 경우 내림 차순
		else if(this.age > o.age)
			return 1;
		
		return this.age - o.age;
		
	}

}
