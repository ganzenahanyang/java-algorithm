package test;

import java.util.ArrayList;
import java.util.List;

public class CollectionTest {
	
	static class Pair{
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}
		
	}

	static List<Integer> intList = new ArrayList<>();
	static List<Integer> copyIntList = new ArrayList<>();
	static List<Pair> pairs = new ArrayList<>();
	static List<Pair> copyPairs = new ArrayList<>();

	public static void main(String[] args) {
		intList.add(0);
		intList.add(0);
		intList.add(0);
		
		copyIntList.addAll(intList);
		intList.set(0, 1);
		intList.add(3);
		
		System.out.println("intList");
		intList.forEach(v -> System.out.print(v +" "));
		System.out.println("\ncopyIntList");
		copyIntList.forEach(v -> System.out.print(v +" "));
		System.out.println();
		
		pairs.add(new Pair(0, 0));
		pairs.add(new Pair(1, 0));
		pairs.add(new Pair(2, 0));
		copyPairs.addAll(pairs);
		
		pairs.get(0).y = 1;
		pairs.get(0).x = 1;
		pairs.add(new Pair(3, 0));
		
		System.out.println("pairs");
		pairs.forEach(v -> System.out.printf("(%d, %d) ", v.y, v.x));
		System.out.println();
		System.out.println("copyPairs");
		copyPairs.forEach(v -> System.out.printf("(%d, %d) ", v.y, v.x));
		System.out.println();
	}
}
