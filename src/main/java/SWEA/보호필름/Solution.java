package SWEA.보호필름;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
 
public class Solution {
 
    static int[][] board;
    static int[][] copy;
    static int T, D, W, K;
    static int answer;
    static String PATH = "C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\SWEA\\보호필름\\input.txt";
 
    public static void main(String[] args) throws Exception {
        // System.setIn(new FileInputStream(PATH));
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        T = Integer.parseInt(br.readLine());
        for (int t = 1; t <= T; t++) {
            init(br);
            perm(0, 0);
            System.out.printf("#%d %d\n", t, answer);
        }
    }
 
    private static void perm(int i, int cnt) {
        if (cnt >= answer)
            return;
        if (i == D) {
            if (check()) {
                answer = Math.min(answer, cnt);
 
            }
            return;
        }
        // 0개부터 D개까지 하나씩 다해보려고할때 참고
        perm(i + 1, cnt);
 
        paint(i, 0);
        perm(i + 1, cnt + 1);
 
        paint(i, 1);
        perm(i + 1, cnt + 1);
 
        for (int j = 0; j < W; j++) {
            board[i][j] = copy[i][j];
        }
 
    }
    // 시간 잡아먹는 도사
    private static boolean check() {
        for (int j = 0; j < W; j++) {
            int cnt = 1;
            boolean pass = false;
            for (int i = 0; i < D - 1; i++) {
                if (board[i][j] == board[i + 1][j]) {
                    ++cnt;
                } else {
                     cnt = 1;
                }
                if (cnt == K) {
                    pass = true;
                    break;
                }
            }
 
            if (!pass)
                return false;
        }
         return true;
     }
 
    private static void paint(int y, int n) {
        for (int j = 0; j < W; j++) {
            board[y][j] = n;
        }
    }
 
    private static void init(BufferedReader br) throws IOException {
        String[] s = br.readLine().split(" ");
        D = Integer.parseInt(s[0]);
        W = Integer.parseInt(s[1]);
        K = Integer.parseInt(s[2]); // 연속된게 K개 있어야함
 
        board = new int[D][W];
        // 메모리 감소 
        copy = new int[D][W];
        for (int i = 0; i < D; i++) {
            s = br.readLine().split(" ");
            for (int j = 0; j < W; j++) {
                board[i][j] = Integer.parseInt(s[j]);
                copy[i][j] = Integer.parseInt(s[j]);
            }
        }
        answer = Integer.MAX_VALUE;
    }
 
}