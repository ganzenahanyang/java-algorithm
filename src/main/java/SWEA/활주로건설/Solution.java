package SWEA.활주로건설;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Solution {

	static int T, N, X; // X는 경사로의 길이
	static int[][] board;
	static boolean[][] visit;
	static int answer;
	static final String PATH = "C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\SWEA\\활주로건설\\input.txt";

	public static void main(String[] args) throws Exception {
		//System.setIn(new FileInputStream(PATH));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		T = Integer.parseInt(br.readLine());

		for (int t = 1; t <= T; t++) {
			init(br);
			vertical();
			horizontal();
			System.out.printf("#%d %d\n", t, answer);
		}
	}

	private static void vertical() {
		// 수직 이동
		visit = new boolean[N][N];
		int cnt = 0;
		for (int j = 0; j < N; j++) {
			int y = 0;
			int x = j;

			while (y < N - 1) {
				if (board[y][x] == board[y + 1][x]) {
					++y;
				} else {
					int h = board[y][x];
					int hh = board[y + 1][x];
					if (Math.abs(h - hh) == 1) {
						boolean canMove = true;
						// down
						if (h > hh) {
							for (int k = 1; k <= X; k++) {
								if (y+ k >= N || board[y + k][x] != hh) {
									canMove = false;
									break;
								}
							}
							if (canMove) {
								for (int k = 1; k <= X; k++) {
									visit[y + k][j] = true;
								}
								y += X;
							} else {
								break;
							}
						}
						// up
						else {
							for (int k = 0; k < X; k++) {
								if (y - k < 0 || board[y - k][x] != h || visit[y - k][x]) {
									canMove = false;
									break;
								}
							}

							if (canMove) {
								for (int k = 0; k < X; k++) {
									visit[y - k][x] = true;
								}
								++y;

							} else {
								break;
							}
						}
					}else {
						break;
					}
				}
			}
			if (y == N - 1)
				++cnt;
		}
		answer += cnt;
	}

	private static void horizontal() {
		// 수평 이동
		visit = new boolean[N][N];
		int cnt = 0;
		for (int i = 0; i < N; i++) {

			int y = i;
			int x = 0;

			while (x < N - 1) {
				// 높이 같음
				if (board[y][x] == board[y][x + 1]) {
					++x;
				}
				// 높이 다름
				else {
					int h = board[y][x];
					int hh = board[y][x + 1];
					// 높이 차 1일 때
					if (Math.abs(h - hh) == 1) {
						// 내려가야한다.
						boolean canMove = true;
						if (h > hh) {
							
							for (int k = 1; k <= X; k++) {
								if (x + k >= N || board[y][x + k] != hh) {
									canMove = false;
									break;
								}
							}
							if (canMove) {
								for (int k = 1; k <= X; k++) {
									visit[y][x + k] = true;
								}
								x += X;
							} else {
								break;
							}
						}
						// 올라가야한다.
						else {
							for (int k = 0; k < X; k++) {
								if (x - k < 0 || board[y][x - k] != h || visit[y][x - k]) {
									canMove = false;
									break;
								}
							}

							if (canMove) {
								for (int k = 0; k < X; k++) {
									visit[y][x - k] = true;
								}
								++x;
							} else {
								break;
							}
						}
					}else {
						break;
					}
				}
			}
			// 끝까지 간거임
			if (x == N - 1)
				cnt++;
		}
		answer += cnt;

	}

	private static void init(BufferedReader br) throws Exception {
		String[] line = br.readLine().split(" ");
		N = Integer.parseInt(line[0]);
		X = Integer.parseInt(line[1]);
		board = new int[N][N];

		for (int i = 0; i < N; i++) {
			line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}
		answer = 0;
	}

}
