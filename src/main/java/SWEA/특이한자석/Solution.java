package SWEA.특이한자석;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

public class Solution {

	static class Cmd {
		int no; // 보정 필요
		int dir; // 1 시계방향 -1 반시계 방향

		public Cmd(int no, int dir) {
			super();
			this.no = no;
			this.dir = dir;
		}

	}

	static int[][] board;
	static int[][] copy;
	static int T, K;
	static ArrayList<Cmd> cmds;
	static int answer;

	public static void main(String[] args) throws Exception {
//		System.setIn(
//				new FileInputStream("C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\SWEA\\특이한자석\\input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			init(br);
			for (int i = 0; i < cmds.size(); i++) {
				Cmd c = cmds.get(i);
				rotate(c);

			}

			getScore();
			System.out.printf("#%d %d\n", t, answer);
		}
	}

	private static void getScore() {
		for (int i = 0; i < 4; i++) {
			if (board[i][0] == 1) {
				answer += (int) Math.pow(2, i);
			}
		}
	}

	private static void rotate(Cmd c) {
		int even, odd; //
		if (c.no % 2 == 0) { // 0아니면 2
			even = c.dir;
			odd = c.dir * -1;
		} else { // 1아니면 3
			odd = c.dir;
			even = c.dir * -1;
		}
		copy = new int[4][8]; // 여기다 새로 해야함
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 8; j++) {
				copy[i][j] = board[i][j];
			}
		}
		// 대상 먼저 돌리기
		Deque<Integer> dq = new ArrayDeque<>();
		for (int j = 0; j < 8; j++) {
			dq.add(board[c.no][j]);
		}
		if (c.dir == 1) { // 시계 방향
			dq.addFirst(dq.pollLast());
		} else { // 반시계 방향
			dq.addLast(dq.pollFirst());
		}

		for (int j = 0; j < 8; j++) {
			copy[c.no][j] = dq.pollFirst();
		}

		// 대상 기준 왼쪽
		for (int i = c.no - 1; i >= 0; i--) {
			if (i < 0)
				break;
			// 다른 극일 때
			if (board[i][2] != board[i + 1][6]) {
				int dir;
				if (i % 2 == 0) {
					dir = even;
				} else {
					dir = odd;
				}
				for (int j = 0; j < 8; j++) {
					dq.add(board[i][j]);
				}
				if (dir == 1) {
					dq.addFirst(dq.pollLast());
				} else {
					dq.addLast(dq.pollFirst());
				}

				for (int j = 0; j < 8; j++) {
					copy[i][j] = dq.pollFirst();
				}

			} else {
				break;
			}
		}

		// 대상 기준 오른쪽
		for (int i = c.no + 1; i < 4; i++) {
			if (i >= 4)
				break;
			// 다른 극일 때
			if (board[i][6] != board[i - 1][2]) {
				int dir;
				if (i % 2 == 0) {
					dir = even;
				} else {
					dir = odd;
				}
				for (int j = 0; j < 8; j++) {
					dq.add(board[i][j]);
				}
				if (dir == 1) {
					dq.addFirst(dq.pollLast());
				} else {
					dq.addLast(dq.pollFirst());
				}

				for (int j = 0; j < 8; j++) {
					copy[i][j] = dq.pollFirst();
				}

			} else {
				break;
			}
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 8; j++) {
				board[i][j] = copy[i][j];
			}
		}
	}

	private static void init(BufferedReader br) throws Exception, IOException {
		K = Integer.parseInt(br.readLine());
		board = new int[4][8];
		for (int i = 0; i < 4; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < 8; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		cmds = new ArrayList<>();
		for (int i = 0; i < K; i++) {
			String[] line = br.readLine().split(" ");
			int no = Integer.parseInt(line[0]) - 1;
			int dir = Integer.parseInt(line[1]);
			cmds.add(new Cmd(no, dir));
		}

		answer = 0;

	}

}
