package SWEA.등산로조정;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Solution {
	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}
	}

	static final String PATH = "C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\SWEA\\등산로조정\\input.txt";
	static int T, N, K;
	static int answer;
	static int[][] board;
	static int[][] visit;
	static ArrayList<Pair> tops;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };

	public static void main(String[] args) throws Exception {
		// System.setIn(new FileInputStream(PATH));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			init(br);
			solve();
			System.out.printf("#%d %d\n", t, answer);
		}

	}

	private static void solve() {
		for (int k = 0; k < tops.size(); k++) {
			Pair now = tops.get(k);
			visit = new int[N][N];
			DFS(1, false, now.y, now.x);

		}
	}

	private static void DFS(int depth, boolean dig, int y, int x) {
		int temp = visit[y][x];
		visit[y][x] = depth;
		for (int i = 0; i < 4; i++) {
			int yy = y + dirY[i];
			int xx = x + dirX[i];
			if (yy < 0 || xx < 0 || yy >= N || xx >= N)
				continue;
			// 이미 더 긴시간으로 다녀간 경우
			if (visit[yy][xx] != 0)
				continue;
			// 다음 칸이 높으면서 이미 한번 파냈으면 이동 못함
			if ((board[y][x] <= board[yy][xx]) && dig) {
				continue;
			}
			// 파야할 경우
			if (board[yy][xx] >= board[y][x]) {
				for (int k = 1; k <= K; k++) {
					// 파고나서 이동이 가능해지면
					if (board[yy][xx] - k < board[y][x]) {
						board[yy][xx] -= k;
						// dig는 false 였을꺼니까 반대로 바꿔야함
						DFS(depth + 1, !dig, yy, xx);
						board[yy][xx] += k;

					}
				}

			} else {
				DFS(depth + 1, dig, yy, xx);
			}
		}

		answer = Math.max(answer, depth);
		visit[y][x] = temp;

	}

	private static void init(BufferedReader br) throws Exception {
		String[] s = br.readLine().split(" ");
		N = Integer.parseInt(s[0]);
		K = Integer.parseInt(s[1]);

		board = new int[N][N];

		int top = 0;
		for (int i = 0; i < N; i++) {
			s = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(s[j]);
				top = Math.max(top, board[i][j]);
			}
		}

		tops = new ArrayList<>();
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (board[i][j] == top) {
					tops.add(new Pair(i, j));
				}
			}
		}
		answer = 0;

	}

}
