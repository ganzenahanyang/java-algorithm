package SWEA.홈방범서비스;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Queue;

public class Solution {

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;

		}

	}

	static int[][] board;
	static int[][] visit;
	static int answer; // 손해를 보지 않았을때 서비스를 받는 집의 최대수
	static int T, N, M;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };

//13:25
	public static void main(String[] args) throws Exception {
		System.setIn(
				new FileInputStream("C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\SWEA\\홈방범서비스\\input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		T = Integer.parseInt(br.readLine());
		for (int t = 1; t <= T; t++) {
			init(br);

			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					visit = new int[N][N];
					// visit에 K에 따른 영역 표시
					BFS(i, j);
					updateMax();
				}
			}

			System.out.printf("#%d %d\n", t, answer);
		}
	}

	private static void updateMax() {
		// K에 따른 커버하는 집 체크
		int[] cnts = new int[N * 2];
		
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (board[i][j] != 0)
					++cnts[visit[i][j]];
			}
		}
		int cnt = 0;
		// 한 지점을 기준으로 커버 영역이 늘어갈때마다 커버되는 집 숫자를 더해줌
		for (int i = 1; i < N * 2; i++) {
			cnt += cnts[i];
			int manageFee = i * i + (i - 1) * (i - 1);
			int serviceFee = M * cnt;
			// 손해보지 않는다는 지불비용 >= 운영비용
			if (serviceFee >= manageFee) {
				answer = Math.max(answer, cnt);
			}
		}

	}

	static void BFS(int i, int j) {
		Queue<Pair> q = new ArrayDeque<>();
		q.add(new Pair(i, j));
		visit[i][j] = 1;
		while (!q.isEmpty()) {
			Pair now = q.poll();
			for (int d = 0; d < 4; d++) {
				int yy = now.y + dirY[d];
				int xx = now.x + dirX[d];
				if (yy < 0 || xx < 0 || yy >= N || xx >= N || visit[yy][xx] != 0)
					continue;
				visit[yy][xx] = visit[now.y][now.x] + 1;
				q.add(new Pair(yy, xx));
			}
		}
	}

	private static void init(BufferedReader br) throws Exception {
		String[] nm = br.readLine().split(" ");
		N = Integer.parseInt(nm[0]);
		M = Integer.parseInt(nm[1]);

		board = new int[N][N];
		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		answer = 0;
	}

}
