package boj.ABCDE_13023;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

	static int N, M;
	static ArrayList<Integer>[] list;
	static boolean visit[];
	static boolean isExist = false;

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] line = br.readLine().split(" ");
		N = Integer.parseInt(line[0]);
		M = Integer.parseInt(line[1]);

		list = new ArrayList[N];

		for (int i = 0; i < N; i++) {
			list[i] = new ArrayList<>();
		}
		for (int i = 0; i < M; i++) {
			line = br.readLine().split(" ");
			int n1 = Integer.parseInt(line[0]);
			int n2 = Integer.parseInt(line[1]);
			if (!list[n1].contains(n2))
				list[n1].add(n2);
			if (!list[n2].contains(n1))
				list[n2].add(n1);
		}

		for (int i = 0; i < N; i++) {
			visit = new boolean[N];
			visit[i] = true;
			DFS(0, i);
			if (isExist)
				break;
		}

		System.out.println(isExist ? 1 : 0);

	}

	static void DFS(int depth, int index) {
		if (depth == 4) {
			isExist = true;
			return;
		}

		for (int i = 0; i < list[index].size(); i++) {
			if (visit[list[index].get(i)])
				continue;
			visit[list[index].get(i)] = true;
			DFS(depth + 1, list[index].get(i));
			if (isExist)
				return;
			visit[list[index].get(i)] = false;
		}
	}

}
