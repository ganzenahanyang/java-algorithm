package boj.단어수학_1339;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Main {

    static int N;
    static ArrayList<String> words = new ArrayList<>();
    static HashSet<Character> hs = new HashSet<>();
    static HashMap<Character, Character> hm = new HashMap<>();
    static ArrayList<Character> chars = new ArrayList<>();
    static char[] number = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    static char[] candi;
    static boolean[] visit = new boolean[10];
    static int answer = 0;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        N = Integer.parseInt(br.readLine());

        for (int i = 0; i < N; i++) {
            String word = br.readLine();
            words.add(word);
            for (int j = 0; j < word.length(); j++) {
                hs.add(word.charAt(j));
            }
        }

        chars.addAll(hs);
        candi = new char[chars.size()];

        calculate(0);
        System.out.println(answer);
    }

    static void calculate(int depth) {
        if (depth == chars.size()) {
            // hm.clear();
            for (int i = 0; i < chars.size(); i++) {
                hm.put(chars.get(i), candi[i]);
            }
            converter();

            return;
        }

        for (int i = 0; i < 10; i++) {
            if (!visit[i]) {
                visit[i] = true;
                candi[depth] = number[i];
                calculate(depth + 1);
                visit[i] = false;
            }
        }

    }

    static void converter(){
        int sum = 0;
        for(int i = 0 ; i < words.size() ; i++){
            char[] temp =  words.get(i).toCharArray();
            for(int j = 0 ; j < temp.length ; j++){
                temp[j] = hm.get(temp[j]);
            }
            sum += Integer.parseInt(String.valueOf(temp));
        }
        answer = Math.max(answer, sum);
    }
}
