package boj.모든순열_10974;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {
    static int[] candidate; // 순열의 결과가 담길 배열
    static int[] nums;
    static boolean[] visit;
    static int N;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        N = Integer.parseInt(br.readLine());

        nums = new int[N];
        visit = new boolean[N];
        candidate = new int[N];

        for (int i = 0; i < N; i++) {
            nums[i] = i + 1;
        }

        permutation(0);
    }

    static void permutation(int depth) {
        if (depth == N) {
            Arrays.stream(candidate).forEach(v -> System.out.print(v + " "));
            System.out.println();
            return;
        }

        // 항상 0부터 시작하여 visit[i]를 체크
        for (int i = 0; i < N; i++) {
            if (!visit[i]) {
                visit[i] = true;
                // depth 를 인덱스로 사용하는 것을 잊지말자
                candidate[depth] = nums[i];
                permutation(depth + 1);
                visit[i] = false;
            }
        }
    }
}
