package boj.빙산_2573;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
	static int N, M;
	static int[][] board;
	static int[][] copyBoard;
	static boolean[][] visit; // 매번 나뉘어졌는지 체크
	static int day = 0;
	static int iceCount = 0;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static Queue<Pair> devideQ = new LinkedList<>(); //

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] nm = br.readLine().split(" ");

		N = Integer.parseInt(nm[0]);
		M = Integer.parseInt(nm[1]);

		board = new int[N][M];
		copyBoard = new int[N][M];
		visit = new boolean[N][M];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < M; j++) {
				board[i][j] = copyBoard[i][j] = Integer.parseInt(line[j]);

			}
		}

		while (true) {
			int devideCount = 0;
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < M; j++) {
					if (board[i][j] != 0 && !visit[i][j]) {
						devideQ.add(new Pair(i, j));
						divideChecker();
						devideCount++;
					}
				}
			}

			// 나뉘어진 갯수가 2 이상이면 끝
			if (devideCount >= 2)
				break;
			// 나뉘어진 갯수가 없으면 빙산이 없다는 것
			else if (devideCount < 1) {
				System.out.println(0);
				return;
			}

			day++;

			for (int i = 0; i < N; i++) {
				for (int j = 0; j < M; j++) {
					if (board[i][j] != 0) {
						melt(i, j);
					}
				}
			}

			// 다음 로직을 위해 초기화
			reset();

		}

		System.out.println(day);
		br.close();
	}

	static void melt(int y, int x) {

		int waterCount = 0;

		for (int i = 0; i < 4; i++) {
			int yy = y + dirY[i];
			int xx = x + dirX[i];

			if (board[yy][xx] == 0)
				waterCount++;

		}
		int nextStatus = board[y][x] - waterCount;
		copyBoard[y][x] = nextStatus >= 0 ? nextStatus : 0;

	}

	static void divideChecker() {
		while (!devideQ.isEmpty()) {
			Pair now = devideQ.poll();
			int y = now.y;
			int x = now.x;
			visit[y][x] = true;

			for (int i = 0; i < 4; i++) {
				int yy = y + dirY[i];
				int xx = x + dirX[i];

				if (yy < 0 || xx < 0 || yy >= N || xx >= M || visit[yy][xx] || board[yy][xx] == 0)
					continue;

				visit[yy][xx] = true;
				devideQ.add(new Pair(yy, xx));
			}
		}
	}

	static void reset() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				board[i][j] = copyBoard[i][j]; // 녹은 빙산의 상태값을 원래 보드로 가져옴
			}
		}
		visit = new boolean[N][M]; // 디바이드 체크를 위한 방문 배열 초기화
		devideQ.clear();
	}

}

class Pair {
	int y, x;

	public Pair(int y, int x) {
		super();
		this.y = y;
		this.x = x;
	}

}
