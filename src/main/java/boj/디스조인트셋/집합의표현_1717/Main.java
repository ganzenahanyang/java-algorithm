package boj.디스조인트셋.집합의표현_1717;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

	static int N, M;
	static int[] arr; // 부모 테이블

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] line = br.readLine().split(" ");
		N = Integer.parseInt(line[0]);
		M = Integer.parseInt(line[1]);
		arr = new int[N + 1];
		
		// 자기 자신으로 부모 테이블 초기화
		for (int i = 0; i < N + 1; i++) {
			arr[i] = i;
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < M; i++) {
			line = br.readLine().split(" ");
			int action = Integer.parseInt(line[0]);
			int node1 = Integer.parseInt(line[1]);
			int node2 = Integer.parseInt(line[2]);
			
			if (action == 0) {
				union(node1, node2);
			} else {
				if (findParent(node1) == findParent(node2)) {
					sb.append("YES").append("\n");
				} else {
					sb.append("NO").append("\n");
				}
			}
		}
		System.out.println(sb);

	}

	// 자신이 속한 집합을 찾기
	static int findParent(int node) {
		if (arr[node] != node) {
			// 루트 노드를 찾을 때까지 재귀
			//
			return arr[node] = findParent(arr[node]);
		}
		return node;
	}

	// 병합
	static void union(int node1, int node2) {
		node1 = findParent(node1);
		node2 = findParent(node2);
		
		if (node1 < node2) {
			arr[node2] = node1;
		} else {
			arr[node1] = node2;
		}
	}

}
