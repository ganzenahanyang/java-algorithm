package boj.디스조인트셋.동방프로젝트small_14594;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	static int N, M;
	static int start, end;
	static int[] arr;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		M = Integer.parseInt(br.readLine());
		arr = new int[N];
		// 자기 자신을 부모로 갖기
		for (int i = 0; i < N; i++) {
			arr[i] = i;
		}

		for (int m = 0; m < M; m++) {
			String[] s = br.readLine().split(" ");
			start = Integer.parseInt(s[0]) - 1;
			end = Integer.parseInt(s[1]) - 1;
			for (int i = start; i <= end; i++) {
				if (findParent(start) != findParent(i))
					union(start, i);

			}
		}
		int cnt = 0;
		for (int i = 0; i < N; i++) {
			if (arr[i] == i)
				cnt++;
		}
		System.out.println(cnt);
	}

	public static int findParent(int i) {
		if (arr[i] != i) {
			return arr[i] = findParent(arr[i]);
		}
		return i;

	}

	public static void union(int a, int b) {
		a = findParent(a);
		b = findParent(b);
		// 더 큰 루트 노드가 더 작은 노트를 루트로 삼는게 관행
		if (a < b) {
			arr[b] = a;
		} else {
			arr[a] = b;
		}
	}

}
