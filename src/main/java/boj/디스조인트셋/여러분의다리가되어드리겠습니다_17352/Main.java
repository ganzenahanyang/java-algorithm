package boj.디스조인트셋.여러분의다리가되어드리겠습니다_17352;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	static int N;
	static int[] arr;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		arr = new int[N + 1];

		for (int i = 1; i < N + 1; i++) {
			arr[i] = i;
		}

		for (int i = 0; i < N - 2; i++) {
			String[] s = br.readLine().split(" ");
			int start = Integer.parseInt(s[0]);
			int end = Integer.parseInt(s[1]);

			union(start, end);
		}
		int first = -1, second = -1;
		for(int i = 1 ; i < N + 1 ; i++) {
			if(arr[i] == i) {
				if(first == -1) {
					first = i;
				}else {
					second = i;
					break;
				}
			}
		}
		
		System.out.println(first + " " + second);

	}

	static int findParent(int x) {
		if (arr[x] != x)
			return arr[x] = findParent(arr[x]);
		return x;
	}

	static void union(int a, int b) {
		a = findParent(a);
		b = findParent(b);

		if (a < b) {
			arr[b] = a;
		} else {
			arr[a] = b;
		}
	}

}
