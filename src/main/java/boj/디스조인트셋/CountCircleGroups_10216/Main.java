package boj.디스조인트셋.CountCircleGroups_10216;

import java.io.*;
import java.util.*;

public class Main {

	static class House {
		int no, y, x, range;

		public House(int no, int y, int x, int range) {
			this.no = no;
			this.y = y;
			this.x = x;
			this.range = range;
		}
	}

	static int T, N;
	static int[] parent;
	static ArrayList<House> houses;
	static int answer = 0;

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		T = Integer.parseInt(br.readLine());
		for (int t = 0; t < T; t++) {
			init(br);

			for (int i = 0; i < houses.size(); i++) {
				House now = houses.get(i);
				for (int j = i + 1; j < houses.size(); j++) {
					House next = houses.get(j);
					if (findParent(now.no) == findParent(next.no))
						continue;
					int gap = Math.abs(now.y - next.y) * Math.abs(now.y - next.y)
							+ Math.abs(now.x - next.x) * Math.abs(now.x - next.x);
					if (gap <= (now.range + next.range) * (now.range + next.range)) {
						union(now.no, next.no);
						answer--;
					}
				}
			}

			System.out.println(answer);
		}
	}

	static int findParent(int x) {
		if (parent[x] != x) {
			return parent[x] = findParent(parent[x]);
		}
		return x;
	}

	static void union(int a, int b) {
		a = findParent(a);
		b = findParent(b);

		if (b > a) {
			parent[b] = a;
		} else {
			parent[a] = b;
		}
	}

	static void init(BufferedReader br) throws Exception {
		N = Integer.parseInt(br.readLine());
		parent = new int[N];
		houses = new ArrayList<>();
		for (int i = 0; i < N; i++) {
			String[] s = br.readLine().split(" ");
			int y = Integer.parseInt(s[0]);
			int x = Integer.parseInt(s[1]);
			int depth = Integer.parseInt(s[2]);

			houses.add(new House(i, y, x, depth));
			parent[i] = i;
		}
		answer = N;
	}

}
