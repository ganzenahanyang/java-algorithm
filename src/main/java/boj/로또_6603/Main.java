package boj.로또_6603;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String[] line = br.readLine().split(" ");
            int n = Integer.parseInt(line[0]);
            if (n == 0)
                break;
            int[] numbers = new int[line.length - 1];
            boolean[] visit = new boolean[line.length - 1];

            for (int i = 1; i <= n; i++) {
                numbers[i - 1] = Integer.parseInt(line[i]);
            }

            getLotto(0, 0, numbers, visit);

            System.out.println();
        }
    }

    static void getLotto(int depth, int index, int[] numbers, boolean[] visit) {
        if (depth == 6) {
            for (int i = 0; i < visit.length; i++) {
                if (visit[i]) {
                    System.out.print(numbers[i] + " ");
                }
            }
            System.out.println();
            return;
        }

        for (int i = index; i < numbers.length; i++) {
            if (!visit[i]) {
                visit[i] = true;
                // 중복을 피하기 위해, 다음 depth 에서 이전의 수를 고려하지 않도록 i + 1번째부터 시작하도록 한다.
                getLotto(depth + 1, i + 1, numbers, visit);
                visit[i] = false;
            }
        }
    }
}
