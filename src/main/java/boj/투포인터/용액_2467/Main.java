package boj.투포인터.용액_2467;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {

	static int N;
	static ArrayList<Integer> answer = new ArrayList<>();
	static ArrayList<Integer> liquid = new ArrayList<>();
	static int sum = Integer.MAX_VALUE;

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		String[] line = br.readLine().split(" ");
		for (int i = 0; i < N; i++) {
			liquid.add(Integer.parseInt(line[i]));
		}
		Collections.sort(liquid, new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				// TODO Auto-generated method stub
				return Math.abs(o1) - Math.abs(o2);
			}

		});

		for (int i = 0; i < N - 1; i++) {
			int tempSum = liquid.get(i) + liquid.get(i + 1);
			if (Math.abs(tempSum - 0) < Math.abs(sum - 0)) {
				sum = tempSum;
				answer.clear();
				answer.add(liquid.get(i));
				answer.add(liquid.get(i + 1));
			}
			if (sum == 0)
				break;
		}
		Collections.sort(answer);
		for (Integer n : answer) {
			System.out.print(n + " ");
		}
		System.out.println();
		br.close();

	}

}
