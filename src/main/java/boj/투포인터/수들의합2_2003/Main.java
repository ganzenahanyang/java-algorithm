package boj.투포인터.수들의합2_2003;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	static int N, M;
	static int[] arr;
	static int answer = 0;
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] line = br.readLine().split(" ");
		N = Integer.parseInt(line[0]);
		M = Integer.parseInt(line[1]);
		arr = new int[N];
		line = br.readLine().split(" ");
		for (int i = 0; i < N; i++) {
			arr[i] = Integer.parseInt(line[i]);
		}
		
		int i = 0, j = 1, sum = 0;
		sum = arr[i];
		while(i < N && j < N) {
			if(sum > M) {
				sum -= arr[i++];
			}else if(sum < M) {
				sum += arr[j++];
			}else {
				answer++;
				sum -= arr[i++];
			}
		}
		
		while(i < N) {
			if(sum == M)
				answer++;
			sum -= arr[i++];
		}
		
		System.out.println(answer);
	}

}
