package boj.투포인터.먹을것인가먹힐것인가_7795;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

	static int T, A, B;
	static int[] listA, listB;
	static int answer;

	public static void main(String[] args) throws Exception, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		T = Integer.parseInt(br.readLine());
		for (int i = 0; i < T; i++) {
			init(br);
			count();

			System.out.println(answer);
		}

	}

	private static void count() {
		// 큰 수부터 비교
		int i = A - 1, j = B - 1;
		while (i >= 0 && j >= 0) {
			if (listA[i] > listB[j]) {
				answer += j + 1;
				--i;
			} else if (listA[i] < listB[j]) {
				--j;
			} else {
				--j;
			}
		}
		// j가 음수가 된 것은 B의 남은 모든 값들이 A보다 크기 때문이다.
		// i가 음수가 된 것은 모든 대소 비교를 끝냈기 때문이다.

	}

	private static void init(BufferedReader br) throws Exception {
		String[] ab = br.readLine().split(" ");
		A = Integer.parseInt(ab[0]);
		B = Integer.parseInt(ab[1]);

		listA = new int[A];
		listB = new int[B];

		String[] line = br.readLine().split(" ");
		for (int i = 0; i < A; i++) {
			listA[i] = Integer.parseInt(line[i]);
		}

		Arrays.sort(listA);

		line = br.readLine().split(" ");
		for (int i = 0; i < B; i++) {
			listB[i] = (Integer.parseInt(line[i]));
		}

		Arrays.sort(listB);

		answer = 0;
	}

}
