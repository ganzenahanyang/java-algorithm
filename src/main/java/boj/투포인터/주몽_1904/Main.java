package boj.투포인터.주몽_1904;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

	static int N,M;
	static int[] arr;
	static int answer = 0;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		M = Integer.parseInt(br.readLine()); // 둘이 합쳐서 나와야할 값 
		arr = new int[N];
		String[] s = br.readLine().split(" ");
		for (int i = 0; i < N; i++) {
			arr[i] = Integer.parseInt(s[i]);
		}
		Arrays.sort(arr);
		for(int i = 0 ; i < N - 1 ; i++) {
			if(arr[i] > M)
				break;
			for(int j = i + 1 ; j < N ; j++) {
				if(arr[i] + arr[j] == M) {
					++answer;
					break;
				}else if(arr[i] + arr[j] > M) {
					break;
				}
			}
		}
		
		System.out.println(answer);
		
	}

}
