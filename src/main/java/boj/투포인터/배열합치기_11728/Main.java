package boj.투포인터.배열합치기_11728;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	static int N, M;
	static int[] A, B;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] line = br.readLine().split(" ");
		N = Integer.parseInt(line[0]);
		M = Integer.parseInt(line[1]);
		A = new int[N];
		B = new int[M];
		line = br.readLine().split(" ");
		for (int i = 0; i < N; i++) {
			A[i] = Integer.parseInt(line[i]);
		}
		line = br.readLine().split(" ");
		for (int i = 0; i < M; i++) {
			B[i] = Integer.parseInt(line[i]);
		}
		int a = 0, b = 0, c = 0; // index
		StringBuilder sb = new StringBuilder();
		// a와 b가 모두 범위 내에서 움직일 때
		while (a < N && b < M) {
			if (A[a] < B[b]) {
				sb.append(A[a]).append(" ");
				++a;
			} else {
				sb.append(B[b]).append(" ");
				++b;
			}
		}

		// 둘 중 하나가 범위를 벗어남
		while (a < N) {
			sb.append(A[a]).append(" ");
			++a;
		}

		while (b < M) {
			sb.append(B[b]).append(" ");
			++b;
		}

		System.out.println(sb.toString());

	}

}
