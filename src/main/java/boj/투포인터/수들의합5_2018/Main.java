package boj.투포인터.수들의합5_2018;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	static int N;
	static int[] arr;
	static int answer = 0;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		arr = new int[N];
		for (int i = 0; i < N; i++) {
			arr[i] = i + 1;
		}
		int i = 0, j = 1, sum = 0;
		sum = arr[i];
		while (i < N && j < N) {
			// sum은 i부터 j까지의 부분합이고
			// sum이 15 이상이 될 떄까지 j를 늘린다.
			if (sum < N) {
				sum += arr[j++];
			} else if (sum > N) {
				sum -= arr[i++];
			} else {
				answer++;
				sum -= arr[i++];
			}

		}
		
		while(i < N) {
			if(sum == N)
				answer++;
			sum -= arr[i++];
		}

		System.out.println(answer);
	}

}
