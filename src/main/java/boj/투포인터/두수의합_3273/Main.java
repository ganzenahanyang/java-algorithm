package boj.투포인터.두수의합_3273;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

	static int N, X;
	static int[] arr;
	static int answer = 0;
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());
		arr = new int[N];
		String[] line = br.readLine().split(" ");
		for (int i = 0; i < N; i++) {
			arr[i] = Integer.parseInt(line[i]);
		}
		Arrays.sort(arr);
		X = Integer.parseInt(br.readLine());
		for (int i = 0; i < N - 1; i++) {
			if(i > X)
				break;
			for (int j = i + 1; j < N; j++) {
				if(arr[i] + arr[j] > X)
					break;
				if(arr[i] + arr[j] == X)
					answer++;
			}
		}
		System.out.println(answer);
	}

}
