package boj.투포인터.두수의합_9024;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

	static int T, N, K;
	static ArrayList<Integer> numbers;
	static int cnt;
	static int minGap;

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		T = Integer.parseInt(br.readLine());
		for (int t = 0; t < T; t++) {
			String[] line = br.readLine().split(" ");
			N = Integer.parseInt(line[0]);
			K = Integer.parseInt(line[1]);
			numbers = new ArrayList<>();
			cnt = 0;
			minGap = Integer.MAX_VALUE;
			line = br.readLine().split(" ");
			for (int i = 0; i < N; i++) {
				numbers.add(Integer.parseInt(line[i]));
			}
			Collections.sort(numbers);
			int left = 0, right = numbers.size() - 1;
			while (left < right) {
				int lefter = numbers.get(left);
				int righter = numbers.get(right);
				int tempGap = Math.abs(K - Math.abs(righter + lefter));
				if (tempGap < minGap) {
					minGap = tempGap;
					cnt = 1;
				} else if (tempGap == minGap) {
					cnt++;
				}
				if (righter + lefter > K) {
					right--;
				} else {
					left++;
				}
			}
			System.out.println(cnt);
		}
	}

}
