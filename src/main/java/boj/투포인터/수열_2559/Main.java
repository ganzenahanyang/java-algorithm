package boj.투포인터.수열_2559;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	static int N, K;
	static int[] arr;
	static int answer = 0;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] s = br.readLine().split(" ");
		N = Integer.parseInt(s[0]);
		K = Integer.parseInt(s[1]);
		arr = new int[N];
		s = br.readLine().split(" ");
		for (int i = 0; i < N; i++) {
			arr[i] = Integer.parseInt(s[i]);
		}

		int start = 0, end = K - 1, sum = 0;
		for (int i = 0; i < K; i++) {
			sum += arr[i];
		}
		answer = sum;

		while (end < N - 1) {
			sum -= arr[start++];
			sum += arr[++end];
			answer = Math.max(answer, sum);
		}

		System.out.println(answer);
	}

}
