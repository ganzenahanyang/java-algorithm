package boj.N과M시리즈.N과M_3_15651;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    static int N, M;
    static int[] nums;
    static int[] candidate;
    static StringBuilder sb = new StringBuilder();

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] nm = br.readLine().split(" ");
        N = Integer.parseInt(nm[0]);
        M = Integer.parseInt(nm[1]);

        nums = new int[N];
        candidate = new int[M];

        for (int i = 0; i < N; i++) {
            nums[i] = i + 1;
        }

        permutation(0);

        System.out.print(sb);
    }

    static void permutation(int depth) {
        if (depth == M) {
            for (int i = 0; i < M; i++) {
                sb.append(candidate[i]).append(" ");
            }
            sb.append("\n");
            return;
        }
        /**
         * 일반 순열과 달리 중복을 허용한다.
         * 따라서 visit을 따로 체크하지 않는다.
         */
        for (int i = 0; i < N; i++) {
            candidate[depth] = nums[i];
            permutation(depth + 1);
        }
    }
}

