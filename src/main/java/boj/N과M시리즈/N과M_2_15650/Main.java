package boj.N과M시리즈.N과M_2_15650;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    static int N, M;
    static int[] nums;
    static int[] candidate;
    static boolean[] visit;
    static StringBuilder sb = new StringBuilder();

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] nm = br.readLine().split(" ");
        N = Integer.parseInt(nm[0]);
        M = Integer.parseInt(nm[1]);

        nums = new int[N];
        visit = new boolean[N];
        candidate = new int[M];

        for (int i = 0; i < N; i++) {
            nums[i] = i + 1;
        }

        combination(0, 0);

        System.out.print(sb);
    }

    /**
     * 조합은 순서와 상관 없이 구성이 같으면 1개로 치부한다.
     * 따라서, for 문에서 이전 단계의 수에 접근하지 않도록 다음 재귀로 넘길 때 i + 1을 해준다
     */
    static void combination(int depth, int start) {
        if (depth == M) {
            for (int i = 0; i < M; i++) {
                sb.append(candidate[i]).append(" ");
            }
            sb.append("\n");
            return;
        }

        for (int i = start; i < N; i++) {
            if (!visit[i]) {
                visit[i] = true;
                candidate[depth] = nums[i];
                combination(depth + 1, i + 1);
                visit[i] = false;

            }
        }
    }
}
