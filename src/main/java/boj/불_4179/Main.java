package boj.불_4179;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
	static char[][] board;
	static boolean[][] visited;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static Queue<Pair> jQ = new LinkedList<>();
	static Queue<Pair> fQ = new LinkedList<>();
	static int R, C, count = 0;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String[] rc = br.readLine().split(" ");

		R = Integer.parseInt(rc[0]);
		C = Integer.parseInt(rc[1]);

		board = new char[R][C];
		visited = new boolean[R][C];

		for (int i = 0; i < R; i++) {
			String line = br.readLine();
			for (int j = 0; j < C; j++) {
				board[i][j] = line.charAt(j);
				if (board[i][j] == 'J')
					jQ.add(new Pair(i, j));
				else if (board[i][j] == 'F')
					fQ.add(new Pair(i, j));
			}
		}

		while (true) {
			count++;
			int status = jMove();

			if (status == 1) {
				System.out.println(count);
				return;
			} else if (status == -1) {
				System.out.println("IMPOSSIBLE");
				return;
			} else {
				fireMove();
			}
		}
	}

	static int jMove() {

		Queue<Pair> tempQ = new LinkedList<>();

		while (!jQ.isEmpty()) {
			Pair now = jQ.poll();
			int y = now.y;
			int x = now.x;
			if (board[y][x] == 'F')
				continue;
			visited[y][x] = true;

			for (int i = 0; i < 4; i++) {
				int yy = y + dirY[i];
				int xx = x + dirX[i];

				// 탈출한 경우
				if (yy < 0 || xx < 0 || yy >= R || xx >= C)
					return 1;

				if (board[yy][xx] == '#' || board[yy][xx] == 'F' || visited[yy][xx])
					continue;
				visited[yy][xx] = true;
				board[yy][xx] = 'J';
				tempQ.add(new Pair(yy, xx));
			}

		}
		// 사방이 불이거나 벽일 경우에는 이동하지 못 했기 때문에 tempQ는 비어있다.
		if (tempQ.isEmpty())
			return -1;

		// 정상적으로 이동한 경우
		jQ = tempQ;
		return 0;
	}

	static void fireMove() {
		Queue<Pair> tempQ = new LinkedList<>();
		while (!fQ.isEmpty()) {
			Pair now = fQ.poll();
			int y = now.y;
			int x = now.x;
			for (int i = 0; i < 4; i++) {
				int yy = y + dirY[i];
				int xx = x + dirX[i];

				if (yy < 0 || xx < 0 || yy >= R || xx >= C 
						|| board[yy][xx] == '#' || board[yy][xx] == 'F')
					continue;
				board[yy][xx] = 'F';
				tempQ.add(new Pair(yy, xx));

			}
		}

		fQ = tempQ;
	}
}

class Pair {
	int y, x;

	public Pair(int y, int x) {
		super();
		this.y = y;
		this.x = x;
	}
}
