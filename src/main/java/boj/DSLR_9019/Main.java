package boj.DSLR_9019;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Queue;

public class Main {

	static class Test {
		int value;
		StringBuilder cmd = new StringBuilder();

		public Test() {
			super();
		}

		public Test(int value, StringBuilder cmd) {
			super();
			this.value = value;
			this.cmd = cmd;
		}

		@Override
		public String toString() {
			return "Test [value=" + value + ", cmd=" + cmd + "]";
		}

	}

	static int T, A, B;
	static StringBuilder answer = new StringBuilder();
	static final String PATH = "src/main/java/boj/DSLR_9019/input.txt";
	static boolean[] visit;
	static String[] cmdType = { "D", "S", "L", "R" };

	public static void main(String[] args) throws NumberFormatException, IOException {
		System.setIn(new FileInputStream(PATH));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		T = Integer.parseInt(br.readLine());

		for (int t = 0; t < T; t++) {
			String[] ab = br.readLine().split(" ");
			A = Integer.parseInt(ab[0]);
			B = Integer.parseInt(ab[1]);
			visit = new boolean[10001];
			search();

		}

		System.out.println(answer.toString());

	}

	static void search() {
		Queue<Test> q = new ArrayDeque<>();
		Test t = new Test();
		t.value = A;
		q.add(t);
		visit[A] = true;

		while (!q.isEmpty()) {
			Test now = q.poll();

			if (now.value == B) {
				answer.append(now.cmd.toString()).append("\n");
				return;
			}

			for (int i = 0; i < 4; i++) {
				String type = cmdType[i];
				int nextValue = func(type, now.value);
				if (!visit[nextValue]) {
					visit[nextValue] = true;
					q.add(new Test(nextValue, new StringBuilder(now.cmd).append(type)));
				}
			}
		}

	}

	static int func(String cmd, int value) {
		int nextValue;
		if (cmd.equals("D")) {
			nextValue = value * 2 % 10000;
		} else if (cmd.equals("S")) {
			nextValue = value - 1 < 0 ? 9999 : value - 1;
		} else if (cmd.equals("L")) {
			nextValue = value % 1000 * 10 + value / 1000;
		} else {
			nextValue = value / 10 + value % 10 * 1000;
		}

		return nextValue;
	}

}
