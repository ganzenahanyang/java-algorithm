package boj.토마토_7576;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

class Tomato {
	int y, x;

	public Tomato(int y, int x) {
		super();
		this.y = y;
		this.x = x;
	}
}

public class Main {

	static int[][] map;
	static int[][] weight;
	static int[] dirY = { 0, 0, -1, 1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static Queue<Tomato> q = new LinkedList<>();
	static boolean printZero = true; // 다 익은 경우
	static int minDay = 0;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int M, N;
		String[] mns = br.readLine().split(" ");
		M = Integer.parseInt(mns[0]);
		N = Integer.parseInt(mns[1]);

		map = new int[N][M];
		weight = new int[N][M];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < M; j++) {
				map[i][j] = Integer.parseInt(line[j]);
				if (map[i][j] == 1) {
					q.add(new Tomato(i, j));
					weight[i][j] = 1;
				} else if (map[i][j] == 0) {
					printZero = false;
				}
			}
		}
		// 다 익은 경우
		if (printZero) {
			System.out.println(0);
			return;
		}

		while (!q.isEmpty()) {
			Tomato now = q.poll();
			int x = now.x;
			int y = now.y;
			for (int i = 0; i < 4; i++) {
				int xx = x + dirX[i];
				int yy = y + dirY[i];

				if (xx < 0 || yy < 0 || yy >= N || xx >= M || weight[yy][xx] != 0 || map[yy][xx] != 0)
					continue;

				weight[yy][xx] = weight[y][x] + 1;
				map[yy][xx] = 1;
				q.add(new Tomato(yy, xx));
			}
		}

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				if (map[i][j] == 0) { // 토마토가 다 안 익은 경우
					System.out.println(-1);
					return;
				} else {
					minDay = Math.max(weight[i][j], minDay);
				}
			}
		}

		System.out.println(minDay - 1); // 시작지점을 1로 했기 때문에 1 빼줌
	}

}
