package boj.다이나믹프로그래밍.피보나치수_2747;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine().split(" ")[0]);
		long[] dp = new long[46];
		dp[1] = 1;
		dp[2] = 1;
		for(int i = 1 ; i < dp.length - 2; i++) {
			dp[i + 2] = dp[i + 1] + dp[i];
		}
		System.out.println(dp[n]);
	}

}
