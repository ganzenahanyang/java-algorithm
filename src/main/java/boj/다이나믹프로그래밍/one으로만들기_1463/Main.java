package boj.다이나믹프로그래밍.one으로만들기_1463;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) throws Exception, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine().split(" ")[0]);
		int[] dp = new int[1000001];
		dp[2] = 1;
		dp[3] = 1;

		for (int i = 2; i < dp.length; i++) {
			int min = dp[i - 1] + 1;

			if (i % 3 == 0) {
				min = Math.min(dp[i / 3] + 1, min);
			}
			
			if (i % 2 == 0) {
				min = Math.min(dp[i / 2] + 1, min);
			}

			dp[i] = min;

		}

		System.out.println(dp[n]);
	}

}
