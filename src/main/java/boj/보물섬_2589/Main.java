package boj.보물섬_2589;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
	static char[][] board;
	static int[][] visit; // 가중치를 기록하기 위해 int 배열로 선언
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static int Y, X;
	static int maxCnt = 0; // 가장 멀리 떨어진 거리

	// L 육지 W 바다
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] yx = br.readLine().split(" ");
		Y = Integer.parseInt(yx[0]);
		X = Integer.parseInt(yx[1]);

		board = new char[Y][X];
		visit = new int[Y][X];

		for (int i = 0; i < Y; i++) {
			String line = br.readLine();
			for (int j = 0; j < X; j++) {
				board[i][j] = line.charAt(j);
			}
		}

		for (int i = 0; i < Y; i++) {
			for (int j = 0; j < X; j++) {
				if (board[i][j] == 'L' && visit[i][j] == 0) {
					BFS(i, j);
					visit = new int[Y][X];
				}
			}
		}

		System.out.println(maxCnt);

	}

	static void BFS(int i, int j) {
		int cnt = 0;
		Queue<Loc> q = new LinkedList<>();
		q.add(new Loc(i, j));

		visit[i][j] = 1; // 시작 위치의 가중치를 1로 잡음

		while (!q.isEmpty()) {
			Loc now = q.poll();
			int y = now.y;
			int x = now.x;

			for (int d = 0; d < 4; d++) {
				int yy = y + dirY[d];
				int xx = x + dirX[d];
				if (yy < 0 || xx < 0 || yy >= Y || xx >= X || 
						visit[yy][xx] != 0 || board[yy][xx] == 'W')
					continue;
				visit[yy][xx] = visit[y][x] + 1;
				cnt = Math.max(cnt, visit[yy][xx]);
				q.add(new Loc(yy, xx));
			}
		}

		maxCnt = Math.max(cnt - 1, maxCnt); // 초기 셋팅을 1로 잡았으므로 다시 1을 빼준다
	}
}

class Loc {
	int y, x;

	public Loc(int y, int x) {
		super();
		this.y = y;
		this.x = x;
	}

}
