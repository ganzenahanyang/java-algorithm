package boj.삼성기출.낚시왕_17143;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {
	static class Shark implements Comparable<Shark> {
		int y, x, s, d, z;

		public Shark(int y, int x, int s, int d, int z) {
			super();
			this.y = y;
			this.x = x;
			this.s = s; // 속력
			this.d = d; // 방향
			this.z = z; // 크기
		}
		
		public void update(int y, int x, int d) {
			this.y = y;
			this.x = x;
			this.d = d;
		}

		@Override
		public int compareTo(Shark o) {
			return this.z - o.z;
		}

	}

	static int[][] board;
	static int[][] copy;
	static int R, C, M;
	static int[] dirY = { -1, 1, 0, 0 };
	static int[] dirX = { 0, 0, 1, -1 };
	static int catchSize = -1;
	static ArrayList<Integer> deadList = new ArrayList<>();
	static ArrayList<Shark> sharkList = new ArrayList<>();
	static int fisherJ = -1;
	static int answer = 0;

	public static void main(String[] args) throws IOException {
//		BufferedReader br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\낚시왕_17143\\input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] rcm = br.readLine().split(" ");
		R = Integer.parseInt(rcm[0]);
		C = Integer.parseInt(rcm[1]);
		M = Integer.parseInt(rcm[2]);

		if (M == 0) {
			System.out.println(answer);
			return;
		}

		board = new int[R][C];

		for (int i = 0; i < M; i++) {
			String[] line = br.readLine().split(" ");
			int y = Integer.parseInt(line[0]) - 1; // 위치 보정
			int x = Integer.parseInt(line[1]) - 1; // 위치 보정
			int s = Integer.parseInt(line[2]);
			int d = Integer.parseInt(line[3]) - 1; // 방향 보정
			int z = Integer.parseInt(line[4]);

			if (d < 2) {
				s = s % (R * 2 - 2);
			} else {
				s = s % (C * 2 - 2);
			}

			Shark temp = new Shark(y, x, s, d, z);
			sharkList.add(temp);
			board[y][x] = z;
		}

		while (true) {

			if (!fisherMove())
				break;

			sharkMove();

		}

		System.out.println(answer);
	}

	static boolean fisherMove() {
		++fisherJ;
		if (fisherJ >= C) {
			return false;
		}
		int fisherI = 0;
		while (fisherI != R) {
			if (board[fisherI][fisherJ] != 0) {
				answer += board[fisherI][fisherJ];
				catchSize = board[fisherI][fisherJ];
				board[fisherI][fisherJ] = 0;
				break;
			}
			++fisherI;
		}

		return true;
	}

	static void sharkMove() {
		// 카피에서 해야함
		copy = new int[R][C];
		for (int i = 0; i < sharkList.size(); i++) {
			Shark now = sharkList.get(i);
			if (catchSize == now.z) {
				sharkList.remove(i--);
				continue;
			}
			int y = now.y;
			int x = now.x;
			int s = now.s;
			int d = now.d;

			int yy = y;
			int xx = x;

			while (s-- > 0) {
				yy = y + dirY[d];
				xx = x + dirX[d];

				if (yy >= R || xx >= C || yy < 0 || xx < 0) {
					if (d == 0) {
						d = 1;
						yy = y;
					} else if (d == 1) {
						d = 0;
						yy = y;
					} else if (d == 2) {
						d = 3;
						xx = x;
					} else if (d == 3) {
						d = 2;
						xx = x;
					}

					++s;
				} else {
					y = yy;
					x = xx;
				}
			}
			// 위치와 방향 변경
			now.update(yy, xx, d);
			// 상어 이동
			if (copy[now.y][now.x] == 0) {
				copy[now.y][now.x] = now.z;
			} else {
				if (copy[yy][xx] > now.z) { // now 가 먹힘
					deadList.add(now.z);
				} else {
					deadList.add(copy[yy][xx]);
					copy[now.y][now.x] = now.z;

				}
			}
		}
		// 죽은 상어들 샤크리스트에서 제외
		for (int i = 0; i < deadList.size(); i++) {
			int size = deadList.get(i);
			for (int j = 0; j < sharkList.size(); j++) {
				Shark s = sharkList.get(j);
				if (s.z == size) {
					sharkList.remove(j);
					break;
				}
			}
		}
		// 초기화
		deadList.clear();
		catchSize = -1;
		copyToOrigin();
	}

	static void copyToOrigin() {

		for (int i = 0; i < R; i++) {
			for (int j = 0; j < C; j++) {
				board[i][j] = copy[i][j];
			}
		}
	}

}
