package boj.삼성기출.청소년상어_19236;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

	static class Fish implements Comparable<Fish> {
		int y, x;
		int no, d;

		public Fish(int y, int x, int no, int d) {
			super();
			this.y = y;
			this.x = x;
			this.no = no;
			this.d = d;
		}

		public Fish(Fish f) {
			super();
			this.y = f.y;
			this.x = f.x;
			this.no = f.no;
			this.d = f.d;
		}

		@Override
		public int compareTo(Fish o) {
			return this.no - o.no;
		}

	}

	static ArrayList<Fish> fishList = new ArrayList<>(); // 물고기 정보
	static ArrayList<Integer> deadList = new ArrayList<>(); // 죽은 물고기 번호
	static int[][] board = new int[4][4];
	static Fish shark = new Fish(0, 0, -1, -1); // 상어 번호는 -1
	static int[] dirY = { -1, -1, 0, 1, 1, 1, 0, -1 };
	static int[] dirX = { 0, -1, -1, -1, 0, 1, 1, 1 };

	static int score = 0;

	public static void main(String[] args) throws IOException {
//		BufferedReader br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\청소년상어_19236\\input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		fishList.add(new Fish(-1, -1, -2, 0)); // 허수값 삽입
		for (int i = 0; i < 4; i++) {
			String[] lines = br.readLine().split(" ");
			int k = 0;
			for (int j = 0; j < 4; j++) {
				int no = Integer.parseInt(lines[k++]);
				int dir = Integer.parseInt(lines[k++]) - 1; // 방향 보정
				board[i][j] = no;
				fishList.add(new Fish(i, j, no, dir));
			}
		}
		Collections.sort(fishList);

		Fish dead = fishList.get(board[shark.y][shark.x]);
		deadList.add(dead.no);
		board[shark.y][shark.x] = -1;
		shark.d = dead.d;
		selectDead();

		System.out.println(score);
		br.close();
	}

	static void selectDead() {
		fishMove();

		int iy = shark.y;
		int ix = shark.x;
		int id = shark.d;

		while (true) {
			int yy = shark.y + dirY[id];
			int xx = shark.x + dirX[id];

			if (yy >= 4 || xx >= 4 || yy < 0 || xx < 0) { // 더 이상 이동할 곳이 없다.
				int temp = 0;
				for (int i = 0; i < deadList.size(); i++) {
					temp += deadList.get(i);
				}

				score = Math.max(score, temp);

				return;
			}

			shark.y = yy;
			shark.x = xx;
			if (board[yy][xx] == 0) { // 상어는 빈칸으로 이동 못함
				continue;
			}

			// 상어가 이동할 것이다.
			Fish save = new Fish(shark);
			int[][] copyBoard = new int[4][4];
			ArrayList<Fish> copyFishList = new ArrayList<>();
			// deep copy
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					copyBoard[i][j] = board[i][j];
				}
			}
			fishList.forEach(f -> copyFishList.add(new Fish(f)));
			// select dead
			Fish dead = fishList.get(board[yy][xx]);
			deadList.add(dead.no);

			board[iy][ix] = 0; // 상어가 있던 곳을 빈칸으로
			shark = new Fish(yy, xx, -1, dead.d);
			board[shark.y][shark.x] = -1; // 상어가 위치한 곳을

			selectDead();

			deadList.remove(deadList.size() - 1);
			shark = new Fish(save);
			// deep copy
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 4; j++) {
					board[i][j] = copyBoard[i][j];
				}
			}
			fishList.clear();
			copyFishList.forEach(f -> fishList.add(new Fish(f)));

		}
	}

	static void fishMove() {
		for (int i = 1; i < fishList.size(); i++) {
			Fish now = fishList.get(i);
			if (deadList.contains(now.no))
				continue;
			int y = now.y;
			int x = now.x;
			int d = now.d; // 초기 방향
			while (true) {
				int yy = y + dirY[now.d];
				int xx = x + dirX[now.d];

				if (yy >= 4 || xx >= 4 || yy < 0 || xx < 0 || board[yy][xx] == -1) {
					now.d += 1;
					now.d %= 8;
					if (now.d == d) { // 한바퀴 돌았음
						break;
					}
					continue;
				}

				if (board[yy][xx] == 0) { // 빈 칸
					board[now.y][now.x] = 0;
					now.y = yy;
					now.x = xx;
					board[now.y][now.x] = now.no;
				} else { // 물고기가 있다
					int ty = now.y;
					int tx = now.x;
					Fish target = fishList.get(board[yy][xx]);
					now.y = target.y;
					now.x = target.x;
					board[now.y][now.x] = now.no;
					target.y = ty;
					target.x = tx;
					board[target.y][target.x] = target.no;
				}
				break;
			}
		}
	}

}
