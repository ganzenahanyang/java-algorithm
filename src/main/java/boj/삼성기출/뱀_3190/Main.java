package boj.삼성기출.뱀_3190;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

	static class Snake {
		int hy, hx; // 머리 좌표
		int ty, tx; // 꼬리 좌표
		int hd, td; // 머리 방향 꼬리 방향

		public Snake(int hy, int hx, int ty, int tx, int hd, int td) {
			super();
			this.hy = hy;
			this.hx = hx;
			this.ty = ty;
			this.tx = tx;
			this.hd = hd;
			this.td = td;
		}

	}

	static class Command {
		int time;
		char rotate;

		public Command(int time, char rotate) {
			super();
			this.time = time;
			this.rotate = rotate;
		}

	}

	static int[][] board;
	static char[][] history;
	static Queue<Command> q;
	static int N, K, L; // K = 사과의 갯수 L = 명령수
	static Snake s;
	static int[] dirY = { 0, -1, 0, 1 }; // 오 위 왼 아
	static int[] dirX = { 1, 0, -1, 0 };

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		init(br);

		int time = 1;
		while (true) {
			if (!move())
				break;
			if (!q.isEmpty() && time == q.peek().time) {
				rotate(q.peek().rotate);
				q.poll();
			}
			++time;

		}

		System.out.println(time);
	}

	static void rotate(char r) {
		if (r == 'D') {
			s.hd = (s.hd + 3) % 4;
		} else {
			s.hd = (s.hd + 1) % 4;
		}
		// 길이가 1이라 머리 꼬리 좌표가 같은 경우
		if (s.hy == s.ty && s.hx == s.tx) {
			if (r == 'D') {
				s.td = (s.td + 3) % 4;
			} else {
				s.td = (s.td + 1) % 4;
			}
		}
		// 길이가 2 이상이면 꼬리가 이걸 보고 판단해야함
		else {
			history[s.hy][s.hx] = r;
		}
	}

	static boolean move() {
		int hyy = s.hy + dirY[s.hd];
		int hxx = s.hx + dirX[s.hd];

		if (hyy < 0 || hxx < 0 || hyy >= N || hxx >= N || board[hyy][hxx] == 1) {
			return false;
		}

		if (board[hyy][hxx] == 2) { // 사과 있음

		} else {
			int tyy = s.ty + dirY[s.td];
			int txx = s.tx + dirX[s.td];

			if (history[tyy][txx] == 'D') {
				s.td = (s.td + 3) % 4;
			} else if (history[tyy][txx] == 'L') {
				s.td = (s.td + 1) % 4;
			}
			// 방향 기록 초기화
			history[tyy][txx] = ' ';

			board[s.ty][s.tx] = 0;
			board[tyy][txx] = 1;

			s.ty = tyy;
			s.tx = txx;
		}

		board[hyy][hxx] = 1;

		s.hy = hyy;
		s.hx = hxx;

		return true;
	}

	static void init(BufferedReader br) throws NumberFormatException, IOException {
		q = new LinkedList<>();

		N = Integer.parseInt(br.readLine());
		board = new int[N][N];
		history = new char[N][N];
		s = new Snake(0, 0, 0, 0, 0, 0);
		board[0][0] = 1;

		K = Integer.parseInt(br.readLine());
		for (int i = 0; i < K; i++) {
			String[] line = br.readLine().split(" ");
			int y = Integer.parseInt(line[0]) - 1;
			int x = Integer.parseInt(line[1]) - 1;
			board[y][x] = 2; // 사과
		}

		L = Integer.parseInt(br.readLine());
		for (int i = 0; i < L; i++) {
			String[] line = br.readLine().split(" ");
			int cnt = Integer.parseInt(line[0]);
			char d = line[1].charAt(0);
			q.add(new Command(cnt, d));
		}
	}

}
