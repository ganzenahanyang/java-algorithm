package boj.삼성기출.상어초등학교_21608;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
	static class Student implements Comparable<Student> {
		int no;
		ArrayList<Integer> like;

		public Student(int no, ArrayList<Integer> like) {
			super();
			this.no = no;
			this.like = like;
		}

		@Override
		public int compareTo(Student o) {

			return this.no - o.no;
		}

	}

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

	}

	static ArrayList<Student> students = new ArrayList<Student>();
	static int N;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static int[][] board;
	static int score = 0;

	public static void main(String[] args) throws IOException {
//		BufferedReader br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\상어초등학교_21608\\input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] n = br.readLine().split(" ");
		N = Integer.parseInt(n[0]);
		board = new int[N][N];

		for (int i = 0; i < N * N; i++) {
			String[] line = br.readLine().split(" ");
			int key = Integer.parseInt(line[0]);
			ArrayList<Integer> temp = new ArrayList<>();
			for (int j = 1; j < 5; j++) {
				temp.add(Integer.parseInt(line[j]));
			}
			students.add(new Student(key, temp));
		}

		for (int i = 0; i < students.size(); i++) {
			selectSeat(students.get(i));
		}

		Collections.sort(students);

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				int index = board[i][j] - 1;
				ArrayList<Integer> values = students.get(index).like;
				int cnt = 0;
				for (int d = 0; d < 4; d++) {
					int ii = i + dirY[d];
					int jj = j + dirX[d];

					if (ii >= N || jj >= N || ii < 0 || jj < 0 || board[ii][jj] == 0)
						continue;
					if (values.contains(board[ii][jj]))
						cnt++;
				}
				switch (cnt) {
				case 0:
					score += 0;
					break;
				case 1:
					score += 1;
					break;
				case 2:
					score += 10;
					break;
				case 3:
					score += 100;
					break;
				case 4:
					score += 1000;
					break;
				}
			}
		}
		System.out.println(score);

	}

	public static void selectSeat(Student now) {
		int no = now.no;
		ArrayList<Integer> like = now.like;
		ArrayList<Pair> candidate = new ArrayList<>();
		int likeCnt = 0;

		// 1. 비어있는 칸 중에서 좋아하는 학생이 인접한 칸에 가장 많은 칸으로 자리를 정한다.
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (board[i][j] == 0) {
					int cnt = 0;
					for (int d = 0; d < 4; d++) {
						int ii = i + dirY[d];
						int jj = j + dirX[d];

						if (ii >= N || jj >= N || ii < 0 || jj < 0 || board[ii][jj] == 0)
							continue;

						if (like.contains(board[ii][jj]))
							++cnt;
					}
					if (likeCnt < cnt) {
						candidate.clear();
						candidate.add(new Pair(i, j));
						likeCnt = cnt;
					} else if (likeCnt == cnt) {
						candidate.add(new Pair(i, j));
					}
				}
			}
		}
		if (candidate.size() == 1) {
			Pair p = candidate.get(0);
			board[p.y][p.x] = no;
			return;
		}

		// 2. 1을 만족하는 칸이 여러개이면 인접한 칸 중에서 비어이있는 칸이 가장 많은 칸으로 자리를 정한다.
		int secondCnt = 0;
		ArrayList<Pair> secondCandidate = new ArrayList<>();
		for (int i = 0; i < candidate.size(); i++) {
			Pair p = candidate.get(i);
			int cnt = 0;
			for (int d = 0; d < 4; d++) {
				int yy = p.y + dirY[d];
				int xx = p.x + dirX[d];

				if (yy >= N || xx >= N || yy < 0 || xx < 0 || board[yy][xx] != 0)
					continue;

				cnt++;
			}

			if (secondCnt < cnt) {
				secondCandidate.clear();
				secondCandidate.add(p);
				secondCnt = cnt;
			} else if (secondCnt == cnt) {
				secondCandidate.add(p);
			}
		}

		Pair p = secondCandidate.get(0);
		board[p.y][p.x] = no;
		return;

	}
}
