package boj.삼성기출.컨베이어벨트위의로봇_20055;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {

	static int N, K;
	static List<Tile> belt = new ArrayList<>();
	static int loopCnt = 1;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] nk = br.readLine().split(" ");
		N = Integer.parseInt(nk[0]);
		K = Integer.parseInt(nk[1]);
		String[] line = br.readLine().split(" ");
		for (int i = 0; i < line.length; i++) {
			belt.add(new Tile(Integer.parseInt(line[i]), false));
		}

		while (true) {

			// 1 로봇과 벨트가 함께 회전
			Tile last = belt.get(belt.size() - 1);
			belt.remove(belt.size() - 1);
			belt.add(0, last);
			if(belt.get(N - 1).robot)
				belt.get(N - 1).robot = false;

			// 2 먼저 올라온 로봇부터 이동
			for (int i = N - 1; i >= 0; i--) {
				Tile now = belt.get(i);
				if (now.robot) {
					Tile next = belt.get(i + 1);
					if(!next.robot && next.hp > 0) {
						now.robot = false;
						next.hp--;
						next.robot = true;
						if((i + 1) == N - 1)
							next.robot = false;
					}
				}
			}

			// 3 올리는 위치에 내구도가 0이 아니면 로봇 올리기
			Tile first = belt.get(0);
			if(first.hp > 0) {
				first.robot = true;
				first.hp--;
			}

			if(check())
				break;

			loopCnt++;

		}

		System.out.println(loopCnt);
	}

	static boolean check() {
		int cnt = 0;
		for (int i = 0; i < belt.size(); i++) {
			if (belt.get(i).hp == 0)
				cnt++;
		}
		if (cnt >= K)
			return true;
		else
			return false;
	}
}

class Tile {
	int hp;
	boolean robot;

	public Tile(int hp, boolean robot) {
		super();
		this.hp = hp;
		this.robot = robot;
	}
}
