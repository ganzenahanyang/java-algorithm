package boj.삼성기출.드래곤커브_15685;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

	static int[] dirY = { 0, -1, 0, 1 };
	static int[] dirX = { 1, 0, -1, 0 };
	static int N; // 드래곤 커브의 갯수
	static int[][] board = new int[101][101];
	static int[][] rectangle = { { 1, 1 }, { 1, 1 } };
	static ArrayList<Integer> cmds;
	static int answer = 0;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			int x = Integer.parseInt(line[0]);
			int y = Integer.parseInt(line[1]);
			int d = Integer.parseInt(line[2]);
			int g = Integer.parseInt(line[3]);

			cmds = new ArrayList<>();
			board[y][x] = 1;

			while (g >= 0) {

				if (cmds.isEmpty()) {
					y = y + dirY[d];
					x = x + dirX[d];
					board[y][x] = 1;
					cmds.add((d + 1) % 4);
				} else {

					ArrayList<Integer> temp = new ArrayList<>();
					for (int c = cmds.size() - 1; c >= 0; c--) {
						int dd = cmds.get(c);
						y = y + dirY[dd];
						x = x + dirX[dd];
						board[y][x] = 1;
						temp.add(dd);
					}

					for (int j = 0; j < temp.size(); j++) {
						cmds.add((temp.get(j) + 1) % 4);
					}
				}

				--g;
			}
		}
		count();
		System.out.println(answer);
	}

	private static void count() {
		// 2 * 2 부분 배열이 모두 1인지 판단함
		// 현 위치에서 +1행 +1열을 확인하므로 인덱스의 범위는 100
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < 100; j++) {
				if (board[i][j] == 1) {
					if (board[i + 1][j] == 1 && board[i][j + 1] == 1 && board[i + 1][j + 1] == 1) {
						++answer;
					}
				}
			}
		}
	}

	static int reverseDir(int d) {
		if (d == 0)
			return 2;
		else if (d == 1) {
			return 3;
		} else if (d == 2) {
			return 0;
		} else {
			return 1;
		}
	}

	static int rotateDir(int d) {
		if (d == 0)
			return 3;
		else if (d == 1) {
			return 0;
		} else if (d == 2) {
			return 1;
		} else {
			return 2;
		}
	}

}
