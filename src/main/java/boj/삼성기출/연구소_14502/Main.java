package boj.삼성기출.연구소_14502;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

class Virus {
	int y, x;

	public Virus(int y, int x) {
		super();
		this.y = y;
		this.x = x;
	}

}

public class Main {

	static int N, M;
	static int[][] board;
	static int[][] copyBoard; // 연구소의 초기 상태를 간직할 배열
	static boolean[][] visit;
	static int[] dirY = { 0, 0, -1, 1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static Queue<Virus> q = new LinkedList<>();
	static Queue<Virus> copyQ = new LinkedList<>(); // 바이러스의 초기 위치를 간직할 Queue
	static ArrayList<Virus> selected = new ArrayList<>();
	static ArrayList<Virus> candidate = new ArrayList<>(); // 벽을 세울 후보 좌표를 저장할 ArrayList
	static int maxSafe = 0;

	// 0 = 빈칸, 1 = 벽, 2 = 바이러스
	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] nm = br.readLine().split(" ");

		N = Integer.parseInt(nm[0]);
		M = Integer.parseInt(nm[1]);

		board = new int[N][M];
		visit = new boolean[N][M];
		copyBoard = new int[N][M];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < M; j++) {
				board[i][j] = Integer.parseInt(line[j]);
				copyBoard[i][j] = Integer.parseInt(line[j]);
				if (board[i][j] == 2) {
					q.add(new Virus(i, j));
					copyQ.add(new Virus(i, j));
				}
				if (board[i][j] == 0)
					candidate.add(new Virus(i, j));
			}
		}

		select(0, 0);
		System.out.println(maxSafe);

	}

	public static void select(int index, int depth) {
		if (depth == 3) {
			for (int k = 0; k < selected.size(); k++) {
				int sy = selected.get(k).y;
				int sx = selected.get(k).x;
				board[sy][sx] = 1;
			}

			BFS();
			count();
			reset();
			return;
		}

		for (int i = index; i < candidate.size(); i++) {
			selected.add(candidate.get(i));
			select(i + 1, depth + 1);
			selected.remove(selected.size() - 1);
		}

	}

	public static void reset() {
		// board 초기화
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				board[i][j] = copyBoard[i][j];
			}
		}
		// virusQ 초기화
		q.clear();
		q.addAll(copyQ);
		// visit 초기화
		visit = new boolean[N][M];
	}

	public static void BFS() {
		while (!q.isEmpty()) {
			Virus now = q.poll();
			int y = now.y;
			int x = now.x;
			visit[y][x] = true;
			for (int i = 0; i < 4; i++) {
				int yy = y + dirY[i];
				int xx = x + dirX[i];
				if (xx < 0 || yy < 0 || xx >= M || yy >= N || 
						board[yy][xx] == 1 || visit[yy][xx])
					continue;
				q.add(new Virus(yy, xx));
				visit[yy][xx] = true;
				board[yy][xx] = 2;
			}
		}
	}

	public static void count() {
		int temp = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				if (board[i][j] == 0)
					temp++;
			}
		}
		maxSafe = Math.max(temp, maxSafe);
	}

}

