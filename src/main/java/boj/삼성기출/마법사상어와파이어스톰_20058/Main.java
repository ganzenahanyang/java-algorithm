package boj.삼성기출.마법사상어와파이어스톰_20058;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

	static int N, Q;
	static int[][] board;
	static int[][] copyBoard;
	static boolean[][] visit;
	static ArrayList<Integer> command = new ArrayList<>();
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static int L = 0;

	static int sum = 0;
	static int size = 0;
	static int tempCnt = 0;

	public static void main(String[] args) throws IOException {
//		BufferedReader br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\마법사상어와파이어스톰_20058\\input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] nq = br.readLine().split(" ");
		N = Integer.parseInt(nq[0]);
		Q = Integer.parseInt(nq[1]);

		N = (int) Math.pow(2, N);

		board = new int[N][N];
		visit = new boolean[N][N];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		String[] line = br.readLine().split(" ");
		for (int i = 0; i < Q; i++) {
			command.add(Integer.parseInt(line[i]));
		}

		for (int i = 0; i < command.size(); i++) {
			L = (int) Math.pow(2, command.get(i));
			rotate();
			melt();
		}

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (board[i][j] != 0) {
					sum += board[i][j];
					visit = new boolean[N][N];
					checkSize(i, j);
					size = Math.max(tempCnt, size);
					tempCnt = 0;
				}

			}
		}

		System.out.println(sum);
		System.out.println(size);

	}

	static void checkSize(int i, int j) {
		visit[i][j] = true;
		tempCnt++;
		for (int d = 0; d < 4; d++) {
			int ii = i + dirY[d];
			int jj = j + dirX[d];

			if (ii >= N || jj >= N || ii < 0 || jj < 0 || visit[ii][jj] || board[ii][jj] == 0)
				continue;

			checkSize(ii, jj);
		}
	}

	static void copy() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				board[i][j] = copyBoard[i][j];
			}
		}
	}

	static void melt() {
		copyBoard = new int[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (board[i][j] != 0) {
					int cnt = 0;
					for (int d = 0; d < 4; d++) {
						int ii = i + dirY[d];
						int jj = j + dirX[d];

						if (ii >= N || jj >= N || ii < 0 || jj < 0 || board[ii][jj] == 0)
							continue;
						++cnt;
					}
					if (cnt < 3)
						copyBoard[i][j] = board[i][j] - 1;
					else
						copyBoard[i][j] = board[i][j];
				}
			}
		}
		copy();
	}

	static void rotate() {
		copyBoard = new int[N][N];
		int startI = 0;
		int endI = L;
		int startJ = 0;
		int endJ = L;
		while (endI <= N) {
			for (int i = startI; i < endI; i++) {
				for (int j = startJ; j < endJ; j++) {
					copyBoard[j - startJ + startI][endJ - 1 - i + startI] = board[i][j];
				}
			}
			startJ += L;
			endJ += L;
			if (endJ > N) {
				startI += L;
				endI += L;
				startJ = 0;
				endJ = L;
			}
		}
		copy();
	}
}
