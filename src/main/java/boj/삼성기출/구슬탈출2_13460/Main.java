package boj.삼성기출.구슬탈출2_13460;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;

public class Main {
	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

		public Pair(Pair p) {
			super();
			this.y = p.y;
			this.x = p.x;
		}

	}

	static int N, M;
	static char[][] board;
	static Pair red, blue, end;
	static int answer;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static boolean[][][][] visit;

	public static void main(String[] args) throws Exception {
		Queue<ArrayList<Pair>> q = new ArrayDeque<>();
		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		init(br);
		solve(0);

		if (answer == Integer.MAX_VALUE)
			answer = -1;
		System.out.println(answer);
	}

	private static void solve(int depth) {
		// TODO Auto-generated method stub
		if (depth > 10)
			return;

		if (answer < depth)
			return;

		if (red.y == end.y && red.x == end.x) {
			answer = depth;
			return;
		}

		for (int i = 0; i < 4; i++) {
			Pair copyRed = new Pair(red);
			Pair copyBlue = new Pair(blue);
			ballMove(i);
			// 파란 공이 구멍에 들어간 경우
			if (blue.y == end.y && blue.x == end.x) {
				red = new Pair(copyRed);
				blue = new Pair(copyBlue);
				continue;
			}
			// 위치가 겹칠 경우
			balancing(copyRed, copyBlue, i);
			// 이전에 동일한 상황이 있던 경우
			if (visit[red.y][red.x][blue.y][blue.x]) {
				red = new Pair(copyRed);
				blue = new Pair(copyBlue);
				continue;
			}
			visit[red.y][red.x][blue.y][blue.x] = true;

			solve(depth + 1);

			visit[red.y][red.x][blue.y][blue.x] = false;
			red = new Pair(copyRed);
			blue = new Pair(copyBlue);
		}
	}

	static void ballMove(int d) {
		while (true) {
			red.y += dirY[d];
			red.x += dirX[d];

			if (board[red.y][red.x] == '#') {
				red.y -= dirY[d];
				red.x -= dirX[d];
				break;
			} else if (board[red.y][red.x] == 'O') {
				break;
			}
		}

		while (true) {
			blue.y += dirY[d];
			blue.x += dirX[d];

			if (board[blue.y][blue.x] == '#') {
				blue.y -= dirY[d];
				blue.x -= dirX[d];
				break;
			} else if (board[blue.y][blue.x] == 'O') {
				break;
			}
		}
	}

	static void balancing(Pair copyRed, Pair copyBlue, int d) { // 오 왼 아래 위
		if (red.y == blue.y && red.x == blue.x) {
			if (d == 0) { // 오른쪽으로 이동한 거였다면?
				if (copyRed.x < copyBlue.x) // 빨강 파랑 이었으면
					--red.x;
				else // 파랑 빨강이었으면
					--blue.x;

			} else if (d == 1) { // 왼쪽 이동한 거였다면?
				if (copyRed.x < copyBlue.x) // 빨강 파랑 이었으면
					++blue.x;
				else // 파랑 빨강이었으면
					++red.x;

			} else if (d == 2) { // 아래로 이동했을때
				if (copyRed.y < copyBlue.y) // 빨강이 파랑보다 위였다면?
					--red.y;
				else
					--blue.y;

			} else { // 위로 이동했을때
				if (copyRed.y < copyBlue.y) // 빨강이 파랑보다 위였다면?
					++blue.y;
				else
					++red.y;

			}
		} else {
			// 아무 일 없음
			return;
		}
	}

	static void init(BufferedReader br) throws Exception {
		String[] nm = br.readLine().split(" ");
		N = Integer.parseInt(nm[0]);
		M = Integer.parseInt(nm[1]);
		board = new char[N][M];
		visit = new boolean[N][M][N][M];
		for (int i = 0; i < N; i++) {
			String line = br.readLine();
			for (int j = 0; j < M; j++) {
				board[i][j] = line.charAt(j);
				if (board[i][j] == 'B') {
					blue = new Pair(i, j);
				} else if (board[i][j] == 'R') {
					red = new Pair(i, j);
				} else if (board[i][j] == 'O') {
					end = new Pair(i, j);
				}
			}
		}

		visit[red.y][red.x][blue.y][blue.x] = true;

		answer = Integer.MAX_VALUE;
	}

}
