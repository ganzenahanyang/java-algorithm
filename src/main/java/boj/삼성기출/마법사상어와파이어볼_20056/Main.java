package boj.삼성기출.마법사상어와파이어볼_20056;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {

	static int[] dirY = { -1, -1, 0, 1, 1, 1, 0, -1 };
	static int[] dirX = { 0, 1, 1, 1, 0, -1, -1, -1 };
	static Tile[][] board;
	static Tile[][] copyBoard; // 이동을 기록할 보드
	static int N, M, K;

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String[] nmk = br.readLine().split(" ");
		N = Integer.parseInt(nmk[0]);
		M = Integer.parseInt(nmk[1]);
		K = Integer.parseInt(nmk[2]);

		board = new Tile[N][N];
		copyBoard = new Tile[N][N];

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				board[i][j] = new Tile();
				copyBoard[i][j] = new Tile();

			}
		}

		for (int i = 0; i < M; i++) {
			String[] line = br.readLine().split(" ");
			int y = Integer.parseInt(line[0]) - 1;
			int x = Integer.parseInt(line[1]) - 1;
			int m = Integer.parseInt(line[2]);
			int s = Integer.parseInt(line[3]);
			int d = Integer.parseInt(line[4]);
			board[y][x].list.add(new Ball(y, x, m, d, s));
		}

		for (int k = 0; k < K; k++) {

			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					if (board[i][j].list.isEmpty())
						continue;
					ballMove(board[i][j].list);

				}
			}
			reset();

			// 나누기
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					if (board[i][j].list.size() < 2)
						continue;
					List<Ball> tempList = divide(i, j, board[i][j].list);
					board[i][j].list.clear();
					board[i][j].list.addAll(tempList);

				}
			}

		}

		int sum = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (board[i][j].list.isEmpty())
					continue;
				for (int k = 0; k < board[i][j].list.size(); k++) {
					sum += board[i][j].list.get(k).m;
				}

			}
		}

		System.out.println(sum);

	}

	static void ballMove(List<Ball> list) {
		for (int i = 0; i < list.size(); i++) {
			Ball now = list.get(i);
			int y = now.y;
			int x = now.x;
			int dir = now.d;
			int speed = now.s;

			int yy = y + dirY[dir] * speed % N;
			int xx = x + dirX[dir] * speed % N;
			
			if (yy < 0) {
				yy = N + yy;
			} else if (yy >= N) {
				yy = yy % N;
			}

			if (xx < 0) {
				xx = N + xx;
			} else if (xx >= N) {
				xx = xx % N;
			}

			copyBoard[yy][xx].list.add(new Ball(yy, xx, now.m, now.d, now.s));
		}
	}

	static void reset() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				board[i][j].list.clear();
				board[i][j].list.addAll(copyBoard[i][j].list);
				copyBoard[i][j].list.clear();
			}

		}
	}

	static List<Ball> divide(int y, int x, List<Ball> list) {
		Ball sum = new Ball(y, x, 0, 0, 0);
		int odd = 0;
		int even = 0;
		for (int i = 0; i < list.size(); i++) {
			Ball temp = list.get(i);
			sum.m += temp.m;
			sum.s += temp.s;
			if (temp.d % 2 == 0) {
				even++;
			} else {
				odd++;
			}
		}

		List<Ball> tempList = new ArrayList<>();

		if (sum.m < 5) {
			return tempList;
		}

		int nextM = sum.m / 5;
		int nextS = sum.s / (list.size());

		int startNum;
		if (even == list.size() || odd == list.size()) {
			startNum = 0;
		} else {
			startNum = 1;
		}

		for (int i = 0; i < 4; i++) {
			tempList.add(new Ball(y, x, nextM, startNum, nextS));
			startNum += 2;
		}

		return tempList;
	}

}

class Tile {
	List<Ball> list = new ArrayList<>();
}

class Ball {
	// 좌표 / 무게 / 방향 / 속력
	int y, x, m, d, s;

	public Ball() {

	}

	public Ball(int y, int x, int m, int d, int s) {
		super();
		this.y = y;
		this.x = x;
		this.m = m;
		this.d = d;
		this.s = s;
	}

}
