package boj.삼성기출.이차원배열과연산_17140;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Main {

	static class Number implements Comparable<Number> {
		int value, cnt;

		@Override
		public int compareTo(Number o) {
			if (this.cnt == o.cnt)
				return this.value - o.value;
			return this.cnt - o.cnt;
		}

		public Number(int value, int cnt) {
			super();
			this.value = value;
			this.cnt = cnt;
		}

	}

	static int R, C, K;
	static int[][] board = new int[100][100];
	static int N = 3, M = 3; // N은 행 M은 열 길이
	static HashMap<Integer, Integer> hm;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] rck = br.readLine().split(" ");
		R = Integer.parseInt(rck[0]) - 1;
		C = Integer.parseInt(rck[1]) - 1;
		K = Integer.parseInt(rck[2]);

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < M; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}
		int cnt = 0;
		while (board[R][C] != K && cnt <= 100) {
			if (N >= M) {
				fuctionR();
			} else {
				fuctionC();
			}

			++cnt;
		}

		if (cnt > 100)
			System.out.println(-1);
		else
			System.out.println(cnt);

	}

	private static void fuctionR() {
		int MM = 0;
		for (int i = 0; i < N; i++) {
			hm = new HashMap<>();
			for (int j = 0; j < M; j++) {
				if (board[i][j] == 0)
					continue;
				hm.put(board[i][j], hm.getOrDefault(board[i][j], 0) + 1);
			}
			ArrayList<Number> nums = new ArrayList<>();
			for (Integer key : hm.keySet()) {
				nums.add(new Number(key, hm.get(key)));
			}
			Collections.sort(nums);

			for (int j = 0; j < nums.size() * 2 && j < 100; j += 2) {
				board[i][j] = nums.get(j / 2).value;
				board[i][j + 1] = nums.get(j / 2).cnt;
			}

			if (nums.size() * 2 < 100) {
				for (int j = nums.size() * 2; j < 100; j++) {
					board[i][j] = 0;
				}
			}

			MM = Math.max(MM, nums.size() * 2 > 100 ? 100 : nums.size() * 2);
		}

		M = MM;
	}

	private static void fuctionC() {
		int NN = 0;
		for (int j = 0; j < M; j++) {
			hm = new HashMap<>();
			for (int i = 0; i < N; i++) {
				if (board[i][j] == 0)
					continue;
				hm.put(board[i][j], hm.getOrDefault(board[i][j], 0) + 1);
			}

			ArrayList<Number> nums = new ArrayList<>();
			for (Integer key : hm.keySet()) {
				nums.add(new Number(key, hm.get(key)));
			}
			Collections.sort(nums);

			for (int i = 0; i < nums.size() * 2 && i < 100; i += 2) {
				board[i][j] = nums.get(i / 2).value;
				board[i + 1][j] = nums.get(i / 2).cnt;
			}
			if (nums.size() * 2 < 100) {
				for (int i = nums.size() * 2; i < 100; i++) {
					board[i][j] = 0;
				}
			}

			NN = Math.max(NN, nums.size() * 2 > 100 ? 100 : nums.size() * 2);
		}
		N = NN;
	}

}
