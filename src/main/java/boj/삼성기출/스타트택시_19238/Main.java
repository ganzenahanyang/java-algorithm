package boj.삼성기출.스타트택시_19238;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
	static class Customer {
		int no;
		int sy, sx, ey, ex;

		public Customer(int no, int sy, int sx, int ey, int ex) {
			super();
			this.no = no;
			this.sy = sy;
			this.sx = sx;
			this.ey = ey;
			this.ex = ex;
		}
	}

	static class Taxi {
		int y, x, oil;
		int customerNo;

		public Taxi(int y, int x, int oil) {
			super();
			this.y = y;
			this.x = x;
			this.oil = oil;
		}

	}

	static class Pair implements Comparable<Pair> {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

		@Override
		public int compareTo(Pair o) {
			if (this.y == o.y)
				return this.x - o.x;
			return this.y - o.y;
		}
	}

	static int N, M, L;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static int[][] visit;
	static int[][] board;
	static Taxi t;
	static ArrayList<Customer> customerInfo = new ArrayList<>();
	static int driveCnt = 0;
	static int answer = 0;

	public static void main(String[] args) throws IOException {
		BufferedReader br;
//		br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\스타트택시_19238\\input.txt"));
		br = new BufferedReader(new InputStreamReader(System.in));

		// 승객 위치보정 필요
		String[] nml = br.readLine().split(" ");
		N = Integer.parseInt(nml[0]);
		M = Integer.parseInt(nml[1]);
		L = Integer.parseInt(nml[2]);

		board = new int[N][N];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
				if (board[i][j] == 1)
					board[i][j] = -1;
			}
		}
		String[] taxi = br.readLine().split(" ");
		int y = Integer.parseInt(taxi[0]) - 1;
		int x = Integer.parseInt(taxi[1]) - 1;
		t = new Taxi(y, x, L);

		for (int i = 0; i < M; i++) {
			String[] line = br.readLine().split(" ");
			int sy = Integer.parseInt(line[0]) - 1;
			int sx = Integer.parseInt(line[1]) - 1;
			int ey = Integer.parseInt(line[2]) - 1;
			int ex = Integer.parseInt(line[3]) - 1;
			customerInfo.add(new Customer(i + 1, sy, sx, ey, ex)); // 접근할때 번호 - 1
			board[sy][sx] = i + 1;
		}

		while (true) {
			if (driveCnt == M) {
				answer = t.oil;
				break;
			}

			if (!pickCustomer()) {
				answer = -1;
				break;
			}

			if (!drive()) {
				answer = -1;
				break;
			}
		}

		System.out.println(answer);
	}

	// 승객 태우고 이동
	static boolean drive() {
		visit = new int[N][N];
		Customer c = customerInfo.get(t.customerNo - 1);
		int ey = c.ey;
		int ex = c.ex;

		Pair p = new Pair(t.y, t.x);
		Queue<Pair> q = new LinkedList<>();
		q.add(p);
		visit[t.y][t.x] = 1; // 비용 보정 필요
		while (!q.isEmpty()) {
			Pair now = q.poll();
			int y = now.y;
			int x = now.x;

			if (y == ey && x == ex) {
				break;
			}

			for (int d = 0; d < 4; d++) {
				int yy = y + dirY[d];
				int xx = x + dirX[d];

				if (yy < 0 || xx < 0 || yy >= N || xx >= N || board[yy][xx] == -1 || visit[yy][xx] != 0)
					continue;

				visit[yy][xx] = visit[y][x] + 1;
				q.add(new Pair(yy, xx));
			}
		}
		if (visit[ey][ex] == 0)
			return false;
		int spendOil = visit[ey][ex] - 1;
		if (t.oil >= spendOil) {
			t.y = ey;
			t.x = ex;
			t.oil += spendOil;
			++driveCnt;
			return true;
		} else {
			return false;
		}

	}

	// 승객 찾기
	static boolean pickCustomer() {
		int minDis = -1;
		visit = new int[N][N];
		ArrayList<Pair> candi = new ArrayList<>();
		Queue<Pair> q = new LinkedList<>();
		q.add(new Pair(t.y, t.x));
		if (board[t.y][t.x] > 0) {
			t.customerNo = board[t.y][t.x];
			board[t.y][t.x] = 0;
			return true;
		}
		visit[t.y][t.x] = 1; // 나중에 -1 해야함
		while (!q.isEmpty()) {
			Pair now = q.poll();
			int y = now.y;
			int x = now.x;

			if (board[y][x] > 0) {
				if (minDis == -1) {
					minDis = visit[y][x];
					candi.add(new Pair(y, x));
				} else {
					if (minDis == visit[y][x]) {
						candi.add(new Pair(y, x));
					} else {
						break;
					}
				}
			}

			for (int d = 0; d < 4; d++) {
				int yy = y + dirY[d];
				int xx = x + dirX[d];

				if (yy < 0 || xx < 0 || yy >= N || xx >= N || board[yy][xx] == -1 || visit[yy][xx] != 0)
					continue;

				visit[yy][xx] = visit[y][x] + 1;
				q.add(new Pair(yy, xx));
			}
		}
		if (candi.isEmpty()) {
			return false;
		}

		// 후보 리스트 완성 후 태울 손님 찾기
		Collections.sort(candi);
		int y = candi.get(0).y;
		int x = candi.get(0).x;
		// 손님 태우러갈때 쓴 연료
		int spendOil = visit[y][x] - 1;
		if (t.oil >= spendOil) {
			t.y = y;
			t.x = x;
			t.oil -= spendOil;
			t.customerNo = board[y][x];
			board[y][x] = 0;
			return true;
		} else {
			return false;
		}

	}
}