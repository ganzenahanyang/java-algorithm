package boj.삼성기출.치킨배달_15686;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

	}

	static int N, M;
	static int[][] board;
	static ArrayList<Pair> chickens = new ArrayList<>();
	static ArrayList<Pair> houses = new ArrayList<>();
	static ArrayList<Pair> candi = new ArrayList<>();
	static int answer = Integer.MAX_VALUE; // 최솟값

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] nm = br.readLine().split(" ");
		N = Integer.parseInt(nm[0]);
		M = Integer.parseInt(nm[1]); // 치킨 집의 갯수

		board = new int[N][N];
		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
				if (board[i][j] == 2)
					chickens.add(new Pair(i, j));
				else if (board[i][j] == 1)
					houses.add(new Pair(i, j));
			}
		}
		while (M > 0) {
			select(0, 0);
			--M;
		}
		System.out.println(answer);

	}

	static void select(int index, int depth) {
		if (depth == M) {
			getDist();
			return;
		}

		for (int i = index; i < chickens.size(); i++) {
			candi.add(chickens.get(i));
			select(i + 1, depth + 1);
			candi.remove(candi.size() - 1);
		}
	}

	static void getDist() {
		int sum = 0;
		for (int i = 0; i < houses.size(); i++) {
			Pair h = houses.get(i);
			int temp = Integer.MAX_VALUE;
			for (int j = 0; j < candi.size(); j++) {
				Pair c = candi.get(j);
				int dist = Math.abs(h.y - c.y) + Math.abs(h.x - c.x);
				temp = Math.min(temp, dist);
			}
			sum += temp;
		}
		answer = Math.min(answer, sum);
	}
	
}
