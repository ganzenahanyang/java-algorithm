package boj.삼성기출.로봇청소기_14503;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

	static int N, M;
	static int[][] board;
	static boolean[][] visited;
	static int[] dirY = { -1, 0, 1, 0 };
	static int[] dirX = { 0, 1, 0, -1 };
	static int r, c, d;
	static Queue<Robot> q = new LinkedList<>();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String[] nm = br.readLine().split(" ");
		N = Integer.parseInt(nm[0]);
		M = Integer.parseInt(nm[1]);

		String[] rcd = br.readLine().split(" ");
		r = Integer.parseInt(rcd[0]);
		c = Integer.parseInt(rcd[1]);
		d = Integer.parseInt(rcd[2]);
		q.add(new Robot(r, c, d));

		board = new int[N][M];
		visited = new boolean[N][M];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < M; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		clean();
		System.out.println(count());
	}

	static void clean() {
		while (!q.isEmpty()) {
			Robot now = q.poll();
			int y = now.y;
			int x = now.x;
			int d = now.d;
			visited[y][x] = true;
			// 네방향 돌렸는지 여부
			int breakCounter = 0;
			int yy, xx, dd;
			while (breakCounter < 4) {
				// 왼쪽으로 회전
				if (d == 0) {
					dd = 3;
				} else {
					dd = d - 1;
				}

				yy = y + dirY[dd];
				xx = x + dirX[dd];
				if (yy < 0 || xx < 0 || yy >= N || xx >= M || 
						visited[yy][xx] || board[yy][xx] == 1) {
					d = dd;
					breakCounter++;
				} else {

					q.add(new Robot(yy, xx, dd));
					break;
				}
			}

			// 네 방향 모두 청소되어있거나 벽임 (한바퀴 돌았음)
			if (q.isEmpty()) {
				// 후진해본다
				yy = y - dirY[d];
				xx = x - dirX[d];

				// 후진 불가능하면 끝
				if (yy < 0 || xx < 0 || yy >= N || xx >= M || board[yy][xx] == 1) {
					return;
				}
				// 가능하면 다시 시작
				else {
					q.add(new Robot(yy, xx, d));
				}
			}
		}
	}

	static int count() {
		int cnt = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {

				if (visited[i][j])
					cnt++;
			}
		}
		return cnt;
	}
}

class Robot {
	public int y, x, d;

	public Robot(int y, int x, int d) {
		super();
		this.y = y;
		this.x = x;
		this.d = d;
	}

}
