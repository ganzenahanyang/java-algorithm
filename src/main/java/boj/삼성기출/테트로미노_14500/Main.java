package boj.삼성기출.테트로미노_14500;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {
	// 00:57
//	   북
//서		동
//    남

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

	}

	static int N, M;
	static int[][] board;
	static int[] dirY = { -1, 0, 1, 0 };
	static int[] dirX = { 0, 1, 0, -1 };
	static boolean[][] visit;
	static int answer = 0; // 최대합 구하기
	static ArrayList<Pair> blocks;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		init(br);

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				// ㅗ ㅓ ㅏ ㅜ
				etc(i, j);
				// ㅗ ㅓ ㅏ ㅜ 뺀 나머지

				DFS(0, i, j, board[i][j]);
			}
		}
		System.out.println(answer);
	}

	private static void etc(int y, int x) {
		// 가운데를 중심으로 해서 3방향
		// 4번 회전 가능
		int sum = 0;
		for (int i = 0; i < 4; i++) {
			Pair p1, p2, p3;
			p1 = new Pair(y + dirY[i % 4], x + dirX[i % 4]);
			p2 = new Pair(y + dirY[(i + 1) % 4], x + dirX[(i + 1) % 4]);
			p3 = new Pair(y + dirY[(i + 2) % 4], x + dirX[(i + 2) % 4]);

			if (p1.y < 0 || p1.x < 0 || p1.y >= N || p1.x >= M)
				continue;
			if (p2.y < 0 || p2.x < 0 || p2.y >= N || p2.x >= M)
				continue;
			if (p3.y < 0 || p3.x < 0 || p3.y >= N || p3.x >= M)
				continue;

			sum = Math.max(sum, board[y][x] + board[p1.y][p1.x] + board[p2.y][p2.x] + board[p3.y][p3.x]);
		}
		answer = Math.max(answer, sum);
	}

	private static void DFS(int depth, int y, int x, int sum) {
		visit[y][x] = true;
		if (depth == 3) {

			answer = Math.max(answer, sum);

			return;
		}

		for (int d = 0; d < 4; d++) {
			int yy = y + dirY[d];
			int xx = x + dirX[d];
			if (yy < 0 || xx < 0 || yy >= N || xx >= M || visit[yy][xx])
				continue;
			visit[yy][xx] = true;
			DFS(depth + 1, yy, xx, board[yy][xx] + sum);
			visit[yy][xx] = false;
		}
		visit[y][x] = false;
	}

	private static void init(BufferedReader br) throws IOException {
		String[] nm = br.readLine().split(" ");
		N = Integer.parseInt(nm[0]);
		M = Integer.parseInt(nm[1]);

		board = new int[N][M];
		blocks = new ArrayList<>();
		visit = new boolean[N][M];
		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < M; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

	}

}
