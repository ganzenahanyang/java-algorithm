package boj.삼성기출.사다리조작_15684;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

		public Pair(Pair p) {
			super();
			this.y = p.y;
			this.x = p.x;
		}

	}

	static int N, M, H;
	static int[][] board;
	static int answer = Integer.MAX_VALUE;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] nmh = br.readLine().split(" ");

		N = Integer.parseInt(nmh[0]);
		M = Integer.parseInt(nmh[1]);
		H = Integer.parseInt(nmh[2]);

		board = new int[H][N];

		for (int i = 0; i < M; i++) {
			String[] yx = br.readLine().split(" ");
			// 위치 보정
			int y = Integer.parseInt(yx[0]) - 1;
			int x = Integer.parseInt(yx[1]) - 1;
			board[y][x] = 1;
			board[y][x + 1] = 2;
		}

		for (int end = 0; end < 4; end++) {
			select(0, 0, 0, end);
		}

		if (answer == Integer.MAX_VALUE)
			answer = -1;

		System.out.println(answer);

	}

	static void select(int indexI, int indexJ, int depth, int end) {
		if (depth == end) {
			if (move()) {
				answer = Math.min(answer, end);
			}
			return;
		}

		for (int i = indexI; i < H; i++) {
			for (int j = indexJ; j < N - 1; j++) {
				if (board[i][j] != 0 || board[i][j + 1] != 0)
					continue;
				board[i][j] = 1;
				board[i][j + 1] = 2;
				if (j + 1 == N - 1) {
					select(i + 1, 0, depth + 1, end);
				} else {
					select(i, j + 1, depth + 1, end);
				}

				board[i][j] = 0;
				board[i][j + 1] = 0;
			}
			indexJ = 0;
		}
	}

	static boolean move() {

		for (int j = 0; j < N; j++) {

			int y = 0;
			int x = j;

			while (y < H) {
				// 1이면 오른쪽
				if (board[y][x] == 1) {
					++x;
				}
				// 2면 왼쪽
				else if (board[y][x] == 2) {
					--x;
				}
				++y;
			}

			if (x != j)
				return false;
		}

		return true;
	}

}
