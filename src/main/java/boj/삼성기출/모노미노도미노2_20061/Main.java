package boj.삼성기출.모노미노도미노2_20061;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

	static class Block {
		int no, y, x;

		public Block(int no, int y, int x) {
			super();
			this.no = no;
			this.y = y;
			this.x = x;
		}

		public Block(Block b) {
			super();
			this.no = b.no;
			this.y = b.y;
			this.x = b.x;
		}

	}

	static int[][] red, blue, green;
	static int N;
	static ArrayList<Block> blockList = new ArrayList<>();
	static int score = 0;
	static int blockCount = 0;
	static int count = 1;

	public static void main(String[] args) throws Exception {
		BufferedReader br;
//		br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\모노미노도미노2_20061\\input.txt"));
		br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine().split(" ")[0]);
		for (int i = 0; i < N; i++) {
			String[] txy = br.readLine().split(" ");
			int t = Integer.parseInt(txy[0]);
			int y = Integer.parseInt(txy[1]);
			int x = Integer.parseInt(txy[2]);
			blockList.add(new Block(t, y, x));
		}

		blue = new int[4][6];
		green = new int[6][4];

		for (int i = 0; i < blockList.size(); i++) {
			Block b = blockList.get(i);
			ArrayList<Block> temp = new ArrayList<>();
			if (b.no == 1) {
				temp.add(new Block(b.no, b.y, b.x));
			} else if (b.no == 2) {
				temp.add(new Block(b.no, b.y, b.x + 1));
				temp.add(new Block(b.no, b.y, b.x));

			} else if (b.no == 3) {
				temp.add(new Block(b.no, b.y + 1, b.x));
				temp.add(new Block(b.no, b.y, b.x));
			}

			blue(temp);
			green(temp);

			count++;
		}
		blockCount();

		System.out.println(score);
		System.out.println(blockCount);
	}

	static void green(ArrayList<Block> temp) {
		toGreen(temp);
		delAndMoveGreen();
		specialGreen();
	}

	static void toGreen(ArrayList<Block> temp) {
		// 아래로 이동할꺼니까 y 에 1을 계속 더해줌
		int finalY = 6; // 2번 블록 (가로 2칸)이 동시에 움직이게 하기위함
		int blockNo = 0;

		for (int i = 0; i < temp.size(); i++) {
			Block b = temp.get(i);
			int y = 0;
			int x = b.x;
			blockNo = b.no;
			++y;

			while (y != 6 && green[y][x] == 0) {
				++y;
			}
			--y;

			if (blockNo != 2) {
				green[y][x] = b.no;
			} else {
				if (finalY > y) {
					finalY = y;
				}
			}

		}

		if (blockNo == 2) {
			for (int i = 0; i < temp.size(); i++) {
				Block b = temp.get(i);
				int x = b.x;
				green[finalY][x] = b.no;
			}
		}
	}

	static void delAndMoveGreen() {
		for (int i = 5; i >= 2; i--) {
			boolean del = true;
			for (int j = 0; j < 4; j++) {
				if (green[i][j] == 0) {
					del = false;
					break;
				}
			}
			if (del) {
				for (int j = 0; j < 4; j++) {
					for (int k = i - 1; k >= 0; k--) {
						green[k + 1][j] = green[k][j];
						if (k == 0)
							green[k][j] = 0;
					}
				}
				++i;
				++score;
			}
		}
	}

	static void specialGreen() {

		int specialCnt = 0;
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 4; j++) {
				if (green[i][j] != 0) {
					++specialCnt;
					break;
				}
			}
		}
		if (specialCnt == 0)
			return;

		for (int i = 5 - specialCnt; i >= 0; i--) {
			for (int j = 0; j < 4; j++) {
				green[i + specialCnt][j] = green[i][j];
			}
		}

		for (int i = 0; i < specialCnt; i++) {
			for (int j = 0; j < 4; j++) {
				green[i][j] = 0;
			}
		}
	}

	static void blue(ArrayList<Block> temp) {
		toBlue(temp);
		delAndMoveBlue();
		specialBlue();
	}

	static void toBlue(ArrayList<Block> temp) {
		// 오른쪽으로 이동할꺼니까 x 에 1을 계속 더해줌
		int finalX = 6;
		int blockNo = 0;
		for (int i = 0; i < temp.size(); i++) {
			Block b = temp.get(i);
			blockNo = b.no;
			int y = b.y;
			int x = 0;
			++x;
			while (x != 6 && blue[y][x] == 0) {
				++x;
			}
			--x;

			if (blockNo != 3) {
				blue[y][x] = b.no;
			} else {
				// 2 * 1 블록을 통째로 옮기기 위함
				if (finalX > x) {
					finalX = x;
				}
			}
		}
		// 2 * 1 블록
		if (blockNo == 3) {
			for (int i = 0; i < temp.size(); i++) {
				Block b = temp.get(i);
				int y = b.y;

				blue[y][finalX] = b.no;
			}
		}

	}

	static void delAndMoveBlue() {
		for (int j = 5; j >= 2; j--) {
			boolean del = true;
			for (int i = 0; i < 4; i++) {
				if (blue[i][j] == 0) {
					del = false;
					break;
				}
			}
			if (del) {
				for (int i = 0; i < 4; i++) {
					for (int k = j - 1; k >= 0; k--) {
						blue[i][k + 1] = blue[i][k];
						if (k == 0)
							blue[i][k] = 0;
					}
				}
				++j;
				++score;

			}
		}

	}

	static void specialBlue() {
		int specialCnt = 0;
		for (int j = 0; j < 2; j++) {
			for (int i = 0; i < 4; i++) {
				if (blue[i][j] != 0) {
					++specialCnt;
					break;
				}
			}
		}
		if (specialCnt == 0)
			return;

		for (int i = 0; i < 4; i++) {
			for (int j = 5 - specialCnt; j >= 0; j--) {
				blue[i][j + specialCnt] = blue[i][j];
			}
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < specialCnt; j++) {
				blue[i][j] = 0;
			}
		}
	}

	static void blockCount() {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 6; j++) {
				if (blue[i][j] != 0)
					++blockCount;
			}
		}

		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 4; j++) {
				if (green[i][j] != 0)
					++blockCount;
			}
		}
	}
}