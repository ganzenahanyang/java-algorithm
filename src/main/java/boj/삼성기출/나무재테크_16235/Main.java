package boj.삼성기출.나무재테크_16235;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Tree implements Comparable<Tree> {
    int y, x, z;

    public Tree(int y, int x, int z) {
        super();
        this.y = y;
        this.x = x;
        this.z = z;
    }

    @Override
    public int compareTo(Tree o) {
        return this.z - o.z;
    }

}

class Tile {
    List<Tree> trees = new ArrayList<>();
    int food;

    public Tile(int food) {
        super();

        this.food = food;
    }

}

public class Main {

    static int N, M, K;

    static Tile[][] board;
    static List<Tree> deadTree;
    static int[] dirY = {0, 0, 1, 1, 1, -1, -1, -1}; // 8 방향
    static int[] dirX = {1, -1, 0, 1, -1, 0, 1, -1}; // 8 방향
    static int[][] robotFood;

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] nmk = br.readLine().split(" ");
        N = Integer.parseInt(nmk[0]);
        M = Integer.parseInt(nmk[1]);
        K = Integer.parseInt(nmk[2]);

        board = new Tile[N][N];
        robotFood = new int[N][N];
        for (int i = 0; i < N; i++) {
            String[] line = br.readLine().split(" ");
            for (int j = 0; j < N; j++) {
                int food = Integer.parseInt(line[j]);
                robotFood[i][j] = food; // 겨울에 로봇이 주입할 영양분
                board[i][j] = new Tile(5); // 땅의 기본 영양분은 5
            }
        }

        for (int i = 0; i < M; i++) {
            String[] line = br.readLine().split(" ");
            int y = Integer.parseInt(line[0]) - 1;
            int x = Integer.parseInt(line[1]) - 1;
            int z = Integer.parseInt(line[2]);
            board[y][x].trees.add(new Tree(y, x, z));
        }

        while (K-- > 0) {
            // 죽은 나무 리스트 초기화
            deadTree = new ArrayList<>();

            // 봄
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    if (!board[i][j].trees.isEmpty()) {
                        // 나이 오름차순으로 나무 정렬
                        Collections.sort(board[i][j].trees);
                        List<Tree> treeList = board[i][j].trees;

                        int k = 0;
                        for (; k < treeList.size(); k++) {
                            if (board[i][j].food >= treeList.get(k).z) {
                                board[i][j].food -= treeList.get(k).z;
                                treeList.get(k).z++;
                            } else {
                                for (; k < treeList.size(); k++) {
                                    // for문에서 다시++ 가 되므로 현재 지워진 인덱스값을 다음 차례에 보기위해 --
                                    deadTree.add(treeList.remove(k--));

                                }
                                break;
                            }
                        }

                    }
                }
            }

            // 여름
            for (Tree dead : deadTree) {
                board[dead.y][dead.x].food += dead.z / 2;
            }

            // 가을
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    List<Tree> trees = board[i][j].trees;
                    for (Tree t : trees) {
                        if (t.z % 5 == 0) {

                            for (int d = 0; d < 8; d++) {
                                int yy = t.y + dirY[d];
                                int xx = t.x + dirX[d];
                                if (yy < 0 || xx < 0 || yy >= N || xx >= N)
                                    continue;
                                board[yy][xx].trees.add(new Tree(yy, xx, 1));
                            }
                        }
                    }
                }
            }

            // 겨울
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < N; j++) {
                    board[i][j].food += robotFood[i][j];
                }
            }
        }

        // K년이 지난 후, 나무의 갯수
        int live = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                live += board[i][j].trees.size();
            }
        }

        System.out.println(live);
    }

}


