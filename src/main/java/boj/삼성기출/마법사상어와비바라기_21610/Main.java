package boj.삼성기출.마법사상어와비바라기_21610;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

	static class Cloud {
		int y, x;

		public Cloud(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

	}

	static class Move {
		int d, s;

		public Move(int d, int s) {
			super();
			this.d = d;
			this.s = s;
		}

	}

	static class WaterPlus {
		int y, x, plus;

		public WaterPlus(int y, int x, int plus) {
			super();
			this.y = y;
			this.x = x;
			this.plus = plus;
		}

	}

	static int[][] board;
	static int[][] copyBoard;
	static ArrayList<Cloud> cloudList = new ArrayList<>();
	static boolean[][] dead;
	static ArrayList<Move> command = new ArrayList<>();
	static int N, M;
	// ←, ↖, ↑, ↗, →, ↘, ↓, ↙
	static int[] dirY = { 0, -1, -1, -1, 0, 1, 1, 1 };
	static int[] dirX = { -1, -1, 0, 1, 1, 1, 0, -1 };
	static int[] bugY = { 1, 1, -1, -1 };
	static int[] bugX = { 1, -1, 1, -1 };
	static int answer = 0;

	public static void main(String[] args) throws IOException {
//		BufferedReader br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\마법사상어와비바라기_21610\\input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] nm = br.readLine().split(" ");
		N = Integer.parseInt(nm[0]);
		M = Integer.parseInt(nm[1]);

		board = new int[N][N];

		// 초기 구름 위치 (보정 필요)
		cloudList.add(new Cloud(N - 1, 0));
		cloudList.add(new Cloud(N - 1, 1));
		cloudList.add(new Cloud(N - 2, 0));
		cloudList.add(new Cloud(N - 2, 1));

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		for (int i = 0; i < M; i++) {
			String[] line = br.readLine().split(" ");
			int d = Integer.parseInt(line[0]);
			int s = Integer.parseInt(line[1]);
			command.add(new Move(d, s));
		}

		for (int i = 0; i < command.size(); i++) {
			Move c = command.get(i);
			moveCloud(c);
			rainFall();
			copyWater();
			makeCloud();
		}

		getSum();

		System.out.println(answer);
	}

	public static void getSum() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				answer += board[i][j];
			}
		}
	}

	public static void makeCloud() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (board[i][j] >= 2 && !dead[i][j]) {
					board[i][j] -= 2;
					cloudList.add(new Cloud(i, j));
				}
			}
		}
	}

	public static void copyWater() {
		copyBoard = new int[N][N];
		ArrayList<WaterPlus> history = new ArrayList<>();
		for (int i = 0; i < cloudList.size(); i++) {
			Cloud now = cloudList.get(i);
			int cnt = 0;
			for (int d = 0; d < 4; d++) {
				int yy = now.y + bugY[d];
				int xx = now.x + bugX[d];

				if (yy >= N || xx >= N || yy < 0 || xx < 0 || board[yy][xx] == 0)
					continue;

				cnt++;
			}

			history.add(new WaterPlus(now.y, now.x, cnt));

		}

		for (int i = 0; i < history.size(); i++) {
			WaterPlus now = history.get(i);
			board[now.y][now.x] += now.plus;
		}

		cloudList.clear();

	}

	public static void rainFall() {
		dead = new boolean[N][N];
		for (int i = 0; i < cloudList.size(); i++) {
			Cloud now = cloudList.get(i);
			board[now.y][now.x] += 1;
			dead[now.y][now.x] = true;
		}

	}

	public static void moveCloud(Move m) {
		int d = m.d - 1;
		int s = m.s;
		for (int i = 0; i < cloudList.size(); i++) {
			Cloud now = cloudList.get(i);
			int yy = now.y + dirY[d] * s % N;
			int xx = now.x + dirX[d] * s % N;

			if (yy < 0) {
				yy = N + yy;
			} else if (yy >= N) {
				yy = yy % N;
			}

			if (xx < 0) {
				xx = N + xx;
			} else if (xx >= N) {
				xx = xx % N;
			}

			now.y = yy;
			now.x = xx;
		}

	}

}
