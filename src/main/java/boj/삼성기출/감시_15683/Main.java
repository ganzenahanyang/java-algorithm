package boj.삼성기출.감시_15683;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {
	static class CCTV {
		int no, y, x, r;

		public CCTV(int no, int y, int x, int r) {
			super();
			this.no = no;
			this.y = y;
			this.x = x;
			this.r = r;

		}

	}

	static int N, M; // N * M 보드
	static int[] dirY = { 1, 0, -1, 0 }; // 북 0 동 1 남 2 서 3
	static int[] dirX = { 0, 1, 0, -1 };
	static int[][] board;
	static int answer = Integer.MAX_VALUE;
	static ArrayList<CCTV> cctvList = new ArrayList<>();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] nm = br.readLine().split(" ");
		N = Integer.parseInt(nm[0]);
		M = Integer.parseInt(nm[1]);
		board = new int[N][M];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < M; j++) {
				board[i][j] = Integer.parseInt(line[j]);
				if (board[i][j] != 6 && board[i][j] != 0) {
					int no = board[i][j];
					int rotateNum = 0;
					if (no == 2)
						rotateNum = 2;
					else if (no == 5)
						rotateNum = 1;
					else
						rotateNum = 4;
					cctvList.add(new CCTV(no, i, j, rotateNum)); 
				}
			}
		}

		allCase(0);
		System.out.println(answer);

	}

	static void allCase(int depth) {
		if (depth == cctvList.size()) {
			getBlindSize();
			return;
		}

		CCTV c = cctvList.get(depth);
		int y = c.y;
		int x = c.x;
		int d = 0;
		for (int j = 0; j < c.r; j++) {
			int[][] copy = new int[N][M];
			toCopy(copy);
			if (c.no == 1) {
				marking(y, x, d);
			} else if (c.no == 2) {
				marking(y, x, d);
				marking(y, x, (d + 2) % 4);
			} else if (c.no == 3) {
				marking(y, x, d);
				marking(y, x, (d + 1) % 4);
			} else if (c.no == 4) {
				marking(y, x, d);
				marking(y, x, (d + 1) % 4);
				marking(y, x, (d + 2) % 4);
			} else {
				marking(y, x, d);
				marking(y, x, (d + 1) % 4);
				marking(y, x, (d + 2) % 4);
				marking(y, x, (d + 3) % 4);
			}
			allCase(depth + 1);
			d = (d + 1) % 4;
			toOrigin(copy);
		}

	}

	static void toOrigin(int[][] copy) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				board[i][j] = copy[i][j];
			}
		}
	}

	static void toCopy(int[][] copy) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				copy[i][j] = board[i][j];
			}
		}
	}

	static void marking(int y, int x, int d) {
		while (true) {
			y = y + dirY[d];
			x = x + dirX[d];

			if (y < 0 || x < 0 || y >= N || x >= M || board[y][x] == 6)
				break;

			if (board[y][x] == 0) // 빈칸인 경우에만 마킹, CCTV는 통과
				board[y][x] = -1;
		}
	}

	static void getBlindSize() {
		int sum = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				if (board[i][j] == 0)
					++sum;
			}
		}
		answer = Math.min(answer, sum);
	}
}
