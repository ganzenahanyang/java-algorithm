package boj.삼성기출.이공사팔easy_12100;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	static int N;
	static int[][] board;
	static int answer;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		init(br);
		solve(0);

		System.out.println(answer);
	}

	private static void solve(int depth) {
		if (depth == 5) {
			getMax();
			return;
		}

		for (int d = 0; d < 4; d++) {
			int[][] copy = new int[N][N];
			originToCopy(copy);
			move(d);
			solve(depth + 1);

			copyToOrigin(copy);
		}

	}

	private static void getMax() {
		int tempMax = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				tempMax = Math.max(tempMax, board[i][j]);
			}
		}

		answer = Math.max(tempMax, answer);

	}

	private static void move(int d) {
		boolean[][] visit = new boolean[N][N];
		// 북쪽
		if (d == 0) {
			for (int j = 0; j < N; j++) {
				for (int i = 1; i < N; i++) {
					if (board[i][j] == 0)
						continue;
					int y = i;
					int x = j;

					while (--y >= 0) {
						if (board[y][x] != 0)
							break;
					}

					if (y < 0) {
						y = 0;
						board[y][x] = board[i][j];
						board[i][j] = 0;
					} else {
						if (board[y][x] == board[i][j]) {
							if (visit[y][x]) {
								board[y + 1][x] = board[i][j];
								if (y + 1 != i) {
									board[i][j] = 0;
								}
							} else {
								board[y][x] *= 2;
								board[i][j] = 0;
								visit[y][x] = true;
							}
						} else {
							board[y + 1][x] = board[i][j];
							if (y + 1 != i) {
								board[i][j] = 0;
							}
						}
					}
				}
			}
		}
		// 남쪽
		else if (d == 2) {
			for (int j = 0; j < N; j++) {
				for (int i = N - 2; i >= 0; i--) {
					if (board[i][j] == 0)
						continue;
					int y = i;
					int x = j;

					while (++y < N) {
						if (board[y][x] != 0)
							break;
					}

					if (y >= N) {
						y = N - 1;
						board[y][x] = board[i][j];
						board[i][j] = 0;
					} else {
						if (board[y][x] == board[i][j]) {
							if (visit[y][x]) {
								board[y - 1][x] = board[i][j];
								if (y - 1 != i) {
									board[i][j] = 0;
								}
							} else {
								board[y][x] *= 2;
								board[i][j] = 0;
								visit[y][x] = true;
							}
						} else {
							board[y - 1][x] = board[i][j];
							if (y - 1 != i) {
								board[i][j] = 0;
							}
						}
					}
				}
			}
		}

		// 동쪽
		else if (d == 1) {
			for (int i = 0; i < N; i++) {
				for (int j = N - 2; j >= 0; j--) {
					if (board[i][j] == 0)
						continue;

					int y = i;
					int x = j;

					while (++x < N) {
						if (board[y][x] != 0)
							break;
					}

					if (x >= N) {
						x = N - 1;
						board[y][x] = board[i][j];
						board[i][j] = 0;
					} else if (board[y][x] == board[i][j]) {

						if (visit[y][x]) {

							board[y][x - 1] = board[i][j];
							if (x - 1 != j) {
								board[i][j] = 0;
							}
						} else {
							board[y][x] *= 2;
							board[i][j] = 0;
							visit[y][x] = true;
						}
					} else {
						board[y][x - 1] = board[i][j];
						if (x - 1 != j) {
							board[i][j] = 0;
						}
					}
				}
			}

		}
		// 서쪽
		else if (d == 3) {
			for (int i = 0; i < N; i++) {
				for (int j = 1; j < N; j++) {
					if (board[i][j] == 0)
						continue;

					int y = i;
					int x = j;

					while (--x >= 0) {
						if (board[y][x] != 0)
							break;
					}

					if (x < 0) {
						x = 0;
						board[y][x] = board[i][j];
						board[i][j] = 0;
					} else if (board[y][x] == board[i][j]) {
						if (visit[y][x]) {
							board[y][x + 1] = board[i][j];
							if (x + 1 != j) {
								board[i][j] = 0;
							}
						} else {
							board[y][x] *= 2;
							board[i][j] = 0;
							visit[y][x] = true;
						}
					} else {
						board[y][x + 1] = board[i][j];
						if (x + 1 != j) {
							board[i][j] = 0;
						}
					}
				}
			}
		}
	}

	private static void copyToOrigin(int[][] copy) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				board[i][j] = copy[i][j];
			}
		}
	}

	private static void originToCopy(int[][] copy) {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				copy[i][j] = board[i][j];
			}
		}
	}

	private static void init(BufferedReader br) throws NumberFormatException, IOException {
		N = Integer.parseInt(br.readLine());
		board = new int[N][N];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		answer = 0;

	}

}
