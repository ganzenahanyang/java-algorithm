package boj.삼성기출.인구이동_16234;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

	}

	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static int[][] board;
	static int[][] visit;
	static int unionNo = 1;
	static int day = 0;
	static int N, L, R;
	static HashMap<Integer, ArrayList<Pair>> hm;

	public static void main(String[] args) throws Exception {
//		BufferedReader br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\인구이동_16234\\input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String[] nlr = br.readLine().split(" ");
		N = Integer.parseInt(nlr[0]);
		L = Integer.parseInt(nlr[1]);
		R = Integer.parseInt(nlr[2]);

		board = new int[N][N];
		hm = new HashMap<>();
		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);

			}
		}

		while (true) {
			visit = new int[N][N];
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					if (visit[i][j] == 0) {
						makeUnion(i, j);
					}
				}
			}

			if (hm.isEmpty())
				break;

			divideEnd();
			++day;
		}

		System.out.println(day);

	}

	static void makeUnion(int i, int j) {
		ArrayList<Pair> union = new ArrayList<>();
		Pair p = new Pair(i, j);
		Queue<Pair> q = new LinkedList<>();
		q.add(p);
		visit[p.y][p.x] = unionNo;
		while (!q.isEmpty()) {
			Pair now = q.poll();
			int y = now.y;
			int x = now.x;
			union.add(new Pair(y, x));
			for (int d = 0; d < 4; d++) {
				int yy = now.y + dirY[d];
				int xx = now.x + dirX[d];

				if (yy < 0 || xx < 0 || yy >= N || xx >= N || visit[yy][xx] != 0)
					continue;

				int gap = Math.abs(board[y][x] - board[yy][xx]);
				if (gap < L || gap > R)
					continue;

				visit[yy][xx] = unionNo;
				q.add(new Pair(yy, xx));
			}
		}

		if (union.size() == 1) {
			return;
		}

		hm.put(unionNo, union);
		++unionNo;
	}

	static void divideEnd() {
		for (Integer key : hm.keySet()) {
			ArrayList<Pair> union = hm.get(key);
			int total = 0;
			for (int i = 0; i < union.size(); i++) {
				Pair p = union.get(i);
				total += board[p.y][p.x];
			}
			int newNum = total / union.size();
			for (int i = 0; i < union.size(); i++) {
				Pair p = union.get(i);
				board[p.y][p.x] = newNum;
			}
		}

		hm.clear();
		unionNo = 1;
	}

}
