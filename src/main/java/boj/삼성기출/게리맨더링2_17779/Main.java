package boj.삼성기출.게리맨더링2_17779;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

	}

	static int N;
	static int[][] board;
	static int[][] union; // 매번 초기화 시킬 것
	static int gap = Integer.MAX_VALUE;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };

	public static void main(String[] args) throws IOException {
		BufferedReader br;
		br = new BufferedReader(new InputStreamReader(System.in));
//		br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\게리맨더링2_17779\\input.txt"));

		N = Integer.parseInt(br.readLine().split(" ")[0]);

		board = new int[N][N];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		union = new int[N][N];
		search();
		System.out.println(gap);
		br.close();
	}

	static void search() {
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				for (int d1 = 1; d1 < N; d1++) {
					for (int d2 = 1; d2 < N; d2++) {
						if (i + d1 >= N || j - d1 < 0)
							continue;
						if (i + d2 >= N || j + d2 >= N)
							continue;

						make(i, j, d1, d2);
					}
				}
			}
		}

	}

	private static void make(int y, int x, int d1, int d2) {
		union = new int[N][N];
		union[y][x] = 5;
		// 복쪽에서 왼쪽 아래로
		int yy = y;
		int xx = x;
		for (int i = 0; i < d1; i++) {
			yy = yy + 1;
			xx = xx - 1;
			if (yy >= N || xx >= N || yy < 0 || xx < 0)
				return;
			union[yy][xx] = 5;
		}

		// 서쪽에서 오른쪽 아래로
		for (int i = 0; i < d2; i++) {
			yy = yy + 1;
			xx = xx + 1;
			if (yy >= N || xx >= N || yy < 0 || xx < 0)
				return;
			union[yy][xx] = 5;
		}

		// 남쪽에서 오른쪽 위로
		for (int i = 0; i < d1; i++) {
			yy = yy - 1;
			xx = xx + 1;
			if (yy >= N || xx >= N || yy < 0 || xx < 0)
				return;
			union[yy][xx] = 5;
		}

		// 동쪽에서 왼쪽 위로
		for (int i = 0; i < d2; i++) {
			yy = yy - 1;
			xx = xx - 1;
			if (yy >= N || xx >= N || yy < 0 || xx < 0)
				return;
			union[yy][xx] = 5;
		}

		yy = y;
		xx = x;
		for (int i = 0; i < d1; i++) {
			Queue<Pair> q = new LinkedList<>();
			q.add(new Pair(yy + 1, xx));
			union[yy + 1][xx] = 5;
			while (!q.isEmpty()) {
				Pair now = q.poll();
				int nowY = now.y;
				int nowX = now.x;
				union[nowY][nowX] = 5;

				int nextY = nowY + 1;
				int nextX = nowX;
				if (union[nextY][nextX] == 5)
					break;
				q.add(new Pair(nextY, nextX));

			}
			yy = yy + 1;
			xx = xx - 1;
		}

		yy = y;
		xx = x;
		for (int i = 0; i < d2; i++) {
			Queue<Pair> q = new LinkedList<>();
			q.add(new Pair(yy + 1, xx));
			union[yy + 1][xx] = 5;
			while (!q.isEmpty()) {
				Pair now = q.poll();
				int nowY = now.y;
				int nowX = now.x;
				union[nowY][nowX] = 5;

				int nextY = nowY + 1;
				int nextX = nowX;
				if (union[nextY][nextX] == 5)
					break;
				q.add(new Pair(nextY, nextX));

			}
			yy = yy + 1;
			xx = xx + 1;
		}

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (union[i][j] != 0)
					continue;
				if (i >= 0 && i < y + d1 && j >= 0 && j <= x) {
					union[i][j] = 1;
				} else if (i >= 0 && i <= y + d2 && j > x && j <= N - 1) {
					union[i][j] = 2;
				} else if (i >= y + d1 && i <= N - 1 && j >= 0 && j < x - d1 + d2) {
					union[i][j] = 3;
				} else if (i > y + d2 && i <= N - 1 && j >= x - d1 + d2 && j <= N - 1) {
					union[i][j] = 4;
				} else {
					union[i][j] = 5;
				}
			}
		}
		int[] number = new int[6];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				number[union[i][j]] += board[i][j];
			}
		}

		Arrays.sort(number);
		int tempGap = number[5] - number[1];

		gap = Math.min(gap, tempGap);

	}

}
