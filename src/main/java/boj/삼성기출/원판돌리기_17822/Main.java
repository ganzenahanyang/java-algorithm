package boj.삼성기출.원판돌리기_17822;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

	static class Command {
		int no; // 배수 번호 원판 회전
		int d; // 회전 방향
		int k; // k칸회전

		public Command(int no, int d, int k) {
			super();
			this.no = no;
			this.d = d;
			this.k = k;
		}

	}

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

	}

	static int N, M, T; // 원판크기 M개의 정수 T번 회전
	static int[][] board;
	static boolean[][] visit;
	static Queue<Command> cmdQ;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		init(br);

		while (!cmdQ.isEmpty()) {
			rotate(cmdQ.poll());

			if (select() == 0)
				if (!avgFunc())
					break;

		}

		System.out.println(getAnswer());
	}

	private static int getAnswer() {
		int sum = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				sum += board[i][j];
			}
		}

		return sum;
	}

	private static boolean avgFunc() {
		int sum = 0, cnt = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				sum += board[i][j];
				if (board[i][j] > 0)
					++cnt;
			}
		}
		if (cnt == 0) {
			return false;
		}

		double avg = (double) sum / cnt;

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				if (board[i][j] != 0) {
					if (board[i][j] > avg)
						--board[i][j];
					else if (board[i][j] < avg)
						++board[i][j];
				}
			}
		}
		return true;
	}

	private static int select() {
		visit = new boolean[N][M];
		int adjNum = 0;
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				// 방문하지 않고 숫자가 남아있는 경우
				if (!visit[i][j] && board[i][j] != 0)
					adjNum += BFS(i, j);
			}
		}
		// 방문된 위치 모두 0으로 만들기
		if (adjNum != 0) {
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < M; j++) {
					if (visit[i][j])
						board[i][j] = 0;
				}
			}
		}

		return adjNum;

	}

	private static int BFS(int i, int j) {
		int adjNum = 0;
		Queue<Pair> q = new LinkedList<>();
		q.add(new Pair(i, j));
		visit[i][j] = true;
		int target = board[i][j]; // 이거랑 같은거 찾기
		
		while (!q.isEmpty()) {
			Pair now = q.poll();
			int y = now.y;
			int x = now.x;
			++adjNum;

			for (int d = 0; d < 4; d++) {
				int yy = y + dirY[d];
				int xx = x + dirX[d];

				if (yy >= N || yy < 0)
					continue;

				// 원판을 구성했을때 0열과 M-1열은 연결
				if (xx >= M) {
					xx = 0;
				} else if (xx < 0) {
					xx = M - 1;
				}

				if (board[yy][xx] != target || visit[yy][xx])
					continue;
				visit[yy][xx] = true;
				q.add(new Pair(yy, xx));

			}
		}

		if (adjNum <= 1) {
			adjNum = 0;
			visit[i][j] = false;
		}

		return adjNum;
	}

	private static void rotate(Command c) {
		for (int i = 0; i < N; i++) {
			if ((i + 1) % c.no == 0) { // 배수 판
				int[] temp = new int[M];
				for (int j = 0; j < M; j++) {
					if (c.d == 0) { // 시계
						temp[(j + c.k) % M] = board[i][j];
					} else { // 반시계
						temp[(j + M - c.k) % M] = board[i][j];
					}
				}

				for (int j = 0; j < M; j++) {
					board[i][j] = temp[j];
				}
			}
		}

	}

	private static void init(BufferedReader br) throws IOException {
		String[] nmt = br.readLine().split(" ");
		N = Integer.parseInt(nmt[0]);
		M = Integer.parseInt(nmt[1]);
		T = Integer.parseInt(nmt[2]);

		board = new int[N][M];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < M; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		cmdQ = new LinkedList<>();
		for (int i = 0; i < T; i++) {
			String[] line = br.readLine().split(" ");
			int no = Integer.parseInt(line[0]);
			int d = Integer.parseInt(line[1]);
			int k = Integer.parseInt(line[2]);
			cmdQ.add(new Command(no, d, k));
		}
	}

}
