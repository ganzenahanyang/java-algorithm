package boj.삼성기출.마법사상어와블리자드_21611;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

	static class Magic {
		int d, s;

		public Magic(int d, int s) {
			super();
			this.d = d;
			this.s = s;
		}

	}

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

	}

	// ↑, ↓, ←, →
	static int[] dirY = { -1, 1, 0, 0 };
	static int[] dirX = { 0, 0, -1, 1 };

	static int[] chainY = { 0, 1, 0, -1 };
	static int[] chainX = { -1, 0, 1, 0 };
	static int[][] board;
	static ArrayList<Magic> commands = new ArrayList<>();
	static ArrayList<Integer> chains;
	static Pair shark;
	static int N, M;
	static int[] balls;
	static int answer = 0;

	public static void main(String[] args) throws IOException {
//		BufferedReader br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\마법사상어와블리자드_21611\\input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] nm = br.readLine().split(" ");
		N = Integer.parseInt(nm[0]);
		M = Integer.parseInt(nm[1]);

		board = new int[N][N];
		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		for (int i = 0; i < M; i++) {
			String[] ds = br.readLine().split(" ");
			int d = Integer.parseInt(ds[0]) - 1;
			int s = Integer.parseInt(ds[1]);

			commands.add(new Magic(d, s));
		}

		shark = new Pair(N / 2, N / 2);
		balls = new int[4];

		for (int i = 0; i < commands.size(); i++) {
			doMagic(commands.get(i));
			makeChain();

			boolean go = true;
			while (go) {
				ballMove();
				go = ballBomb();
			}

			ballDivide();
			makeBoard();
		}

		for (int i = 1; i < 4; i++) {
			answer += balls[i] * i;
		}

		System.out.println(answer);
	}

	static void ballDivide() {
		ArrayList<Integer> temp = new ArrayList<>();

		int target = 0;
		int groupCnt = 0;
		for (int i = 0; i < chains.size(); i++) {
			int now = chains.get(i);
			if (target == 0) {
				target = now;
				++groupCnt;
			} else {
				if (target == now) {
					++groupCnt;

				} else {
					temp.add(groupCnt);
					temp.add(target);
					groupCnt = 1;
					target = now;

				}
				if (i == chains.size() - 1) {
					temp.add(groupCnt);
					temp.add(target);
				}
			}
		}
		while (temp.size() > N * N - 1) { // chain의 최대 길이는 N * N - 1
			temp.remove(N * N - 1);
		}

		chains.clear();
		chains.addAll(temp);
	}

	static boolean ballBomb() {

		int target = 0; // 터질 구슬의 번호
		int groupCnt = 0; // 그룹 몇개 들어갔나?
		int groupStart = -1; // 그룹이 시작 번호

		boolean isBomb = false;
		for (int i = 0; i < chains.size(); i++) {
			int now = chains.get(i);
			if (target == 0) { // 초기상태
				target = now;
				++groupCnt;
				groupStart = 0;
			} else {
				if (target == now) {
					++groupCnt;
					if (i == chains.size() - 1 && groupCnt >= 4) { // 마지막까지 4개이상 그룹화되었을 경우 처리
						while (groupCnt-- > 0) {
							++balls[target]; // 구슬 카운트
							chains.set(groupStart++, 0); // 폭파자리는 0으로
						}
						isBomb = true;
					}
				} else {
					if (groupCnt >= 4) { // 폭파
						while (groupCnt-- > 0) {
							++balls[target]; // 구슬 카운트
							chains.set(groupStart++, 0); // 폭파자리는 0으로
						}
						isBomb = true;
					}

					groupCnt = 1;
					groupStart = i;
					target = now;
				}
			}
		}

		return isBomb;
	}

	static void ballMove() {
		ArrayList<Integer> temp = new ArrayList<Integer>();
		for (int i = 0; i < chains.size(); i++) {
			if (chains.get(i) == 0)
				continue;
			temp.add(chains.get(i));
		}
		chains.clear();
		chains.addAll(temp);
	}

	static void makeBoard() {
		int y = shark.y;
		int x = shark.x;
		int loop = 0;
		int len = 1; // 11 22 33 44 순으로 빙글빙글
		int d = 0; // ←, ↓, →, ↑
		int index = 0;
		board = new int[N][N];
		boolean go = true;
		if (chains.isEmpty()) {
			return;
		}
		while (true) {
			for (int i = 0; i < len; i++) {
				int yy = y + chainY[d];
				int xx = x + chainX[d];

				if (yy >= N || xx >= N || yy < 0 || xx < 0) {
					go = false;
					break;
				}
				board[yy][xx] = chains.get(index);

				if (++index >= chains.size()) {
					go = false;
					break;
				}

				y = yy;
				x = xx;

			}
			if (!go)
				break;

			++loop;

			if (loop == 2) {
				loop = 0;
				++len;
			}

			d += 1;
			d %= 4;

		}
	}

	static void makeChain() {
		chains = new ArrayList<>();
		int y = shark.y;
		int x = shark.x;
		int loop = 0;
		int len = 1; // 11 22 33 44 순으로 빙글빙글
		int d = 0; // ←, ↓, →, ↑

		boolean go = true;
		while (true) {
			for (int i = 0; i < len; i++) {
				int yy = y + chainY[d];
				int xx = x + chainX[d];

				if (yy >= N || xx >= N || yy < 0 || xx < 0) {
					go = false;
					break;
				}
				if (board[yy][xx] != 0)
					chains.add(board[yy][xx]);

				y = yy;
				x = xx;

			}
			if (!go)
				break;

			++loop;

			if (loop == 2) {
				loop = 0;
				++len;
			}

			d += 1;
			d %= 4;

		}
	}

	static void doMagic(Magic m) {
		int d = m.d;
		int s = m.s;
		int x = shark.x;
		int y = shark.y;
		for (int i = 0; i < s; i++) {
			int yy = y + dirY[d];
			int xx = x + dirX[d];

			if (yy >= N || xx >= N || yy < 0 || xx < 0)
				break;

			board[yy][xx] = 0;
			y = yy;
			x = xx;
		}
	}

}
