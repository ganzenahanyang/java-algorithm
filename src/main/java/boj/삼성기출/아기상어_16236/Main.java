package boj.삼성기출.아기상어_16236;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

	static class Fish {
		int y, x;
		int size;
		int exp;

		public Fish(int y, int x, int size, int exp) {
			super();
			this.y = y;
			this.x = x;
			this.size = size;
			this.exp = exp;
		}

	}

	static class Pair implements Comparable<Pair> {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

		@Override
		public int compareTo(Pair o) {
			if (this.y == o.y)
				return this.x - o.x;
			return this.y - o.y;
		}

	}

	static int N;
	static int[][] board;
	static int[][] visit;
	static Fish shark;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static int fishCnt = 0;
	static int deadCnt = 0;
	static int time = 0;

	public static void main(String[] args) throws IOException {
//		BufferedReader br = new BufferedReader(new FileReader(
//				"C:\\Users\\admin\\git\\java-algorithm\\src\\main\\java\\boj\\삼성기출\\아기상어_16236\\input.txt"));
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine().split(" ")[0]);
		board = new int[N][N];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
				if (board[i][j] == 9) {
					shark = new Fish(i, j, 2, 0);
				} else if (board[i][j] != 0) {
					fishCnt++;
				}
			}
		}

		while (deadCnt != fishCnt) {
			if (!sharkMove())
				break;

		}

		System.out.println(time);

	}

	public static boolean sharkMove() {
		int minDist = -1;
		visit = new int[N][N];
		Queue<Pair> q = new LinkedList<>();
		ArrayList<Pair> candi = new ArrayList<>();
		visit[shark.y][shark.x] = 1;
		q.add(new Pair(shark.y, shark.x));
		while (!q.isEmpty()) {
			Pair now = q.poll();
			int y = now.y;
			int x = now.x;
			for (int i = 0; i < 4; i++) {
				int yy = y + dirY[i];
				int xx = x + dirX[i];

				if (yy >= N || xx >= N || yy < 0 || xx < 0 || visit[yy][xx] != 0 || board[yy][xx] > shark.size)
					continue;

				visit[yy][xx] = visit[y][x] + 1;
				if (board[yy][xx] != 0 && board[yy][xx] != 9 && board[yy][xx] < shark.size) {
					if (minDist == -1) {
						minDist = visit[yy][xx];
						candi.add(new Pair(yy, xx));
					} else {
						if (minDist == visit[yy][xx]) {
							candi.add(new Pair(yy, xx));
						}
					}
				}
				q.add(new Pair(yy, xx));
			}
		}

		if (candi.isEmpty())
			return false;

		Collections.sort(candi);

		Pair target = candi.get(0);
		board[shark.y][shark.x] = 0;
		board[target.y][target.x] = 9;
		++deadCnt;
		shark.y = target.y;
		shark.x = target.x;
		++shark.exp;
		if (shark.exp == shark.size) {
			shark.exp = 0;
			++shark.size;
		}
		time += --minDist; // 제자리 비용이 1이어서 보정

		return true;

	}
}
