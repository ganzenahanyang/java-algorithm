package boj.삼성기출.경사로_14890;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	static int N, L;
	static int[][] board;
	static boolean[][] visit;
	static int answer = 0;

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		init(br);
		count();
		System.out.println(answer);

	}

	private static void count() {
		visit = new boolean[N][N];

		for (int i = 0; i < N; i++) {
			if (horizontalMove(i))
				++answer;
		}
		visit = new boolean[N][N];

		for (int j = 0; j < N; j++) {
			if (verticalMove(j))
				++answer;
		}

	}

	private static boolean verticalMove(int j) { // 위에서 아래로 이동
		int y = 0;
		int x = j;
		int h = board[y][x];
		while (y < N - 1) {
			if (board[y + 1][x] == h) {
				++y;
			} else {
				if (Math.abs(h - board[y + 1][x]) == 1) {
					// 올라가기 전에 다리 놓을 수 있는지 확인
					if (board[y + 1][x] > h) {
						for (int i = 0; i < L; i++) {
							if (y - i < 0 || board[y - i][x] != h || visit[y - i][x]) {
								return false;
							}
						}
						// 다리놓기
						for (int i = 0; i < L; i++) {
							visit[y - i][x] = true;
						}
						// 이동
						++y;
						++h;
					} else {
						// 내려가기전에 다리 놓을 수 있나 확인
						for (int i = 1; i <= L; i++) {
							if (y + i >= N || board[y + i][x] != h - 1) {
								return false;
							}
						}
						//다리 놓기
						for (int i = 1; i <= L; i++) {
							visit[y + i][x] = true;
						}
						// 다리 길이만큼 이동
						y += L;
						--h;
					}
				} else {
					return false;
				}
			}
		}

		return true;

	}

	private static boolean horizontalMove(int i) { // 왼쪽에서 오른쪽으로 이동
		int y = i;
		int x = 0;
		int h = board[y][x]; // 현재 높이
		while (x < N - 1) {

			if (board[y][x + 1] == h) { // 같은 높이일 경우 그냥 이동
				++x;
			} else {
				if (Math.abs(h - board[y][x + 1]) == 1) { // 높이차가 1
					if (board[y][x + 1] > h) { // 올라가야 할 때
						for (int j = 0; j < L; j++) {
							// 경사로 못 놓거나 같은 높이가 아니거나 다리가 이미 있거
							if (x - j < 0 || board[y][x - j] != h || visit[y][x - j]) {
								return false;
							}

						}
						// 다리를 놓는다.
						for (int j = 0; j < L; j++) {
							visit[y][x - j] = true;
						}

						++x; 
						++h;
					} else { // 내려가야함
						// 내려가기전에 다리 놓을 수 있나 확인
						for (int j = 1; j <= L; j++) {
							if (x + j >= N || board[y][x + j] != h - 1) {
								return false;
							}
						}

						// 다리 놓기
						for (int j = 1; j <= L; j++) {
							visit[y][x + j] = true;
						}
						// 다리 길이만큼 이동
						x += L;
						--h;

					}
				} else {
					return false;
				}
			}
		}

		return true;

	}

	static void init(BufferedReader br) throws IOException {
		String[] nl = br.readLine().split(" ");
		N = Integer.parseInt(nl[0]);
		L = Integer.parseInt(nl[1]);

		board = new int[N][N];
		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}
	}

}
