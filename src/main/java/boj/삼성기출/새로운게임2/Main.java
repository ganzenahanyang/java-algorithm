package boj.삼성기출.새로운게임2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

	static class Horse {
		int no, y, x, d;

		public Horse(int no, int y, int x, int d) {
			super();
			this.no = no;
			this.y = y;
			this.x = x;
			this.d = d;
		}
	}

	static class Tile {
		ArrayList<Integer> hList = new ArrayList<>();
	}

	static int N, K;
	static int[] dirY = { 0, 0, -1, 1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static Tile[][] hBoard; // 쌓인거 체크
	static int[][] board; // 색깔
	static ArrayList<Horse> hInfo; // 말 정보

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] nk = br.readLine().split(" ");
		N = Integer.parseInt(nk[0]);
		K = Integer.parseInt(nk[1]);

		board = new int[N][N];
		hBoard = new Tile[N][N];
		hInfo = new ArrayList<>();
		// 번호로 접근하기 위한 허수값
		hInfo.add(new Horse(-1, -1, -1, -1));

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				hBoard[i][j] = new Tile();
			}
		}

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		for (int i = 1; i <= K; i++) {
			String[] line = br.readLine().split(" ");
			// 값 보정
			int y = Integer.parseInt(line[0]) - 1;
			int x = Integer.parseInt(line[1]) - 1;
			int d = Integer.parseInt(line[2]) - 1;
			hInfo.add(new Horse(i, y, x, d));
			hBoard[y][x].hList.add(i);
		}

		int cnt = 1;
		while (cnt <= 1000) {
			boolean end = false;
			for (int i = 1; i <= K; i++) {
				Horse h = hInfo.get(i);
				move(h);
				// 이동한 위치에 4개 이상 쌓였는지 확인
				if (check4(h)) {
					end = true;
					break;
				}
			}
			if (end == true)
				break;

			++cnt;
		}

		if (cnt > 1000)
			System.out.println(-1);
		else
			System.out.println(cnt);

	}

	private static boolean check4(Horse h) {
		return hBoard[h.y][h.x].hList.size() >= 4 ? true : false;
	}

	private static void redWhite(Horse h) {
		int yy = h.y + dirY[h.d];
		int xx = h.x + dirX[h.d];

		ArrayList<Integer> nowList = hBoard[h.y][h.x].hList; // 현 위치에 쌓여있는 말들
		int nextColor = board[yy][xx];

		ArrayList<Integer> moveList = new ArrayList<>();
		boolean okay = false;
		for (int i = 0; i < nowList.size(); i++) {
			if (okay) {
				moveList.add(nowList.get(i));
				nowList.remove(i--);
			} else {
				if (h.no == nowList.get(i)) {
					okay = true;
					moveList.add(nowList.get(i));
					// 삭제할 때 인덱스 조정 필요
					nowList.remove(i--);
				}
			}
		}
		if (nextColor == 1) { // 빨강일 경우 역순
			for (int i = moveList.size() - 1; i >= 0; i--) {
				hBoard[yy][xx].hList.add(moveList.get(i).intValue());
			}
		} else {
			hBoard[yy][xx].hList.addAll(moveList);
		}
		// 이동한 말들에 대한 위치 수정
		for (int i = 0; i < moveList.size(); i++) {
			hInfo.get(moveList.get(i)).y = yy;
			hInfo.get(moveList.get(i)).x = xx;
		}

	}

	private static void blue(Horse h) {
		int yy = h.y + dirY[h.d];
		int xx = h.x + dirX[h.d];

		h.d = reverse(h.d);
		yy = h.y + dirY[h.d];
		xx = h.x + dirX[h.d];
		// 방향 바꿨는데도 파랑이거나 경계 넘어가면 멈춤
		if (yy < 0 || xx < 0 || yy >= N || xx >= N || board[yy][xx] == 2)
			return;
		// 방향 바꿔서 이동할 수 있으면 이동
		redWhite(h);
	}

	private static void move(Horse h) {
		int yy = h.y + dirY[h.d];
		int xx = h.x + dirX[h.d];

		// 밖에 나가거나 파랑일 경우
		if (yy < 0 || xx < 0 || yy >= N || xx >= N || board[yy][xx] == 2) {
			blue(h);
		}
		// 흰색이나 빨강일 경우
		else {
			redWhite(h);
		}

	}

	private static int reverse(int d) {
		if (d == 0)
			return 1;
		else if (d == 1)
			return 0;
		else if (d == 2)
			return 3;
		else
			return 2;
	}

}
