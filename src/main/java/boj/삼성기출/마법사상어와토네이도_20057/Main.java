package boj.삼성기출.마법사상어와토네이도_20057;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	static class Tor {
		int y, x, d;

		public Tor(int y, int x, int d) {
			super();
			this.y = y;
			this.x = x;
			this.d = d;
		}

	}

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

	}

	static int N;
	static int outSand = 0;
	static int[][] board;
	static int[] dirY = { 0, 1, 0, -1 };
	static int[] dirX = { -1, 0, 1, 0 };

	static double[] percent = { 0.05, 0.1, 0.1, 0.02, 0.07, 0.07, 0.02, 0.01, 0.01 };
	static Pair[] pairW = { new Pair(0, -2), new Pair(-1, -1), new Pair(1, -1), new Pair(-2, 0), new Pair(-1, 0),
			new Pair(1, 0), new Pair(2, 0), new Pair(-1, 1), new Pair(1, 1) };
	static Pair[] pairS = { new Pair(2, 0), new Pair(1, -1), new Pair(1, 1), new Pair(0, -2), new Pair(0, -1),
			new Pair(0, 1), new Pair(0, 2), new Pair(-1, -1), new Pair(-1, 1) };
	static Pair[] pairE = { new Pair(0, 2), new Pair(-1, 1), new Pair(1, 1), new Pair(-2, 0), new Pair(-1, 0),
			new Pair(1, 0), new Pair(2, 0), new Pair(-1, -1), new Pair(1, -1) };
	static Pair[] pairN = { new Pair(-2, 0), new Pair(-1, -1), new Pair(-1, 1), new Pair(0, -2), new Pair(0, -1),
			new Pair(0, 1), new Pair(0, 2), new Pair(1, -1), new Pair(1, 1) };
	static Tor tor;

	// 서남동북 으로 토네이도가 이동할 때, 날리는 모래를 각각 구현
	// 1 1 2 2 3 3 4 4 으로 토네이도가 같은 방향으로 이동
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = br.readLine();
		N = Integer.parseInt(s);
		board = new int[N][N];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
			}
		}

		tor = new Tor(N / 2, N / 2, 0);

		int loop = 1;
		int cnt = 0;
		while (true) {

			if (loop > N)
				break;

			for (int i = 0; i < loop; i++) {
				torMove();
				if (tor.y < 0 || tor.x < 0 || tor.y >= N || tor.x >= N)
					break;
				sandMove(tor.y, tor.x, tor.d, board[tor.y][tor.x]);
			}

			tor.d = (tor.d + 1) % 4;

			++cnt;
			if (cnt == 2) {
				cnt = 0;
				++loop;
			}

		}

		System.out.println(outSand);

	}

	static void torMove() {
		tor.y = tor.y + dirY[tor.d];
		tor.x = tor.x + dirX[tor.d];
	}

	static void sandMove(int y, int x, int dir, int sand) {
		// 방향에 따라 구현을 달리한다.
		int yy = 0, xx = 0, ss; // ss = sand
		int alpha = sand;
		for (int i = 0; i < 9; i++) {
			if (dir == 0) {
				yy = y + pairW[i].y;
				xx = x + pairW[i].x;
			} else if (dir == 1) { // 남
				yy = y + pairS[i].y;
				xx = x + pairS[i].x;
			} else if (dir == 2) { // 동
				yy = y + pairE[i].y;
				xx = x + pairE[i].x;
			} else if (dir == 3) { // 북
				yy = y + pairN[i].y;
				xx = x + pairN[i].x;
			}
			ss = (int) (sand * percent[i]);
			alpha -= ss;
			if (yy < 0 || xx < 0 || yy >= N || xx >= N) {
				outSand += ss;
				continue;
			}
			board[yy][xx] += ss;

		}
		yy = y + dirY[dir];
		xx = x + dirX[dir];
		if (yy < 0 || xx < 0 || yy >= N || xx >= N) {
			outSand += alpha;

		} else {
			board[yy][xx] += alpha;
		}

	}

}
