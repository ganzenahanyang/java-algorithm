package boj.문자열반복_2675;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;
		StringBuilder sb;

		int count = Integer.parseInt(br.readLine());

		for (int i = 0; i < count; i++) {
			sb = new StringBuilder();
			st = new StringTokenizer(br.readLine());

			int recur = Integer.parseInt(st.nextToken());
			String str = st.nextToken();

			for (int j = 0; j < str.length(); j++) {
				for (int k = 0; k < recur; k++) {
					sb.append(str.charAt(j));
				}
			}

			System.out.println(sb);

		}
	}
}
