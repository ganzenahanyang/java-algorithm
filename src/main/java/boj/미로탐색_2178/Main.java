package boj.미로탐색_2178;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main {

	static int[][] arr;
	static int[][] weight;
	static boolean[][] visited;
	static int[] di = { 0, 0, 1, -1 };
	static int[] dj = { 1, -1, 0, 0 };
	static int N, M;

	static class Pair {
		int i, j;

		public Pair(int i, int j) {
			super();
			this.i = i;
			this.j = j;
		}

	}

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st;

		st = new StringTokenizer(br.readLine(), " ");
		N = Integer.parseInt(st.nextToken());
		M = Integer.parseInt(st.nextToken());

		arr = new int[N][M];
		weight = new int[N][M];
		visited = new boolean[N][M];

		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split("");
			for (int j = 0; j < M; j++) {
				arr[i][j] = Integer.parseInt(line[j]);
			}
		}

		Queue<Pair> q = new LinkedList<>();
		q.add(new Pair(0, 0));
		visited[0][0] = true;
		weight[0][0] = 1;

		while (!q.isEmpty()) {
			Pair now = q.poll();

			for (int k = 0; k < 4; k++) {
				int ii = now.i + di[k];
				int jj = now.j + dj[k];

				if (ii < 0 || ii >= N || jj < 0 || jj >= M 
						|| visited[ii][jj] || arr[ii][jj] != 1)
					continue;

				visited[ii][jj] = true;
				weight[ii][jj] = weight[now.i][now.j] + 1;
				q.add(new Pair(ii, jj));
			}

		}

		System.out.println(weight[N - 1][M - 1]);
	}
}
