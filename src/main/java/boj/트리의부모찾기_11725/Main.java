package boj.트리의부모찾기_11725;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

	static class Node {
		int index;
		int root;
		ArrayList<Node> adj;

		public Node(int index) {
			super();
			this.index = index;
			this.root = 0;
			this.adj = new ArrayList<>();
		}

	}

	static int N;

	public static void main(String[] args) throws NumberFormatException, IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine());

		Node[] nodes = new Node[N + 1];
		for (int i = 1; i <= N; i++) {
			nodes[i] = new Node(i);
		}

		for (int i = 0; i < N - 1; i++) {
			String[] line = br.readLine().split(" ");
			int a = Integer.parseInt(line[0]);
			int b = Integer.parseInt(line[1]);
			nodes[a].adj.add(nodes[b]);
			nodes[b].adj.add(nodes[a]);
		}

		boolean[] visit = new boolean[N + 1];

		Queue<Node> q = new LinkedList<>();
		nodes[1].root = -1;
		visit[1] = true;
		q.add(nodes[1]);
		
		while (!q.isEmpty()) {
			Node now = q.poll();
			
			for (Node n : now.adj) {
				if (!visit[n.index]) {
					visit[n.index] = true;
					n.root = now.index;
					q.add(n);
				}
			}
		}
		
		for(int i = 2 ; i < nodes.length ; i++) {
			System.out.println(nodes[i].root);
		}
	}

}
