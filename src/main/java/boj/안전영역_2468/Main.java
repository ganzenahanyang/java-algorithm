package boj.안전영역_2468;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
	static int N;
	static int[][] board;
	static boolean[][] visit;
	static int[] dirY = { 0, 0, 1, -1 };
	static int[] dirX = { 1, -1, 0, 0 };
	static Queue<Loc> q = new LinkedList<>();
	static int maxSafe = 0;
	static int maxH = 0;
	static int nowH = 0;

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine().split(" ")[0]);
		board = new int[N][N];
		visit = new boolean[N][N];
		for (int i = 0; i < N; i++) {
			String[] line = br.readLine().split(" ");
			for (int j = 0; j < N; j++) {
				board[i][j] = Integer.parseInt(line[j]);
				maxH = Math.max(maxH, board[i][j]);
			}
		}

		while (maxH >= 0) { // 가장 많이 오는 장마부터 안 올때까지
			int cnt = 0;
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					if (board[i][j] > maxH && !visit[i][j]) {
						q.add(new Loc(i, j));
						BFS();
						cnt++;
					}
				}
			}

			maxSafe = Math.max(maxSafe, cnt);
			visit = new boolean[N][N];
			q.clear();
			maxH--;
		}

		System.out.println(maxSafe);
		br.close();

	}

	static void BFS() {

		while (!q.isEmpty()) {
			Loc now = q.poll();
			int y = now.y;
			int x = now.x;
			visit[y][x] = true;
			for (int i = 0; i < 4; i++) {
				int yy = y + dirY[i];
				int xx = x + dirX[i];
				if (yy < 0 || xx < 0 || yy >= N || xx >= N || 
						visit[yy][xx] || board[yy][xx] <= maxH)
					continue;

				q.add(new Loc(yy, xx));
				visit[yy][xx] = true;

			}
		}

	}
}

class Loc {
	int y, x;

	public Loc(int y, int x) {
		super();
		this.y = y;
		this.x = x;
	}

}
