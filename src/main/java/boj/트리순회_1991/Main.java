package boj.트리순회_1991;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	static class Node {
		char name;
		Node left;
		Node right;

		public Node(char name, Node left, Node right) {
			super();
			this.name = name;
			this.left = left;
			this.right = right;
		}

		public Node(char name) {
			super();
			this.name = name;
		}

	}

	static class Tree {
		Node root;

		public void searchNode(Node node, char name, char left, char right) {
			if (node == null)
				return;
			else if (node.name == name) {
				node.left = left == '.' ? null : new Node(left);
				node.right = right == '.' ? null : new Node(right);
			} else {
				searchNode(node.left, name, left, right);
				searchNode(node.right, name, left, right);
			}
		}

		public void insertNode(char name, char left, char right) {
			if (root == null) {
				root = new Node(name, left == '.' ? null : new Node(left), right == '.' ? null : new Node(right));
			} else {
				searchNode(root, name, left, right);
			}
		}

		public void preOrder(Node node) {
			if (node != null) {
				System.out.print(node.name);
				if (node.left != null)
					preOrder(node.left);
				if (node.right != null)
					preOrder(node.right);
			}
		}

		public void inOrder(Node node) {
			if (node != null) {
				if (node.left != null)
					inOrder(node.left);
				System.out.print(node.name);
				if (node.right != null)
					inOrder(node.right);
			}
		}

		public void postOrder(Node node) {
			if (node != null) {
				if (node.left != null)
					postOrder(node.left);
				if (node.right != null)
					postOrder(node.right);
				System.out.print(node.name);
			}
		}
	}

	static int N;
	static Tree t = new Tree();

	public static void main(String[] args) throws Exception, IOException {
		// TODO Auto-generated method stub
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		N = Integer.parseInt(br.readLine().split(" ")[0]);

		for (int i = 0; i < N; i++) {
			String line[] = br.readLine().split(" ");
			char name = line[0].charAt(0);
			char left = line[1].charAt(0);
			char right = line[2].charAt(0);
			t.insertNode(name, left, right);
		}
		t.preOrder(t.root);
		System.out.println();
		t.inOrder(t.root);
		System.out.println();
		t.postOrder(t.root);

	}

}
