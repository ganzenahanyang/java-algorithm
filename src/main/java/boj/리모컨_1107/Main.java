package boj.리모컨_1107;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

	static boolean[] button;
	static String[] error;
	static int N, M;

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		N = Integer.parseInt(br.readLine());
		M = Integer.parseInt(br.readLine());
		button = new boolean[10];
		error = new String[M];
		Arrays.fill(button, true);
		if (M != 0) {
			String[] line = br.readLine().split(" ");
			for (int i = 0; i < M; i++) {
				button[Integer.parseInt(line[i])] = false;
				error[i] = line[i];
			}
		}

		int onlyPlusMinus = Math.abs(100 - N);
		int cnt = Integer.MAX_VALUE;
		for (int i = 0; i < 1000000; i++) {
			int copy = i;
			int len = 0;
			boolean pass = true;

			if (i != 0) {
				while (copy > 0) {
					if (!button[copy % 10]) {
						pass = false;
						break;
					}
					len++;
					copy /= 10;
				}
			} else {
				if (!button[i]) {
					pass = false;
				} else {
					len = 1;
				}
			}

			if (pass && len > 0) {
				cnt = Math.min(cnt, len + Math.abs(N - i));
			} else {
				continue;
			}
		}

		System.out.println(Math.min(onlyPlusMinus, cnt));

	}

}