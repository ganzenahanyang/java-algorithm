package boj.로봇시뮬레이션_2174;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Main {
	static int A, B, N, M;
	static Robot[][] board;

	static int[] dirY = { -1, 0, 1, 0 };
	static int[] dirX = { 0, 1, 0, -1 };
	static List<Robot> robots = new ArrayList<>();
	static Queue<Robot> q = new LinkedList<>();
	static List<Command> commands = new ArrayList<>();

	public static void main(String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String[] ab = br.readLine().split(" ");
		A = Integer.parseInt(ab[0]);
		B = Integer.parseInt(ab[1]);

		String[] nm = br.readLine().split(" ");
		N = Integer.parseInt(nm[0]); // 세로
		M = Integer.parseInt(nm[1]); // 가로

		board = new Robot[B][A];

		for (int i = 0; i < N; i++) {
			String[] yxd = br.readLine().split(" ");
			int y = B - Integer.parseInt(yxd[1]); // 일반적인 2차원 배열의 좌표로 고치기
			int x = Integer.parseInt(yxd[0]) - 1; // 일반적인 2차원 배열의 좌표로 고치기
			String d = yxd[2];
			robots.add(new Robot(i + 1, y, x, d));
			board[y][x] = robots.get(i); // 보드에 로봇 설정
			
		}

		for (int i = 0; i < M; i++) {
			String[] cmd = br.readLine().split(" ");
			int index = Integer.parseInt(cmd[0]) - 1;
			String dir = cmd[1];
			int loop = Integer.parseInt(cmd[2]);
			commands.add(new Command(index, dir, loop));
		}

		for (int i = 0; i < commands.size(); i++) {
			Command now = commands.get(i);
			Robot robot = robots.get(now.index);

			for (int j = 0; j < now.loop; j++) {
				int y = robot.y;
				int x = robot.x;
				String d = robot.d;

				int yy, xx;
				String dd;
				if (now.cmd.equals("F")) {
					yy = robot.y + dirY[dirToInt(d)];
					xx = robot.x + dirX[dirToInt(d)];
					dd = d;
					if (yy < 0 || xx < 0 || yy >= B || xx >= A) {
						System.out.printf("Robot %d crashes into the wall\n", robot.n);
						return;
					} else if (board[yy][xx] != null) {
						System.out.printf("Robot %d crashes into robot %d\n", robot.n, board[yy][xx].n);
						return;
					}
					robot.y = yy;
					robot.x = xx;
					robot.d = dd;
					board[yy][xx] = robot;
					board[y][x] = null;
				} else if (now.cmd.equals("L")) {
					yy = robot.y;
					xx = robot.x;
					dd = turnLeft(d);

					robot.y = yy;
					robot.x = xx;
					robot.d = dd;
					board[yy][xx] = robot;
				} else { // R
					yy = robot.y;
					xx = robot.x;
					dd = turnRight(d);

					robot.y = yy;
					robot.x = xx;
					robot.d = dd;
					board[yy][xx] = robot;
				}
				
			}
		}
		System.out.println("OK");
		br.close();

	}

	static int dirToInt(String dir) {
		if (dir.equals("N")) {
			return 0;
		} else if (dir.equals("E")) {
			return 1;
		} else if (dir.equals("S")) {
			return 2;
		} else {
			return 3;
		}
	}

	static String turnLeft(String dir) {
		if (dir.equals("N")) {
			return "W";
		} else if (dir.equals("E")) {
			return "N";
		} else if (dir.equals("S")) {
			return "E";
		} else {
			return "S";
		}
	}

	static String turnRight(String dir) {
		if (dir.equals("N")) {
			return "E";
		} else if (dir.equals("E")) {
			return "S";
		} else if (dir.equals("S")) {
			return "W";
		} else {
			return "N";
		}
	}
}

class Command {
	int index;
	String cmd;
	int loop;

	public Command(int index, String cmd, int loop) {
		super();
		this.index = index;
		this.cmd = cmd;
		this.loop = loop;
	}

}

class Robot {
	int n, y, x;
	String d;

	public Robot(int n, int y, int x, String d) {
		super();
		this.n = n;
		this.y = y;
		this.x = x;
		this.d = d;
	}

}
