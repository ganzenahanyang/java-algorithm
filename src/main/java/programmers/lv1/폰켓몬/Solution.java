package programmers.lv1.폰켓몬;

import java.util.HashMap;

public class Solution {
	public int solution(int[] nums) {
		HashMap<Integer, Integer> hm = new HashMap<>();
		for (int i = 0; i < nums.length ; i++) {
			hm.put(nums[i],1);
		}
		
		return hm.size() > nums.length / 2 ? nums.length / 2 : hm.size();
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		int[] input = { 3, 1, 2, 3 };
		System.out.println(s.solution(input));
	}
}
