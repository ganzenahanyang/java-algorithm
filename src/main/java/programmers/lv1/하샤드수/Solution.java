package programmers.lv1.하샤드수;

public class Solution {
	public boolean solution(int x) {
		boolean answer = true;
		int origin = x;
		int sum = 0;
		while (x > 0) {
			sum += x % 10;
			x /= 10;
		}
		if (origin % sum != 0)
			answer = false;
		return answer;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
