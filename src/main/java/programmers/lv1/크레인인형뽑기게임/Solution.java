package programmers.lv1.크레인인형뽑기게임;

import java.util.Stack;

class Solution {
    public int solution(int[][] board, int[] moves) {
        int answer = 0;
        Stack<Integer> bucket = new Stack<>();
        for (int i = 0; i < moves.length; i++) {
            int x = moves[i] - 1;
            int doll = 0;
            for (int y = 0; y < board[x].length; y++) {
                if (board[y][x] != 0) {
                    doll = board[y][x];
                    board[y][x] = 0;
                    break;
                }
            }
            // 인형을 못 집은 경우는 제외
            if (doll == 0)
                continue;

            if (bucket.empty()) {
                bucket.push(doll);
            } else {
                if (bucket.peek() == doll) {
                    bucket.pop();
                    answer += 2; // 2개가 만나 사라졌으므로 +=2
                } else {
                    bucket.push(doll);
                }
            }
        }
        return answer;
    }
}
