package programmers.lv1.모의고사;

import java.util.ArrayList;
import java.util.Collections;

public class Solution {

	static class Student implements Comparable<Student> {
		int no;
		int cnt;

		public Student(int no, int cnt) {
			super();
			this.no = no;
			this.cnt = cnt;
		}

		@Override
		public int compareTo(Student o) {
			if (this.cnt == o.cnt)
				return this.no - o.no;
			return o.cnt - this.cnt;
		}

	}

	static ArrayList<Student> arr = new ArrayList<>();

	public ArrayList<Integer> solution(int[] answers) {
		for (int i = 0; i < 3; i++) {
			arr.add(new Student(i + 1, 0));
		}
		int[] people1 = { 1, 2, 3, 4, 5 };
		int[] people2 = { 2, 1, 2, 3, 2, 4, 2, 5 };
		int[] people3 = { 3, 3, 1, 1, 2, 2, 4, 4, 5, 5 };
		for (int i = 0; i < answers.length; i++) {
			int answer = answers[i];
			if (people1[i % people1.length] == answer) {
				arr.get(0).cnt++;
			}
			if (people2[i % people2.length] == answer) {
				arr.get(1).cnt++;
			}
			if (people3[i % people3.length] == answer) {
				arr.get(2).cnt++;
			}
		}

		Collections.sort(arr);
		int max = arr.get(0).cnt;
		ArrayList<Integer> answer = new ArrayList<Integer>();
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i).cnt == max) {
				answer.add(arr.get(i).no);
			}
		}
		return answer;
	}
	
	public int[] solution1(int[] answers) {
		for (int i = 0; i < 3; i++) {
			arr.add(new Student(i + 1, 0));
		}
		int[] people1 = { 1, 2, 3, 4, 5 };
		int[] people2 = { 2, 1, 2, 3, 2, 4, 2, 5 };
		int[] people3 = { 3, 3, 1, 1, 2, 2, 4, 4, 5, 5 };
		for (int i = 0; i < answers.length; i++) {
			int answer = answers[i];
			if (people1[i % people1.length] == answer) {
				arr.get(0).cnt++;
			}
			if (people2[i % people2.length] == answer) {
				arr.get(1).cnt++;
			}
			if (people3[i % people3.length] == answer) {
				arr.get(2).cnt++;
			}
		}

		Collections.sort(arr);
		int max = arr.get(0).cnt;
		ArrayList<Integer> answer = new ArrayList<Integer>();
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i).cnt == max) {
				answer.add(arr.get(i).no);
			}
		}
		return answer.stream().mapToInt(v -> v.intValue()).toArray();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
