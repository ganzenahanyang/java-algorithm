package programmers.lv1.콜라츠추측;

public class Solution {

	public int solution(int num) {
		int answer = 0;
		long n = num;
		while (n != 1) {
			if (n % 2 == 0) {
				n /= 2;
			} else {
				n *= 3;
				++n;
			}
			++answer;
		}
		if (answer > 500)
			answer = -1;
		return answer;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
