package programmers.lv1.연습문제_문자열내림차순으로배치하기;

import java.util.Arrays;
import java.util.Collections;

public class Solution {
	
    public String solution(String s) {
        String answer = "";
        String[] splited = s.split("");
        Arrays.sort(splited, Collections.reverseOrder());
        for(int i = 0 ; i < splited.length ; i++)
        	answer += splited[i];
        return answer;
    }

	public static void main(String[] args) {
		Solution s = new Solution();
		String input = "Zbcdefg";
		System.out.println(s.solution(input));
	}

}
