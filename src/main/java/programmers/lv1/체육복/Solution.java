package programmers.lv1.체육복;

public class Solution {
	static int answer = 0;

	public int solution(int n, int[] lost, int[] reserve) {
		int ans = n;
		int[] extra = new int[n + 1];
		for (int i = 0; i < lost.length; i++) {
			extra[lost[i]]--;
		}
		for (int i = 0; i < reserve.length; i++) {
			extra[reserve[i]]++;
		}

		// recurs(0, lost.length, lost, extra);

		for (int i = 1; i < extra.length; i++) {
			if (extra[i] == -1) {
				int prev = i - 1;
				int next = i + 1;

				if (prev >= 1 && extra[prev] == 1) {
					extra[i]++;
					extra[prev]--;
				} else if (next < extra.length && extra[next] == 1) {
					extra[i]++;
					extra[next]--;
				} else {
					ans--;
				}
			}
		}

		return ans;
	}

	static void recurs(int depth, int end, int[] lost, int[] extra) {
		if (depth == end) {
			int sum = 0;
			for (int i = 1; i < extra.length; i++) {
				if (extra[i] != -1)
					sum++;
			}
			answer = Math.max(answer, sum);
			return;
		}

		int now = lost[depth];
		int prev = now - 1;
		int next = now + 1;
		if (extra[now] == -1) {
			if (prev >= 1 && extra[prev] == 1) {
				extra[now]++;
				extra[prev]--;
				recurs(depth + 1, end, lost, extra);
				extra[prev]++;
				extra[now]--;
			}

			if (next < extra.length && extra[next] == 1) {
				extra[now]++;
				extra[next]--;
				recurs(depth + 1, end, lost, extra);
				extra[next]++;
				extra[now]--;
			}
		}

		recurs(depth + 1, end, lost, extra);

	}

	public static void main(String[] args) {
		Solution s = new Solution();
		int n = 5;
		int[] lost = { 2, 4 };
		int[] reserve = { 3 };
		s.solution(n, lost, reserve);
	}

}
