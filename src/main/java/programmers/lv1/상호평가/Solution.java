package programmers.lv1.상호평가;

public class Solution {

	public String solution(int[][] scores) {

		StringBuilder sb = new StringBuilder();
		for (int j = 0; j < scores.length; j++) {

			int sum = 0;
			int max = Integer.MIN_VALUE;
			int min = Integer.MAX_VALUE;
			int selfScore = scores[j][j];
			int cnt = 0;
			for (int i = 0; i < scores.length; i++) {
				if (selfScore == scores[i][j])
					cnt++;

				sum += scores[i][j];
				max = Math.max(scores[i][j], max);
				min = Math.min(scores[i][j], min);
			}

			int selfMinus = 0;
			if (cnt == 1) {
				if (selfScore == min || selfScore == max) {
					sum -= selfScore;
					selfMinus = 1;
				}
			}

			double avg = (double) sum / (scores.length - selfMinus);
			if (avg >= 90) {
				sb.append("A");
			} else if (avg >= 80) {
				sb.append("B");
			} else if (avg >= 70) {
				sb.append("C");
			} else if (avg >= 50) {
				sb.append("D");
			} else {
				sb.append("F");
			}
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		Solution s = new Solution();
//		int[][] scores = { { 100, 90, 98, 88, 65 }, { 50, 45, 99, 85, 77 }, { 47, 88, 95, 80, 67 },
//				{ 61, 57, 100, 80, 65 }, { 24, 90, 94, 75, 65 } };
		int[][] scores = { { 50, 90 }, { 50, 87 } };
		s.solution(scores);

	}

}
