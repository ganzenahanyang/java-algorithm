package programmers.lv1.숫자문자열과영단어;

public class Solution {

	public int solution(String s) {
		String[] arr = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine" };
		for (int i = 0; i < arr.length; i++) {
			s = s.replace(arr[i], String.valueOf(i));
		}
		return Integer.parseInt(s);
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		String str = "oneone4seveneight";
		s.solution(str);

	}

}
