package programmers.lv1.제일작은수제거하기;

public class Solution {

	public int[] solution(int[] arr) {
		int[] answer;
		if (arr.length == 1) {
			answer = new int[1];
			answer[0] = -1;
			return answer;
		}
		answer = new int[arr.length - 1];
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < arr.length; i++) {
			min = Math.min(arr[i], min);
		}
		int j = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] != min) {
				answer[j++] = arr[i];
			}
		}
		return answer;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
