package programmers.lv1.연습문제_문자열내마음대로정렬하기;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Solution {

	static class Str implements Comparable<Str> {
		String value;
		int index;

		public Str(String value, int index) {
			super();
			this.value = value;
			this.index = index;
		}

		@Override
		public int compareTo(Str o) {
			if (this.value.charAt(index) < o.value.charAt(index))
				return -1;
			else if (this.value.charAt(index) > o.value.charAt(index)) {
				return 1;
			} else {
				List<String> list = new ArrayList<>();
				list.add(this.value);
				list.add(o.value);
				Collections.sort(list);
				if (list.get(0) == this.value) {
					return -1;
				} else {
					return 1;
				}
			}

		}

	}

	public String[] solution(String[] strings, int n) {
		String[] answer = {};
		ArrayList<Str> arr = new ArrayList<>();
		for (int i = 0; i < strings.length; i++) {
			arr.add(new Str(strings[i], n));
		}
		Collections.sort(arr);
		answer = new String[arr.size()];
		for (int i = 0; i < answer.length; i++) {
			answer[i] = arr.get(i).value;
		}

		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
//		String[] arr = { "sun", "bed", "car" };
//		int n = 1;
		String[] arr = { "abce", "abcd", "cdx"};
		int n = 2;
		String[] answer = s.solution(arr, n);
		for (String str : answer) {
			System.out.println(str);
		}
	}

}
