package programmers.lv1.연습문제_문자열다루기기본;

import java.util.regex.Pattern;

public class Solution {
	
    public boolean solution(String s) {
        boolean answer = true;
        String pattern = "\\d{4}|\\d{6}";
        answer = Pattern.matches(pattern, s);
        return answer;
    }

	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.solution("1234"));

	}

}
