package programmers.lv1.같은숫자는싫어;

import java.util.ArrayList;

public class Solution {
	public ArrayList<Integer> solution(int[] arr) {
		ArrayList<Integer> answer = new ArrayList<>();
		for (int i = 0; i < arr.length; i++) {
			if (answer.isEmpty()) {
				answer.add(arr[i]);
			} else {
				if (answer.get(answer.size() - 1) != arr[i]) {
					answer.add(arr[i]);
				}
			}
		}
		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		int[] arr = { 1, 1, 3, 3, 0, 1, 1 };
		s.solution(arr);
	}

}
