package programmers.lv1.복서정렬하기;

import java.util.ArrayList;
import java.util.Collections;

public class Solution {

	static class Boxer implements Comparable<Boxer> {
		int no;
		int weight;
		int overWin;
		double rate; // 승률

		public Boxer(int no, int weight) {
			super();
			this.no = no;
			this.weight = weight;
		}

		public int compareTo(Boxer o) {
			if (o.rate == this.rate) {
				if (o.overWin == this.overWin) {
					if (o.weight == this.weight) {
						return this.no - o.no;
					} else {
						return o.weight - this.weight;
					}
				} else {
					return o.overWin - this.overWin;
				}
			} else if (o.rate < this.rate) {
				return -1;
			} else {
				return 1;
			}
		}

	}

	static ArrayList<Boxer> arr = new ArrayList<>();

	public int[] solution(int[] weights, String[] head2head) {
		int[] answer = new int[weights.length];
		for (int i = 0; i < weights.length; i++) {
			arr.add(new Boxer(i + 1, weights[i]));
		}

		for (int i = 0; i < weights.length; i++) {
			int win = 0;
			int overWin = 0;
			int nCount = 0;
			String[] history = head2head[i].split("");
			for (int j = 0; j < history.length; j++) {
				if (history[j].equals("W")) {
					++win;
					if (arr.get(i).weight < arr.get(j).weight) {
						++overWin;
					}
				} else if (history[j].equals("N")) {
					nCount++;
				}
			}
			arr.get(i).rate = history.length == nCount ? 0 : (double) win / (history.length - nCount);
			arr.get(i).overWin = overWin;

		}

		Collections.sort(arr);
		for (int i = 0; i < answer.length; i++) {
			answer[i] = arr.get(i).no;
		}
		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		int[] weights = { 50, 82, 75, 120 };
		String[] head2head = { "NLWL", "WNLL", "LWNW", "WWLN" };
		s.solution(weights, head2head);
	}

}
