package programmers.lv1.다트게임;

import java.util.ArrayList;

public class Solution {
	public int solution(String dartResult) {

		int answer = 0;
		ArrayList<Integer> history = new ArrayList<>();
		for (int i = 0; i < dartResult.length(); i++) {
			int score;
			boolean minus = false;
			boolean multiple = false;

			score = Character.getNumericValue(dartResult.charAt(i));
			if (score == 1 && (i < dartResult.length() - 2) && Character.getNumericValue(dartResult.charAt(i + 1)) == 0) {
				score = 10;
				++i;
			}
			++i;

			if (dartResult.charAt(i) == 'S') {
				score = (int) Math.pow(score, 1);
			} else if (dartResult.charAt(i) == 'D') {
				score = (int) Math.pow(score, 2);
			} else if (dartResult.charAt(i) == 'T') {
				score = (int) Math.pow(score, 3);
			}

			if(i == dartResult.length() - 1) {
				history.add(score);
				continue;
			}else
				++i;

			if (dartResult.charAt(i) == '*') {
				
				multiple = true;

			} else if (dartResult.charAt(i) == '#') {
				minus = true;

			} else {
				--i;
			}

			if (minus) {
				score *= -1;
			} else if (multiple) {
				score *= 2;
				if(!history.isEmpty())
					history.set(history.size() - 1, history.get(history.size() - 1) * 2);
			}

			history.add(score);
			

		}

		answer = history.stream().mapToInt(Integer::intValue).sum();

		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		String input = "1D2S#10S";
		String[] inputs = {"1S2D*3T", "1D2S#10S", "1D2S0T", "1S*2T*3S"};
		System.out.println(s.solution(input));
	}
}
