package programmers.lv1.로또의최고순위와최적순위;

public class Solution {

	public int[] solution(int[] lottos, int[] win_nums) {
		int[] answer = new int[2];
		boolean[] match = new boolean[46];
		int[] score = { 6, 6, 5, 4, 3, 2, 1 };
		int zeroCnt = 0;
		int matchCnt = 0;
		// 당첨번호 마킹
		for (int i = 0; i < win_nums.length; i++) {
			match[win_nums[i]] = true;
		}
		// 0갯수 세기, 일치 숫자 세기
		for (int i = 0; i < lottos.length; i++) {
			if (lottos[i] == 0)
				zeroCnt++;
			else {
				if (match[lottos[i]]) {
					matchCnt++;
				}
			}
		}

		int min = score[matchCnt];
		int max = score[(matchCnt + zeroCnt)];
		answer[0] = max;
		answer[1] = min;

		return answer;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
