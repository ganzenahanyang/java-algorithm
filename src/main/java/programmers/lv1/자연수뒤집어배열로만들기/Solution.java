package programmers.lv1.자연수뒤집어배열로만들기;

import java.util.Arrays;

public class Solution {

	public int[] solution(long n) {

		String s = String.valueOf(n);

		int[] answer = new int[s.length()];
		for (int i = 0; i < s.length(); i++) {
			answer[i] = (int) s.charAt(s.length() - 1 - i);
		}
		return answer;
	}

	public int[] solution1(long n) {

		String s = String.valueOf(n);
		StringBuilder sb = new StringBuilder(s);
		sb.reverse();
		String[] line = sb.toString().split("");

		int[] answer = new int[s.length()];
		for (int i = 0; i < line.length; i++) {
			answer[i] = Integer.parseInt(line[i]);
		}
		Arrays.stream(answer).forEach(v -> System.out.print(v + " "));
		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		long n = 12345;
		s.solution1(n);

	}

}
