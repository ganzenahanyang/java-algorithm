package programmers.lv1.최소직사각형;

public class Solution {

	public int solution(int[][] sizes) {
		int garo = 0, sero = 0;
		for (int i = 0; i < sizes.length; i++) {
			// 큰 값을 무조건 가로로
			int tGaro, tSero;
			if (sizes[i][0] > sizes[i][1]) {
				tGaro = sizes[i][0];
				tSero = sizes[i][1];
			} else {
				tGaro = sizes[i][1];
				tSero = sizes[i][0];
			}

			garo = Math.max(tGaro, garo);
			sero = Math.max(sero, tSero);

		}
		return garo * sero;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
