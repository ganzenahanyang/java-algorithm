package programmers.lv1.키패드누르기;

public class Solution {

	static class Pair {
		int y, x;

		public Pair(int y, int x) {
			super();
			this.y = y;
			this.x = x;
		}

	}
	
	public static int getDist(Pair a, Pair b) {
		return Math.abs(a.y - b.y) + Math.abs(a.x - b.x);
	}
	
	public static void move(Pair a, Pair b, boolean left) {
		a.x = b.x;
		a.y = b.y;
		if(left)
			answer += "L";
		else
			answer += "R";
	}
	static String answer ="";
	public String solution(int[] numbers, String hand) {

		Pair left = new Pair(3, 0);
		Pair right = new Pair(3, 2);
		Pair[] keypads = { new Pair(3, 1), new Pair(0, 0), new Pair(0, 1), new Pair(0, 2), new Pair(1, 0),
				new Pair(1, 1), new Pair(1, 2), new Pair(2, 0), new Pair(2, 1), new Pair(2, 2), new Pair(3, 2) };

		for (int i = 0; i < numbers.length; i++) {
			int n = numbers[i];
			if(n == 1 || n == 4 || n == 7) {
				move(left, keypads[n], true);
			}else if(n == 3 || n == 6 || n == 9) {
				move(right, keypads[n], false);
			}else {
				if(getDist(left, keypads[n]) < getDist(right, keypads[n])){
					move(left, keypads[n], true);
				}else if(getDist(left, keypads[n]) > getDist(right, keypads[n])){
					move(right, keypads[n], false);
				}else {
					if(hand.equals("left")) {
						move(left, keypads[n], true);
					}else {
						move(right, keypads[n], false);
					}
				}
			}
		}
		return answer;
	}
}
