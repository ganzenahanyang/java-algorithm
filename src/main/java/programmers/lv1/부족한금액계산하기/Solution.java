package programmers.lv1.부족한금액계산하기;

public class Solution {

	public long solution(int price, int money, int count) {
		long answer = -1;
		long total = 0;
		for (int i = 1; i <= count; i++) {
			total += i * price;
		}
		if (money >= total)
			answer = 0;
		else
			answer = total - money;
		return answer;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
