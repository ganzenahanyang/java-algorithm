package programmers.lv1.연습문제_이상한문자만들기;

public class Solution {
	public String solution(String s) {
		String answer = "";
		boolean upper = true;
		for (int i = 0; i < s.length(); i++) {
			String c = Character.toString(s.charAt(i));
			if (c.equals(" ")) {
				upper = true;
				answer += " ";
				continue;
			}
			if (upper) {
				answer += c.toUpperCase();
				upper = false;
			} else {
				answer += c.toLowerCase();
				upper = true;
			}
		}
		return answer;
	}

}
