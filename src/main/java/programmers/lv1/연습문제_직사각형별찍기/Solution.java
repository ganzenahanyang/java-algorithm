package programmers.lv1.연습문제_직사각형별찍기;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String[] ab = br.readLine().split(" ");
        int a = Integer.parseInt(ab[0]);
        int b = Integer.parseInt(ab[1]);
        for(int i = 0 ; i < b ; i++) {
        	for(int j = 0 ; j < a ; j++) {
        		System.out.print("*");
        	}System.out.println();
        }
        br.close();

    }
}
