package programmers.lv1.실패율;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Solution {

    static class Test implements Comparable<Test> {
        int no;
        double fail;

        public Test(int no, double fail) {
            super();
            this.no = no;
            this.fail = fail;
        }

        @Override
        public int compareTo(Test o) {
            if (this.fail > o.fail)
                return -1;
            else if (this.fail < o.fail)
                return 1;
            else {
                if (this.no < o.no)
                    return -1;
                else if (this.no == o.no)
                    return 0;
                else
                    return 1;
            }
        }
    }

    public int[] solution(int N, int[] stages) {
        int[] answer = new int[N];
        int playerNum = stages.length;
        int[] stageStop = new int[N + 1];
        double[] fail = new double[N+1];
        ArrayList<Test> list = new ArrayList<>();
        for(int i = 1 ; i < N+1 ; i++) {
            int stopPlayer = 0;
            for(int j = 0 ; j < stages.length ; j++) {
                if(i == stages[j]) {
                    stopPlayer++;
                }
            }
            list.add(new Test(i, (double) stopPlayer / (double) playerNum));
            playerNum -= stopPlayer;
        }
        Collections.sort(list);
        for(int i = 0 ; i < list.size();i ++) {
            answer[i] = list.get(i).no;
        }

        return answer;
    }


	public static void main(String[] args) {
		Solution s = new Solution();
		int[] stages = { 2, 1, 2, 6, 2, 4, 3, 3 };
		Arrays.stream(s.solution(5, stages)).boxed().forEach(val -> System.out.print(val + " "));
	}
}
