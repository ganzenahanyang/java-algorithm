package programmers.lv1.시저암호;

public class Solution {

	public String solution(String s, int n) {
		String answer = "";
		char[] arr = s.toCharArray();
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == ' ')
				continue;
			if (Character.isLowerCase(arr[i])) {
				if (arr[i] + n > 'z') {
					arr[i] = (char) ('a' + arr[i] + n - 'z' - 1);
				} else {
					arr[i] += n;
				}
			} else if (Character.isUpperCase(arr[i])) {
				if (arr[i] + n > 'Z') {
					arr[i] = (char) ('A' + arr[i] + n - 'Z' - 1);
				} else {
					arr[i] += n;
				}
			}
		}
		answer = String.valueOf(arr);
		return answer;
	}

	public String solution1(String s, int _n) {
		return s.chars().map(c -> {
			int n = _n % 26;
			if (c >= 'a' && c <= 'z') {
				return 'a' + (c - 'a' + n) % 26;
			} else if (c >= 'A' && c <= 'Z') {
				return 'A' + (c - 'A' + n) % 26;
			} else {
				return c;
			}
		}).mapToObj(c -> String.valueOf((char) c)).reduce((a, b) -> a + b).orElse("");
	}

	public static void main(String[] args) {
		Solution sol = new Solution();
		String s = "a B z";
		int n = 4;
		sol.solution(s, n);

	}

}
