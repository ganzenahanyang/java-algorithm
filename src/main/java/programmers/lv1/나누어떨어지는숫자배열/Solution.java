package programmers.lv1.나누어떨어지는숫자배열;

import java.util.ArrayList;
import java.util.Collections;

public class Solution {

	public ArrayList<Integer> solution(int[] arr, int divisor) {
		ArrayList<Integer> answer = new ArrayList<>();
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] % divisor == 0)
				answer.add(arr[i]);
		}
		if (answer.isEmpty())
			answer.add(-1);
		Collections.sort(answer);
		return answer;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
