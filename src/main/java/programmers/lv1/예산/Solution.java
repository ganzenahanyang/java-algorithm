package programmers.lv1.예산;

import java.util.Arrays;

public class Solution {
	
    public int solution(int[] d, int budget) {
        int answer = 0;
        Arrays.sort(d);
        for(int i : d) {
        	if(budget >= i) {
        		budget -= i;
        		++answer;
        	}else
        		break;
        }
        return answer;
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
