package programmers.lv1.x만큼간격이있는n개의숫자;

public class Solution {

	public long[] solution(int x, int n) {
		long[] answer = new long[n];
		int index = 0;
		long num = x;
		while (n-- > 0) {
			answer[index++] = num;
			num += x;
		}
		return answer;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
