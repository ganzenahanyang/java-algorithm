package programmers.lv1.three진법뒤집기;

public class Solution {
	public int solution(int n) {
		int answer = 0;
		StringBuilder sb = new StringBuilder();
		while (n > 0) {
			sb.append(String.valueOf(n % 3));
			n /= 3;
		}
		answer = Integer.parseInt(sb.toString(), 3);
		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		int n = 45;
		s.solution(45);
	}

}
