package programmers.lv1.완주하지못한선수;

import java.util.Arrays;
import java.util.HashMap;

public class Solution {
    public String solution(String[] participant, String[] completion) {
        
        String answer = "";
       
        HashMap<String, Integer> hm = new HashMap<>();
        
        for(String player : participant)
            hm.put(player, hm.getOrDefault(player, 0) + 1);
        
        for(String player : completion)
            hm.put(player, hm.get(player) - 1);
        
        for(String key : hm.keySet()){
            if(hm.get(key) != 0){
                answer = key;
                break;
            }
        }
        return answer;
    }
    
    public String solution1(String[] participant, String[] completion) {
        
        Arrays.sort(participant);
        Arrays.sort(completion);
        
        int i = 0;
        for(; i < completion.length ; i++) {
        	if(!participant[i].equals(completion[i])) {
        		break;
        	}
        }
        return participant[i];
    }
    
    
}
