package programmers.lv1.핸드폰번호가리기;

public class Solution {

	public String solution(String phone_number) {
		StringBuilder sb = new StringBuilder();
		String answer = "";
		for (int i = 0; i < phone_number.length() - 4; i++) {
			sb.append("*");
		}
		sb.append(phone_number.substring(phone_number.length() - 4, phone_number.length()));
		answer = sb.toString();
		System.out.println(answer);
		return answer;
	}

	public String solution1(String phone_number) {
		char[] ch = phone_number.toCharArray();
		for (int i = 0; i < ch.length - 4; i++) {
			ch[i] = '*';
		}
		return String.valueOf(ch);
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		String phone_number = "01033334444";
		s.solution(phone_number);
	}

}
