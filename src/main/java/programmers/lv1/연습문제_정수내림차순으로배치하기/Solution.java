package programmers.lv1.연습문제_정수내림차순으로배치하기;

import java.util.Arrays;

public class Solution {
	
    public long solution(long n) {
        long answer = 0;
        String num = Long.toString(n);
        char [] chars = new char[num.length()];
        for(int i = 0 ; i < num.length() ; i++) {
        	chars[i] = num.charAt(i);
        	
        }
        
        Arrays.sort(chars);
        String temp = "";
        for(int i = chars.length - 1 ; i >= 0 ; i--) {
        	temp += String.valueOf(chars[i]);
        }
        answer = Long.parseLong(temp);
        return answer;
    }

	public static void main(String[] args) {
		Solution s = new Solution();
		System.out.println(s.solution(118372));

	}

}
