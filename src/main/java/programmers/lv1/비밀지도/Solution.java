package programmers.lv1.비밀지도;

public class Solution {

	public String[] solution(int n, int[] arr1, int[] arr2) {
		String[] answer = new String[n];
		char[][] board = new char[n][n];
		for (int i = 0; i < n; i++) {
			int t1 = arr1[i];
			int t2 = arr2[i];
			for (int j = n - 1; j >= 0; j--) {
				int n1 = t1 % 2;
				int n2 = t2 % 2;
				if(n1 == 1 || n2 == 1) {
					board[i][j] = '#';
				}else {
					board[i][j] = ' ';
				}
				t1 /= 2;
				t2 /= 2;
			}
		}
		for(int i = 0 ; i < n ; i++) {
			answer[i] = String.valueOf(board[i]);
		}
		
		return answer;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
