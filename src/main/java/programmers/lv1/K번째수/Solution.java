package programmers.lv1.K번째수;

import java.util.ArrayList;
import java.util.Collections;

public class Solution {

	public int[] solution(int[] array, int[][] commands) {
		int[] answer = new int[commands.length];
		for (int i = 0; i < commands.length; i++) {
			int start = commands[i][0] - 1;
			int end = commands[i][1] - 1;
			int index = commands[i][2] - 1;
			ArrayList<Integer> temp = new ArrayList<>();
			for (int j = start; j <= end; j++) {
				temp.add(array[j]);
			}
			Collections.sort(temp);
			answer[i] = temp.get(index);
		}
		return answer;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
