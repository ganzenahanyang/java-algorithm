package programmers.lv1.소수찾기;

import java.util.Arrays;

class Solution {
	public int solution(int n) {
		boolean[] arr = new boolean[n + 1];
		Arrays.fill(arr, true);

		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (!arr[i])
				continue;
			int j = 2;
			while (i * j <= n) {
				arr[i * j] = false;
				++j;
			}
		}
		int cnt = 0;
		for (int i = 2; i <= n; i++) {
			if (arr[i])
				cnt++;
		}
		return cnt;
	}
}
