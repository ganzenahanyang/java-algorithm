package programmers.lv2.오픈채팅방;

import java.util.ArrayList;
import java.util.HashMap;

public class Solution {

    static HashMap<String, String> hm = new HashMap<>();

    public ArrayList<String> solution(String[] record) {
        ArrayList<String> answer = new ArrayList<>();
        for (int i = 0; i < record.length; i++) {
            // 초기화
            String[] line = record[i].split(" ");
            String action = line[0];
            String uid = line[1];
            String name = "";

            // Enter, Change 인 경우에만 name 이 있다.
            if (line.length == 3) {
                name = line[2];
            }

            // name 이 빈 값이 아니면 uid 와 닉네임을 매핑해준다.
			// 이미 존재하는 key:value 구성인 경우, 새로운 value 가 overwrite 된다.
            if (!name.isEmpty())
                hm.put(uid, name);

            //각 동작별 메시지를 생성
            StringBuilder sb = new StringBuilder();
            sb.append(uid);
            if (action.equals("Enter")) {
                sb.append(" 들어왔습니다.");
            } else if (action.equals("Leave")) {
                sb.append(" 나갔습니다.");
            }

            // 닉네임 변경 기록은 출력되지 않는다.
            if (!action.equals("Change"))
                // "uid 들어왔습니다." or "uid 나갔습니다." 로 저장
                answer.add(sb.toString());
        }

        // uid 를 닉네임으로 바꿔준다.
        for (int i = 0; i < answer.size(); i++) {
            String uid = answer.get(i).split(" ")[0];
            String change = answer.get(i).replace(uid, hm.get(uid) + "님이");
            answer.set(i, change);
        }

        return answer;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        String[] record = {"Enter uid1234 Muzi", "Enter uid4567 Prodo", "Leave uid1234", "Enter uid1234 Prodo",
                "Change uid4567 Ryan"};
        s.solution(record);
    }

}
