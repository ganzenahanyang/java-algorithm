package programmers.lv2.카카오프렌즈컬러링북;

import java.util.ArrayDeque;
import java.util.Queue;

public class Solution {
    static class Pair {
        int y, x;

        public Pair(int y, int x) {
            super();
            this.y = y;
            this.x = x;
        }

    }

    static int[][] visit;
    static int[] dirY; // 행 이동 방향
    static int[] dirX; // 열 이동 방향
    static int numberOfArea; // 영역 번호 (1번부터 시작)
    static int maxSizeOfOneArea; // 영역의 최대 크기

    public int[] solution(int m, int n, int[][] picture) {
        // 전역변수를 함수 내에서 초기화해줘야 통과됨 (왜인지는 모름)
        numberOfArea = 1;
        maxSizeOfOneArea = 0;
        dirY = new int[]{0, 0, 1, -1};
        dirX = new int[]{1, -1, 0, 0};
        visit = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // 색이 칠해진 곳이면서, 어떤 영역에도 속하지 않는 경우
                if (picture[i][j] != 0 && visit[i][j] == 0) {
                    BFS(i, j, numberOfArea, m, n, picture);
                    numberOfArea++;
                }
            }
        }

        int[] answer = new int[2];
        answer[0] = numberOfArea - 1;
        answer[1] = maxSizeOfOneArea;

        return answer;
    }

    public void BFS(int i, int j, int no, int m, int n, int[][] picture) {

        Queue<Pair> q = new ArrayDeque<>();
        q.add(new Pair(i, j));
        // 특정 번호로 영역 표시
        visit[i][j] = no;
        // 영역의 크기 카운트
        int cnt = 1;
        while (!q.isEmpty()) {
            Pair now = q.poll();

            for (int d = 0; d < 4; d++) {
                int yy = now.y + dirY[d];
                int xx = now.x + dirX[d];

                if (yy < 0 || xx < 0 || yy >= m || xx >= n
                        || visit[yy][xx] != 0 || picture[yy][xx] != picture[i][j]) {
                    continue;
                }
                cnt++;
                visit[yy][xx] = no;
                q.add(new Pair(yy, xx));
            }
        }
        // 영역의 최댓값 구하기
        maxSizeOfOneArea = Math.max(maxSizeOfOneArea, cnt);
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}
