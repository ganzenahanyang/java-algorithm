package programmers.lv2.주식가격;

public class Solution {

	public int[] solution(int[] prices) {
		int[] answer = new int[prices.length];
		for (int i = 0; i < prices.length; i++) {
			int cnt = -1;
			for (int j = i; j < prices.length; j++) {
				if (prices[i] <= prices[j]) {
					++cnt;
				}else {
					++cnt;
					break;
				}
			}
			answer[i] = cnt;
		}

		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		int[] prices = { 1, 2, 3, 2, 3 };
		s.solution(prices);
	}

}
