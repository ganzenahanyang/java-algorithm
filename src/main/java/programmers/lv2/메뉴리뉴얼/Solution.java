package programmers.lv2.메뉴리뉴얼;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

public class Solution {

	static int[] maxCnt;
	static HashMap<String, Integer> hm = new HashMap<>();
	static boolean[] visit;
	static char[] splited;
	static char[] candi;

	public ArrayList<String> solution(String[] orders, int[] course) {
		ArrayList<String> answer = new ArrayList<>();
		maxCnt = new int[course[course.length - 1] + 1];
		for (int i = 0; i < orders.length; i++) {
			char[] temp = orders[i].toCharArray();
			Arrays.sort(temp);
			orders[i] = String.valueOf(temp);
			splited = orders[i].toCharArray();
			visit = new boolean[splited.length];
			for (int j = 0; j < course.length; j++) {
				candi = new char[course[j]];
				select(0, course[j], 0, orders);
			}
		}

		for (String key : hm.keySet()) {
			int value = hm.get(key);
			if (value == maxCnt[key.length()]) {
				answer.add(key);
			}

		}

		Collections.sort(answer);
		answer.stream().forEach(v -> System.out.print(v + " "));
		return answer;
	}

	static void select(int depth, int end, int index, String[] orders) {
		if (depth == end) {
			int cnt = 0;
			for (String s : orders) {
				boolean plus = true;
				for (int i = 0; i < candi.length; i++) {
					if (!s.contains(String.valueOf(candi[i]))) {
						plus = false;
						break;
					}
				}
				if (plus)
					cnt++;
			}
			if (cnt > 1) {
				String now = String.valueOf(candi);
				maxCnt[end] = Math.max(maxCnt[end], cnt);
				hm.put(now, cnt);
			}
			return;
		}

		for (int i = index; i < splited.length; i++) {
			if (visit[i])
				continue;
			visit[i] = true;
			candi[depth] = splited[i];
			select(depth + 1, end, i + 1, orders);
			visit[i] = false;
		}
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		String[] orders = { "ABCFG", "AC", "CDE", "ACDE", "BCFG", "ACDEH" };
		int[] course = { 2, 3, 4 };
		s.solution(orders, course);

	}

}
