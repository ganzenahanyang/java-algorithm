package programmers.lv2.타겟넘버;

public class Solution {
	static int answer = 0;

	public int solution(int[] numbers, int target) {
		DFS(0, numbers.length, 0, target, numbers);
		return answer;
	}

	static void DFS(int depth, int len, int total, int target, int[] numbers) {
		if (depth == len) {
			if (total == target)
				answer++;
			return;
		}

		DFS(depth + 1, len, total + numbers[depth], target, numbers);
		DFS(depth + 1, len, total - numbers[depth], target, numbers);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
