package programmers.lv2.다리를지나는트럭;

import java.util.ArrayDeque;
import java.util.Queue;

public class Solution {

	static class Truck {
		int weight;
		int time;

		public Truck(int weight, int time) {
			super();
			this.weight = weight;
			this.time = time;
		}

	}

	public int solution(int bridge_length, int weight, int[] truck_weights) {

		int answer = 0;

		int nowWeight = 0; // 현재 올라간 차의 무게

		Queue<Truck> q = new ArrayDeque<>();
		int i = 0; // 차 무게에 접근하기 위한 인덱스
		
		q.add(new Truck(truck_weights[i], 1));
		nowWeight += q.peek().weight;
		++i;
		++answer;
		
		while (!q.isEmpty()) {
			// 있는 트럭 모두 진행시키기
			int loop = q.size();
			for (int k = 0; k < loop; k++) {
				Truck now = q.poll();
				if (now.time != bridge_length) {
					now.time++;
					q.add(now);
				} else {
					nowWeight -= now.weight;
				}
			}

			// 새로 올릴 트럭 있나 확인
			if (i < truck_weights.length && (nowWeight + truck_weights[i] <= weight) && q.size() < bridge_length) {
				q.add(new Truck(truck_weights[i], 1));
				nowWeight += truck_weights[i];
				++i;
			}
			++answer;
		}

		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		int bridge_length = 2;
		int weight = 10;
		int[] truck_weights = { 7, 4, 5, 6 };

		int answer = s.solution(bridge_length, weight, truck_weights);
		System.out.println(answer);
	}

}
