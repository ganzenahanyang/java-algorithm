package programmers.lv2.HIndex;

import java.util.Arrays;

public class Solution {

	public int solution(int[] citations) {
		int answer = 0;
		Arrays.sort(citations);
		
		for(int i = 0 ; i < citations.length ; i++) {
			int h = citations.length - i;
			
			if(citations[i] >= h) {
				answer = h;
				break;
			}
		}
		
		System.out.println(answer);
		
		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		int[] arr = { 3, 0, 6, 1, 5 };
		s.solution(arr);

	}

}
