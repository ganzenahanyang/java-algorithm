package programmers.lv2.전화번호목록;

import java.util.Arrays;

public class Solution {

    public boolean solution(String[] phone_book) {
        boolean answer = true;
        /**
         * 사전 순으로 정렬하게 되면
         * i+1번째 문자열은 i번째 문자열과
         *
         * 1) 같거나
         * 2) i번째 문자열 + @ 이거나
         * 3) 이 외 사전 순서상 뒤 이다.
         */
        Arrays.sort(phone_book);
        for (int i = 0; i < phone_book.length - 1; i++) {
            if (phone_book[i + 1].startsWith(phone_book[i])) {
                answer = false;
                break;
            }
        }
        return answer;
    }


    public static void main(String[] args) {

        Solution s = new Solution();
//		String ex = "97674223";
//		System.out.println(ex.substring(0, 3));
        String[] phone_book = {"119", "97674223", "1195524421"};
//        Arrays.stream(phone_book).sorted().forEach(val -> System.out.print(val + " "));
//        System.out.println();
        s.solution(phone_book);

    }

}
