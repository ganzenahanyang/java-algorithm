package programmers.lv2.가장큰수;

import java.util.Arrays;
import java.util.Comparator;

class Solution {

    public String solution(int[] numbers) {
        String answer = "";

        String[] strs = new String[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            strs[i] = Integer.toString(numbers[i]);
        }

        Arrays.sort(strs, new Comparator<String>() {

            @Override
            public int compare(String o1, String o2) {
                // TODO Auto-generated method stub
                return (o1 + o2).compareTo(o2 + o1);
            }
        });

        if (strs[0].equals("0"))
            answer = "0";
        else {
            StringBuilder sb = new StringBuilder();
            for (String s : strs) {
                sb.append(s);
            }
            answer = sb.toString();
        }

        System.out.println(answer);

        return answer;
    }

    /**
     * 문자열에 새로 붙이는게 아니라 StringBuilder로 붙이는게 훨씬 속도 빠름
     * <p>
     * 30 3
     * (o1 + o2).compareTo(o2 + o1);
     * 303 vs 330
     * 음수 나와버림
     * 30이 3보다 앞으로
     */


    public static void main(String[] args) {
        Solution s = new Solution();
        int[] numbers = {6, 10, 2};
        s.solution(numbers);
    }

}
