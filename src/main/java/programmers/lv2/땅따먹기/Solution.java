package programmers.lv2.땅따먹기;

public class Solution {
    static int answer = 0;

    int solution(int[][] land) {

        for (int i = 1; i < land.length; i++) {
            int jj = 0;
            while (jj < land[i].length) {
                int max = 0;
                for (int j = 0; j < land[i].length; j++) {
                    if (j == jj)
                        continue;
                    max = Math.max(land[i - 1][j] + land[i][jj], max);
                }
                land[i][jj] = max;
                ++jj;
            }
            if (i == land.length - 1) {
                for (int j = 0; j < land[i].length; j++) {
                    answer = Math.max(answer, land[i][j]);
                }
            }
        }

        return answer;
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

    }

}
