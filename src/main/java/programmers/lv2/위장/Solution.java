package programmers.lv2.위장;

import java.util.HashMap;

public class Solution {

    public int solution(String[][] clothes) {
        int answer = 1;
        HashMap<String, Integer> hm = new HashMap<>();
        // 옷 종류 별 가짓 수 map에 저장
        for (int i = 0; i < clothes.length; i++) {
            hm.put(clothes[i][1], hm.getOrDefault(clothes[i][1], 0) + 1);
        }
        // 경우의 수 구하기
        for (String key : hm.keySet()) {
            answer *= hm.get(key) + 1;
        }
        // 아무것도 안 입는 경우는 제외
        --answer;
        return answer;
    }


    public static void main(String[] args) {
        Solution s = new Solution();
        String[][] input1 = {{"yellowhat", "headgear"}, {"bluesunglasses", "eyewear"},
                {"green_turban", "headgear"}};
        String[][] input2 = {{"crowmask", "face"}, {"bluesunglasses", "face"}, {"smoky_makeup", "face"}};
        //System.out.println(s.solution(input1));
        System.out.println();
        System.out.println(s.solution(input2));
    }

}
