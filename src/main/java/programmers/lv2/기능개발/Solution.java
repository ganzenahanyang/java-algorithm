package programmers.lv2.기능개발;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class Solution {
	static class Work {
		int progress;
		int speed;

		public Work(int progress, int speed) {
			super();
			this.progress = progress;
			this.speed = speed;
		}

	}

	static Queue<Work> q = new LinkedList<>();

	public ArrayList<Integer> solution(int[] progresses, int[] speeds) {
		ArrayList<Integer> answer = new ArrayList<>();
		for (int i = 0; i < progresses.length; i++) {
			q.add(new Work(progresses[i], speeds[i]));
		}
		while (!q.isEmpty()) {
			int cnt = 0;
			int qSize = q.size();

			while (qSize-- > 0) {
				Work now = q.poll();
				now.progress += now.speed;
				q.add(now);
			}
			qSize = q.size();
			while (qSize-- > 0) {
				Work now = q.peek();
				if (now.progress >= 100) {
					q.poll();
					cnt++;
				} else {
					break;
				}
			}
			if(cnt > 0)
				answer.add(cnt);

		}
		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		int[] progresses = { 93, 30, 55 };
		int[] speeds = { 1, 30, 5 };
		s.solution(progresses, speeds).stream().forEach(val -> System.out.print(val + " "));
	}
}
