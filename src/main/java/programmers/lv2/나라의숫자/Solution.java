package programmers.lv2.나라의숫자;

public class Solution {
	/**
	 * 3진법으로 생각
	 * 나머지가
	 * 0 = 4
	 * 1 = 1
	 * 2 = 2
	 * 
	 */

	public String solution(int n) {
		String answer = "";
		StringBuilder sb = new StringBuilder();
		while (n > 0) {
			int mod = n % 3;
			if (mod == 0) {
				n = n / 3 - 1;
			} else {
				n = n / 3;
			}
			sb.append(String.valueOf(mod == 0 ? 4 : mod));
		}
		sb = sb.reverse();
		answer = sb.toString();
		return answer;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
