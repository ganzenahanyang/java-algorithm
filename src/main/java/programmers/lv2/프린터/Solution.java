package programmers.lv2.프린터;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class Solution {

    static class Work {
        int no;
        int priority;

        public Work(int no, int priority) {
            super();
            this.no = no;
            this.priority = priority;
        }

    }

    static Deque<Work> q = new ArrayDeque<>();

    public int solution(int[] priorities, int location) {
        int answer = 1;
        for (int i = 0; i < priorities.length; i++) {
            q.add(new Work(i, priorities[i]));
        }

        while (!q.isEmpty()) {
            Work now = q.poll();
            Iterator<Work> iter = q.iterator();
            while (iter.hasNext()) {
                // now 보다 우선순위가 높은 작업이 있으면 now 를 맨 뒤에 넣고 마침
                if (iter.next().priority > now.priority) {
                    q.addLast(now);
                    break;
                }
            }
            // 끝까지 돌지않았다면 now 를 인쇄해야한다는 것
            if (!iter.hasNext()) {
                if (now.no == location)
                    break;
                else
                    ++answer;
            }
        }

        return answer;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        int[] priorities = {1, 1, 9, 1, 1, 1};
        int location = 0;
        System.out.println(s.solution(priorities, location));
    }
}
