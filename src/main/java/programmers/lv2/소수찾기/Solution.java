package programmers.lv2.소수찾기;

import java.util.ArrayList;

public class Solution {

	static ArrayList<Character> candi = new ArrayList<>();
	static int answer = 0;
	static int[] arr = new int[10000000];
	static boolean[] visit;

	public int solution(String numbers) {

		char[] splited = numbers.toCharArray();
		visit = new boolean[splited.length];
		// 0은 소수 1은 소수가 아님 2는 이미 카운트됨
		arr[0] = 1;
		arr[1] = 1;
		for (int i = 2; i <= Math.sqrt(10000000); i++) {
			if (arr[i] == 0) {
				int j = 2;
				while (i * j < 10000000) {
					arr[i * j] = 1;
					j++;
				}
			}
		}

		select(0, splited);

		return answer;
	}

	static void select(int depth, char[] splited) {
		if (depth > splited.length) {

			return;
		}

		if (!candi.isEmpty()) {
			char[] target = new char[candi.size()];
			for (int i = 0; i < candi.size(); i++) {
				target[i] = candi.get(i);
			}
			int num = Integer.parseInt(String.valueOf(target));
			if (arr[num] == 0) {
				answer++;
				arr[num] = 2;
			}
		}

		for (int i = 0; i < splited.length; i++) {
			if (!visit[i]) {
				candi.add(splited[i]);
				visit[i] = true;
				select(depth + 1, splited);
				visit[i] = false;
				candi.remove(candi.size() - 1);
			}
		}

	}
}
