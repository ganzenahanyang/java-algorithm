package programmers.lv2.최댓값과최솟값;

public class Solution {

    public String solution(String s) {
        StringBuilder sb  = new StringBuilder();
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        String[] nums = s.split(" ");
        for(int i = 0 ; i < nums.length ; i++) {
        	max = Math.max(Integer.parseInt(nums[i]), max);
        	min = Math.min(Integer.parseInt(nums[i]), min);
        }
        sb.append(min).append(" ").append(max);
        return sb.toString();
    }
    
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
