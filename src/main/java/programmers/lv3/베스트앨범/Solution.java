package programmers.lv3.베스트앨범;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Solution {

	static class Song implements Comparable<Song> {
		int no;
		int cnt;
		String genre;

		public Song(int no, int cnt, String genre) {
			super();
			this.no = no;
			this.cnt = cnt;

			this.genre = genre;
		}

		public Song(int cnt, String genre) {
			super();
			this.cnt = cnt;
			this.genre = genre;
		}

		@Override
		public int compareTo(Song o) {
			if (this.cnt < o.cnt) {
				return 1;
			} else if (this.cnt > o.cnt) {
				return -1;
			} else {
				if (this.no < o.no) {
					return -1;
				} else {
					return 1;
				}
			}

		}

	}

	static HashMap<String, Integer> hm = new HashMap<>();
	static ArrayList<Song> ranks = new ArrayList<>();

	public ArrayList<Integer> solution(String[] genres, int[] plays) {
		ArrayList<Integer> answer = new ArrayList<>();
		for (int i = 0; i < genres.length; i++) {
			hm.put(genres[i], hm.getOrDefault(genres[i], 0) + plays[i]);
		}
		for (String key : hm.keySet()) {
			ranks.add(new Song(hm.get(key), key));
		}

		Collections.sort(ranks);

		for (int i = 0; i < ranks.size(); i++) {
			ArrayList<Song> temp = new ArrayList<>();
			for (int j = 0; j < genres.length; j++) {
				if (ranks.get(i).genre.equals(genres[j])) {
					temp.add(new Song(j, plays[j], genres[j]));

				}
			}

			Collections.sort(temp);
			answer.add(temp.get(0).no);
			if (temp.size() > 1)
				answer.add(temp.get(1).no);
		}

		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		String[] genres = { "classic", "pop", "classic", "classic", "pop" };
		int[] plays = { 500, 600, 150, 800, 2500 };
		s.solution(genres, plays).forEach(val -> System.out.print(val + " "));
	}
}
