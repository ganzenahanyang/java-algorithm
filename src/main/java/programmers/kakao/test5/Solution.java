package programmers.kakao.test5;

public class Solution {
	
    public int solution(int[] info, int[][] edges) {
        int answer = 0;
        for(int i = 0 ; i<edges.length ; i++) {
        	for(int j = 0 ; j < edges[i].length ; j++) {
        		System.out.print(edges[i][j] + " ");
        	}System.out.println();
        }System.out.println();
        return answer;
    }

	public static void main(String[] args) {
		Solution s = new Solution();
		int[] info = {0,0,1,1,1,0,1,0,1,0,1,1};
		int[][] edges = {{0,1},{1,2},{1,4},{0,8},{8,7},{9,10},{9,11},{4,3},{6,5} ,{4,6},{8,9}};
		s.solution(info, edges);
	}

}
