package programmers.kakao.test3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

public class Solution {
	
	
	static class Car implements Comparable<Car>{
		String no;
		String inTime = "";
		String outTime = "";
		int stackMin = 0;
		int fee = 0;
		
		public int gapMin() throws ParseException {
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			if(this.outTime.isEmpty()) {
				this.outTime = "23:59";
			}
			Date bef = format.parse(this.inTime);
			Date aft = format.parse(this.outTime);
			long millisec = aft.getTime() - bef.getTime();
			long hour = millisec / 1000 / 60 /60;
			long min = millisec / 1000 / 60 % 60;

			int result = (int)(hour * 60 + min);
			return result;
			
		}

		@Override
		public int compareTo(Car o) {
			return Integer.parseInt(this.no) - Integer.parseInt(o.no);
		}
	}

	public int[] solution(int[] fees, String[] records) throws ParseException {
		int[] answer = {};
		HashSet<String> carNo = new HashSet<>();
		ArrayList<Car> cars = new ArrayList<>();
		for(int i = 0 ; i < records.length ; i++) {
			String[] info = records[i].split(" ");
			String time = info[0];
			String no = info[1];
			String type = info[2];
			
			if(carNo.contains(no)) {
				for(int j = 0 ; j < cars.size() ; j++) {
					Car car = cars.get(j);
					if(car.no.equals(no)) {
						if(type.equals("IN")) {
							car.inTime = time;
						}else {
							car.outTime = time;
						}
						if(!car.inTime.isEmpty() && !car.outTime.isEmpty()) {
							car.stackMin += car.gapMin();
							car.inTime = "";
							car.outTime = "";
						}
						break;
					}
				}
			}else {
				carNo.add(no);
				Car car = new Car();
				car.no = no;
				if(type.equals("IN")) {
					car.inTime = time;
				}else {
					car.outTime = time;
				}
				cars.add(car);
			}
		}
		
		Collections.sort(cars);
		answer = new int[cars.size()];
		
		for(int i = 0 ; i < cars.size() ; i++) {
			Car car = cars.get(i);
			if(!car.inTime.isEmpty() && car.outTime.isEmpty()) {
				car.stackMin += car.gapMin();
			}
			int gapMin = car.stackMin;
			if(gapMin <= fees[0]) {
				answer[i] = fees[1];
			}else {
				int fee = fees[1];
				gapMin -= fees[0];
				if(gapMin % fees[2] != 0) {
					gapMin /= fees[2];
					++gapMin;
				}else {
					gapMin /= fees[2];
				}
				fee += gapMin * fees[3];
				answer[i] = fee;
			}
		}
		
		for(int i = 0 ; i < answer.length ; i++) {
			System.out.println(answer[i]);
		}
		
		
		return answer;
	}

	public static void main(String[] args) throws ParseException {
		Solution s = new Solution();
		int[] fees = {120, 0, 60, 591};
		String[] records = { "16:00 3961 IN","16:00 0202 IN","18:00 3961 OUT","18:00 0202 OUT","23:58 3961 IN" };
		s.solution(fees, records);
	}

}
