package programmers.kakao.test2;

import java.util.HashMap;

public class Solution {

	static public boolean checkerNotPrime(long n) {
		boolean notPrime = false;
		if (n == 0 || n == 1)
			return true;
		int i;
		for (i = 2; (i * i) <= n; i++) {
			if (n % i == 0) {
				notPrime = true;
				break;
			}
		}
		System.out.println(i);
		return notPrime;
	}

	public int solution(int n, int k) {
		int answer = 0;
		String nResult = "";
		StringBuilder sb = new StringBuilder();
		while (n > 0) {
			long remain = n % k;
			if (remain > 9) {
				sb.insert(0, (char) (remain + 55));
			} else {
				sb.insert(0, remain);
			}
			n /= k;
		}
		nResult = "11";
		System.out.println("진수 변환 : " + nResult);
		String[] splited = nResult.split("0+");
		HashMap<Long, Integer> hm = new HashMap<>();
		if (splited.length != 0) {
			for (int i = 0; i < splited.length; i++) {
				hm.put(Long.parseLong(splited[i]), hm.getOrDefault(Long.parseLong(splited[i]), 0) + 1);
			}
			for (Long key : hm.keySet()) {
				if (!checkerNotPrime(key)) {
					answer += hm.get(key);
				}

			}
		}else {
			if (!checkerNotPrime(Long.parseLong(nResult))) {
				++answer;
			}
		}

		System.out.println("answer = " + answer);
		return answer;
	}

	public static void main(String[] args) {
		Solution s = new Solution();
		s.solution(110011, 10);

	}

}
