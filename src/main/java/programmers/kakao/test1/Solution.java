package programmers.kakao.test1;

import java.util.ArrayList;
import java.util.HashMap;

public class Solution {
	
	static class Report{
		String name; //신고당한사람
		ArrayList<String> attacker = new ArrayList<String>(); //신고자
		public Report(String name, ArrayList<String> attacker) {
			super();
			this.name = name;
			this.attacker = attacker;
		}
	}
    public int[] solution(String[] id_list, String[] report, int k) {
        int[] answer = new int[id_list.length]; 
        ArrayList<Report> reportList = new ArrayList<>();
        for(int i = 0 ; i < id_list.length ; i++) {
        	reportList.add(new Report(id_list[i], new ArrayList<String>()));
        }
        
        HashMap<String, Integer> hm = new HashMap<>() ;
        HashMap<String, String> history  = new HashMap<>() ;
        for(int i = 0 ; i < report.length ; i++) {
        	String[] list = report[i].split(" ");
        	String a = list[0]; //신고자
        	String b = list[1]; //신고당한사람
        	
        	
        	for(int j = 0 ; j < id_list.length ; j++) {
        		Report r = reportList.get(j);
        		if(r.name.equals(b)) {
        			if(r.attacker.contains(a))
        				continue;
        			else
        				r.attacker.add(a);
        			
        			break;
        		}
        	}
        }
        
        for(int i = 0 ; i < reportList.size() ; i++) {
        	Report r = reportList.get(i);
        	if(r.attacker.size() >= k) {
        		for(int j = 0 ; j < r.attacker.size() ; j++) {
                	hm.put(r.attacker.get(j), hm.getOrDefault(r.attacker.get(j), 0) + 1);

        		}
        	}
        }
        
        for(int i = 0 ; i < id_list.length ; i++) {
        	answer[i] = hm.getOrDefault(id_list[i], 0);
        }
        
        
        return answer;
    }
	
	
	
	
	
	
	
	
	

	public static void main(String[] args) {
		Solution s = new Solution();
		String[] id_list = {"muzi", "frodo", "apeach", "neo"};
		String[] report = {"muzi frodo","apeach frodo","frodo neo","muzi neo","apeach muzi"};
		int k = 2;
		s.solution(id_list, report, k);
	}

}
